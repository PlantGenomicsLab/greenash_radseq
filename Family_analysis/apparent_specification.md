## Running Apparent
Input File:
* File originated as VCFv4.2 output from freebayes run on chromosomes
	* Only filter used was to remove badly sequenced inviduals FP3150 and FP3112
* File was converted to format that apparent expects using script [convert_vcf_to_format.py](scripts/convert_vcf_to_format.py)
* SNP's that had REF and ALT strings longer than 1 (A region of nucleotides with a single SNP) were discarded
	* 449 SNP's discarded out of 180,789

Parameter explanation:
- MaxIdent: Sets the maximum triad GDij|k to be considered for outlier significance testing. This parameter directly impacts computation time. By default, MaxIdent is set to 0.1.
- alpha: The alpha level for all significance testing. Not relevant considering we used triad_all.txt for the following steps.
- nloci: The minimum acceptable number loci to be used when computing the pairwise GDij|k. The default value of 100 is suggested, based on previous investigations. All triads for which the number of usable SNPs falls below nloci will be excluded from the analysis
- self: Logical value for instructing 'apparent' whether or not to consider self-crossing (parent i = parent j)
- plot: Logical value for create the plots of both Triad and Dyad analysis. By default, 'apparent' creates only the Triad analysis plot. The default value is TRUE, meaning 'apparent' creates only the Triad analysis plot. The Dyad analysis plot can't be printed, only saved (it is a pdf file with multiple plots for each significant offspring). To save Dyad analysis plot, make sure plot, files and Dyad flags are TRUE.
- Dyad: Compare all parent-offspring pairs. Unused for our analysis due to needing equal confidence in both parents, rather than just one. 


```
source("apparent.R")
options(warn=1)
library(outliers)
#InputFile <- read.table("smaller.txt",sep="\t",h=F)
InputFile <- read.table("converted.txt",sep="\t",h=F)
#InputFile <- read.table("apparent_TestData.txt",sep="\t",h=F)
# Run apparent
o <- apparent(InputFile, MaxIdent=0.10, alpha=0.01, nloci=300, self=False, plot=TRUE, Dyad=FALSE)

# Save output to files
write.table(o$Triad_all,"triad_all.txt")
write.table(o$Triad_sig,"triad_sig.txt")
write.table(o$Triad_summary_geno,"triad_summary_geno.txt")
write.table(o$Triad_summary_pop,"triad_summary_pop.txt")
```

Output:  
* [ApparentPVals](apparent_pvals.pdf)
* [triad_all.txt.gz](triad_all.txt.gz)
	* GD values for all triads (large file). Used best guesses per sample for family analysis
* [triad_summary_geno.txt](triad_summary_geno.txt)
	* Summary statistics from the Triad analysis. For each individual genotype in the analysis: Mean GDij|POk, GDij|POk range, and mean usable number of SNPs for all comparisons involving that genotype. Genotypes exhibiting a mean GDij|POk or number of usable SNPs less than 2 SD's below the population means are considered outliers.

## Family Restructuring

Manipulation of Apparent Output:  
* Only output from triad_all.txt was used
* R code in extract_best_guess.R
1. Samples with cross type "self" were discarded
2. Samples were grouped by Offspring, and the lowest value was selected for each, and output as a csv
3. After being placed in spreadsheet samples were individually highlighted according to agreement from 3 sources: Original parents, A previous family analysis done, and apparent's results
4. Family and individual assignment was made using this agreement as well as GD value output from apparent.
5. Individuals listed as dropped will be used to re-run the remove-individuals filter. Family reassignment will be used in RQTL analysis as well as child_parent custom filter. 

