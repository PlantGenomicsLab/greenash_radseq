import re,sys
import numpy as np

key="All"

def get_header(vcf):
  m = re.compile("#CHROM*") 
  with open(vcf) as rf:
    header = ""
    for i in rf:
      m1 = m.match(i)
      if(m1):
        header = i
        break
    header = header.strip().split()
    header = header[9:] #just get individuals
    return header

def get_num_loci(vcf_fp):
  vcf_fp.seek(0)
  count = 0
  for line in vcf_fp:
    if(line.startswith("#")):
      continue
    count+=1
  return count
    

def fix(fn):
  individuals = get_header(fn)
  with open("converted2.txt",'w') as wf:
    with open(fn) as rf:
      num_loci = get_num_loci(rf)
      snp_ar = np.empty((num_loci+2,len(individuals)),dtype='<81U')
      linecount = 0
      rf.seek(0)
      for line in rf:
        if(line.startswith("#")):
          continue
        line = line.strip().split()
#        lociname = line[0]+"_"+line[1] #chr+position
#        snp_ar[linecount,0] = lociname
        ref,alts = line[3],line[4].split(",") 
        dlist = zip(list(range(1,len(alts)+1)),[x for x in alts])
        dlist = [(str(i[0]),i[1]) for i in dlist]
        d = dict(dlist)
        d['0'] = ref
        d['.'] = '-'
        for i,indiv in enumerate(line[9:]):
          snp = indiv[:3]
          snp = snp.split("|")
          if(len(snp)==1): #phased variant
            snp = snp[0].split("/")
          if(len(snp)==1): #missing variant
            snp = [".","."]
          snp_ar[linecount+2,i] = "%s/%s" % (d[snp[0]],d[snp[1]])
        linecount+=1
      # insert columns
      snp_ar[0,:] = individuals
      snp_ar[1,:] = [key,] * len(individuals)
    np.savetxt(wf,snp_ar.T,delimiter="\t",fmt='%s')

if(__name__=="__main__"):
  fn = sys.argv[1]
  ar = fix(fn)      
