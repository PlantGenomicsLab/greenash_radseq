## Introduction

After calling SNP's in our ddRAD and WGS samples, we produced a [multidimensional scaling (mds) plot](figures/MDS_beforerestructure.pdf)
from the data to visualize family structure. We noticed some potential problems in this plot, such as family H clustering into two parts,
and offspring that were distant from any cluster (3032, 3135, 3147) and decided it would be worth attempting to re-analyze family structure
in our data. We did this via two methods, one being performed by Noah Reid in the lab doing a manual check of parent-child relationships,
and one utilizing existing software called [apparent](https://github.com/halelab/apparent) to do a thorough check of all triads (parent1,parent2,offspring)
in the dataset to determine family structure. The outputs of both methods were then compared with the original family structure, and decisions to relabel,
restructure, or remove individuals were made based on agreement between the three family structures. The details of this analysis are [here](https://drive.google.com/file/d/1ubGb_OtGtvkM1TKpNTYjmPYXKrLAOh_Z/view?usp=sharing).
You can see the clustering improvements in the new MDS [here](figures/MDS_afterrestructure.pdf)

Details on running apparent [here](apparent_specification.md):

## Additional Family Structure Information (original)
* Star = phenotyped across two years  

| Family | Female Parent<br>(Phenotype) | Male Parent<br>(Phenotype) | Number of<br>Progeny Tested | Number of Progeny<br>No Egg Control |
| ------ | ---------------------------- | -------------------------- | --------------------------- | ----------------------------------- |
| Pe-C\* | Pe-97 (S)                    | Pe-SUM(S)                  | 10                          | 5                                   |
| Pe-M   | Pe-36(S)                     | Pe-55(L)                   | 17                          | 0                                   |
| Pe-AC  | Pe-64(L)                     | Pe-59(L)                   | 30                          | 10                                  |
| Pe-D   | Pe-48 (S)                    | Pe-SUM(S)                  |                             |                                     |
| Pe-E   | Pe-45 (L)                    | Pe-56(L)                   |                             |                                     |
| Pe-F   | Pe-45 (L)                    | Pe-67                      |                             |                                     |
| Pe-G   | Pe-41(L)                     | Pe-40(L)                   | 10                          | 5                                   |
| Pe-H   | Pe-65 (L)                    | Pe-56(L)                   |                             |                                     |
| Pe-I   | Pe-65 (L)                    | Pe-62(L)                   |                             |                                     |
| Pe-J\* | Pe-64(L)                     | Pe-56(L)                   | 30                          | 12                                  |
| Pe-K\* | Pe-60(L)                     | Pe-62(L)                   | 10                          | 3                                   |
| Pe-L   | Pe-57                        | Pe-56(L)                   |                             |                                     |
| Pe-X   | Pe-53(L)                     | Pe-55(L)                   | 13                          | 0                                   |
| Pe-Y   | Pe-53(L)                     | Pe-56(L)                   | 30                          | 12                                  |
| Pe-Z   | Pe-53(L)                     | Pe-59(L)                   | 30                          | 12                                  |

## Original Family Structure
- Reflects progeny count after filtering VCF

| Fam   | Par1      | Par2       | Progeny | Avg Host Kill Percentage of Good Eggs | Avg Mean Larval Weight |
|-------|-----------|------------|---------|---------------------------------------|------------------------|
| C     | Pe-97 (S) | Pe-Sum (S) | 10      | 0.13                                  | 0.05                   |
| D     | Pe-48 (S) | Pe-Sum (S) | 5       | 0.21                                  | 0.03                   |
| E     | Pe-45(L)  | Pe-56 (L)  | 4       | 0.09                                  | 0.03                   |
| F     | unknown   | unknown    | 2       | 0.23                                  | 0.06                   |
| G     | unknown   | unknown    | 5       | 0.11                                  | 0.06                   |
| H     | Pe-65 (L) | Pe-56 (L)  | 19      | 0.2                                   | 0.05                   |
| I     | Pe-65 (L) | Pe-62 (L)  | 19      | 0.17                                  | 0.05                   |
| J     | Pe-64 (L) | Pe-56 (L)  | 6       | 0.14                                  | 0.04                   |
| K     | Pe-60 (L) | Pe-62 (L)  | 20      | 0.16                                  | 0.05                   |
| L     | unknown   | unknown    | 1       | unk                                   | unk                    |
| Total |           |            | 91      |                                       |                        |

## New Family Structure
| Fam   | Female (resistance) | Male (resistance) | Progeny | Avg Host Kill Percentage of Good Eggs | Avg Mean Larval Weight |
|-------|---------------------|-------------------|---------|---------------------------------------|------------------------|
| C'    | Pe-97 / 3148 (S)    | Pe-62 / 3160 (L)  | 9       | 0.13                                  | 0.05                   |
| D'    | Pe-97 / 3148 (S)    | Pe-Sum / 3145 (S) | 5       | 0.21                                  | 0.03                   |
| E     | Pe-45 / 3156 (L)    | Pe-56 / 3158 (L)  | 3       | 0.09                                  | 0.03                   |
| Ha    | Pe-65 / 3159 (L)    | Pe-56 / 3158 (L)  | 6       | 0.25                                  | 0.04                   |
| Hb    | Pe-65 / 3159 (L)    | 283 / 3069 (L)    | 14      | 0.14                                  | 0.06                   |
| I     | Pe-65 / 3159 (L)    | Pe-62 / 3160 (L)  | 20      | 0.17                                  | 0.05                   |
| J     | Pe-64 / 3161 (L)    | Pe-56 / 3158 (L)  | 7       | 0.14                                  | 0.04                   |
| K     | Pe-60 / 3162 (L)    | Pe-62 / 3160 (L)  | 19      | 0.16                                  | 0.05                   |
| Total |                     |                   | 83      |                                       |                        |


