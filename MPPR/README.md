## Progress

All scripts mentioned here are included in this git section
Scripts + Results exist on cluster here: `/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/RQTL/mppR`

NOTE:
For mppR we need input geno, pheno, par_per_cross, and map tables
Geno is created from our filtered vcf, the rest are manually created.

1. Filtered VCF taken from /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/variant_call/filtering/typesnp_8/final_filtered.vcf
2. Additional filters run to make VCF compatible with mppR
* Filtering VCF for mapped markers only `filter_vcf_for_mapped_markers.py`
* Filtering for biallelic SNP's only `only_biallelic.sh`
* Additional filtering for strange indel's `filter_indels.py`
3. Final conversion script for VCF to geno.txt format that mppR accepts `vcf_to_mppr.py`
4. Manually create pheno, par_per_cross, and map tables
5. Run mppr.R
* `run_mppr.sh`
* Submits job `mppr.R` to cluster

## Current issues + fix history

1. mppR finds indels despite our filtering (FIXED with `filter_indels.py`)
2. We do not have the male parent for family H-B in our dataset, and 
mppR does not accept NA parents (FIXED have to drop family H-B)
3. No significant QTL are found at end of run (TODO)
