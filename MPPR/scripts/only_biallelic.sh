#!/bin/bash
#SBATCH --job-name=default_run
#SBATCH -o %j.out
#SBATCH -e %j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

module load bcftools

vcf=markers.recode.vcf

bcftools view -m2 -M2 -v snps $vcf > markers.recode.biallelic.vcf
