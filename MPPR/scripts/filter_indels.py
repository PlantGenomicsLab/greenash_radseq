import sys

inpt = sys.argv[1]

with open("biallelic_only.vcf",'w') as wf:
  counter=0
  removed=[]
  with open(inpt) as rf:
    for line in rf:
      counter+=1
      if(line.startswith("#")):
        wf.write(line)
        continue
      ref = line.strip().split()[3]
      if(len(ref)!=1):
        removed.append(counter)
      else:
        wf.write(line)
  print("%d lines removed" % len(removed))
  print("%s" % str(removed))
