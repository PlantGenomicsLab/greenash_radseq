library(mppR)

geno=read.table("geno.txt",row.names=1,header=T,na.strings="--")
geno = as.matrix(geno)

parents=c("Sample_3145","Sample_3148","Sample_3156","Sample_3158","Sample_3159","Sample_3160","Sample_3161","Sample_3162")

geno.par = geno[parents,]
geno.off = geno[!rownames(geno) %in% parents,]


map=read.csv("map_filtered.csv",header=T,stringsAsFactors=FALSE)

print(dim(map))
all(map[,1]==colnames(geno.off))

pheno=as.matrix(read.csv("pheno_reordered.csv"),row.names=1)

geno.off = geno.off[match(rownames(pheno),rownames(geno.off)),]

class(pheno) <- "numeric"

cross.ind = substr(rownames(pheno),1,2)
par.per.cross = as.matrix(read.table("par_per_cross.txt"))

mppData = create.mppData(geno.off = geno.off, geno.par = geno.par, map = map, pheno = pheno, cross.ind = cross.ind, par.per.cross = par.per.cross)

# filtering
mppData <- QC.mppData(mppData = mppData, n.lim = 5, MAF.pop.lim = 0.05, MAF.cr.miss = TRUE, mk.miss = 0.1, gen.miss = 0.25, verbose = TRUE)

mppData <- IBS.mppData(mppData = mppData)

mppData <- IBD.mppData(mppData = mppData, het.miss.par = TRUE, type = 'F', F.gen = 1)

# Parent clustering not needed for parental model
#mppData <- parent_cluster.mppData(mppData = mppData, par.clu = par_clu)

summary(mppData)

QTL_proc <- mpp_proc(pop.name = "Ash", trait.name = "HKP", trait = "HKP",
  mppData = mppData, Q.eff = "par",
  plot.gen.eff = TRUE, N.cim = 1, thre.cof = 3,
  win.cof = 20, window = 20, thre.QTL = 3,
  win.QTL = 20, CI = TRUE, drop = 1.5,
  verbose = TRUE, output.loc = "resultsHKP")

#QTL_proc <- mpp_proc(pop.name = "Ash", trait.name = "MLW", trait = "MLW",
#  mppData = mppData, Q.eff = "par",
#  plot.gen.eff = TRUE, N.cim = 1, thre.cof = 3,
#  win.cof = 20, window = 20, thre.QTL = 3,
#  win.QTL = 20, CI = TRUE, drop = 1.5,
#  verbose = TRUE, output.loc = "resultsMLW")

#MQE <- MQE_proc(pop.name = "Ash", trait.name = "ULA", mppData = mppData,
#  Q.eff = "par", window= 20, verbose = FALSE, output.loc = "saved_mqeproc")
