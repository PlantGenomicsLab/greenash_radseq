import sys

inp = sys.argv[1]

with open(inp) as rf:
  with open("ash_map.txt",'w') as wf:
    wf.write("mk.names\tchr\tpos.cM\n")
    for line in rf:
      line = line.strip().split()
      chr,bp,cm = line
      chrnum=int(chr[3:])
      wf.write("%s\t%s\t%s\n" % (chr+"_"+bp,str(chrnum),cm))
