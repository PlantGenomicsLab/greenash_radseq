vcf_fn="/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/variant_call/filtering/typesnp_8/final_filtered.vcf"
markers_fn="mapped_markers.txt"

markers=[]
with open(markers_fn) as rf:
  for line in rf:
    markers.append(line.strip().split()[:2])
