# Github page for Green Ash (Fraxinus pennsylvanica) RQTL Study
### Important Links
* [Phenotypes_File](https://docs.google.com/spreadsheets/d/1A07rpdYLJ99HnHW23zZmXzYwXGykdwaH5cyZiBIdNUA/edit#gid=0)
* [Family_Analysis](https://docs.google.com/spreadsheets/d/1ubGb_OtGtvkM1TKpNTYjmPYXKrLAOh_Z/edit#gid=116016846)

### Authors
Jeremy Bennett (1), Ava Fritz (1), Noah Reid (1), Jeanne Romero-Severson (2), Megan Reid (2), Jennifer Koch (3),  Jill Wegrzyn (1)

Department of Ecology and Evolutionary Biology, University of Connecticut, Storrs, CT, USA
Department of Biology, University of Notre Dame, Notre Dame, IN, USA
USDA Northeastern Research Station, Delaware OH, USA

### Abstract
Emerald Ash Borer is an invasive pest that is threatening the sustainability of the entire American ash tree population. Killing an ash tree in three years or less, this issue is causing major ecological and economical impacts, as ash trees are a major component of natural forests and an important resource. However, there are some ash trees, known as lingering ash, that appear to resist the harmful effects of an Emerald Ash Borer invasion in their area for much longer than others. In our study, we are conducting a genome wide association study of American green ash trees in order to determine if there is evidence of resistance markers in lingering ash trees that make them less susceptible to being invaded by Emerald Ash Borer. We will be comparing known phenotypes and parentage of our samples to determine whether there is a correlation between the lingering ash trees and their genomes. In addition, we will be applying the information obtained to a mobile application accessory to help common homeowners and researchers identify green ash trees in natural forests that may be affected by Emerald Ash Borer.

## Preprocessing DNA
* 151 total leaf samples, 17 of these were parental 
* 60 failed gDNA QC, 91 sequenced as ddRAD, 12 sequenced WGS
* [ddRAD Reads MultiQC Report](Preprocessing/Alignment/figures/multiqc_report.html)
* ddRAD align 100%, WGS align 82-98%
* [WGS alignment](Preprocessing/Alignment/figures/WGS_mapping.pdf)

[Full breakdown](Preprocessing/README.md)

## Family Analysis
* [MDS plot](Family_analysis/figures/MDS_beforerestructure.pdf) produced after filtering was problematic when clustering by family
* Two distinct clusters formed from family H, multiple individual outliers (3032, 3135, 3147, 3108, 3069) and a mislabeled sample (3132)

1. Software Apparent used to generate "best guess" for parents for each sample
2. Results charted on a [spreadsheet](https://docs.google.com/spreadsheets/d/1ubGb_OtGtvkM1TKpNTYjmPYXKrLAOh_Z/edit#gid=116016846)  and parents were re-assigned using agreement between original family, apparent output, and results from separate analysis done by Lab member Noah Reid
3. Full apparent pipeline [here](Family_analysis/apparent_specification.md)

[Full breakdown](Family_analysis/README.md)

## Variant Calling/Filtering
* 3,565,012 Sites Called using FreeBayes
* 102 Individuals
* Filtered down to 1,325,113 Sites and 100 individuals

[Full breakdown](Variant_calling/README.md)

## Genetic Map Construction
* 27,417 markers. Of which, 4642 are unique
* [Visualization](Genetic_map/GeneticPhysicalCorrelation.pdf) 

## QTL
Multiple attempts at QTL have been performed.
* [MPPR](QTL/mppr/README.md)
* [Merlin](Merlin_analysis/README.md)
* [EMMAX](EMMAX/README.md)
