## Genetic Map pipeline

* Using Lepmap3 and imputed vcf from filtering steps, created a genetic map with 27,417 markers. Of which, 4642 are unique
* [Visualization](figures/GeneticPhysicalCorrelation.pdf)
* [output](mapped_markers.txt)
* Location on cluster: `/labs/Wegrzyn/EAB_github/Filtering_Local/genetic_map`

## Order of scripts, located in scripts directory
1. `split_chroms.sh`, allows lepmap3 to be run in parallel
2. `submit_runs.sh`, submits multiple runs of `run_lepmap3.sh`, run the map making software
3. `remove_outliers.sh`, keep the first LG for each chromosome
4. `combine_physical_genetic.py`, From the output directories, create the mapped_markers file which correlates genetic to physical distance
5. `plot.R`, create the GeneticPhysicalCorrelation.pdf visualization from output

## Filtering VCF by genetic map
* If it is necessary to create a VCF that contains only positions that have a corresponding genetic position
* A possible way to do this is on the cluster here `/labs/Wegrzyn/EAB_github/Filtering_Local/filtering/filter_vcf_from_map`
