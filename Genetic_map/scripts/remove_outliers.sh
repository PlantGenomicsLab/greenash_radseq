

for i in {1..23}
do
  linenum=$(( $(grep  -m1 -n '#*** LG = 2' $i/marker_to_genetic.txt | cut -d: -f1) - 1 ))
  echo $linenum
  head -n $linenum $i/marker_to_genetic.txt > $i/marker_to_genetic_firstlg.txt
done
