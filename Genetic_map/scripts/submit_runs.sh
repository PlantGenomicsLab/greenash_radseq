file="run_lepmap3.sh"

start=-2

for i in {1..7}
do
  f=$(( start + 3 ))
  l=$(( $f + 2 ))
  start=$f
  echo "submitting f = $f l = $l"
  sed -i "16c for i in {$f..$l}" $file
  sbatch $file
done

f=$(( l + 1 ))
l=$(( f + 1 ))
echo "submitting f = $f l = $l"
sed -i "16c for i in {$f..$l}" $file
sbatch $file
