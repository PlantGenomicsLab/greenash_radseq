#!/bin/bash
#SBATCH --job-name=default_run
#SBATCH -o %j.out
#SBATCH -e %j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

#module load bcftools
module load vcftools

#vcf=/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/variant_call/filtering/v4/bcf_mendelian/merged.vcf
vcf=imputed.vcf

for i in {1..23}
do
  if [ $i -lt 10 ]
  then
    c="0$i"
  else
    c="$i"
  fi
  mkdir -p $i
#  bcftools view $vcf -o $i/indiv.vcf  -r Chr$c
  vcftools --vcf $vcf --chr Chr$c --recode --recode-INFO-all --out $i/indiv.vcf
done

