library(ggplot2)
library(ggrepel)
library(ggpubr)


chromplot <- function() {
  myPlots = list()

  for (i in seq(1,23)) {
    chromstr=sprintf("Chromosome %d",i)
    
    # marker=linenum, chr, position in bp
    mp = read.csv(file=sprintf("%d/marker_to_physical.txt",i),sep="\t",col.names=c("chr","position_bp"))
    mp$marker = seq.int(nrow(mp))
    
    # marker, position in cM
#    mg = read.csv(file=sprintf("%d/marker_to_genetic.txt",i),sep="\t",col.names=c("marker","position_cm"),comment.char='#')
    mg = read.csv(file=sprintf("%d/marker_to_genetic_firstlg.txt",i),sep="\t",col.names=c("marker","position_cm"),comment.char='#')
    
    mp$position_cm = mg$position_cm[match(mp$marker,mg$marker)]

# isotonic regression
    mp = na.omit(mp)
#    ir = isoreg(mp$position_cm)
#    mp$ir = ir$yf
    
    gg <- ggplot(mp,aes(x=position_bp,y=position_cm)) +
    geom_point(size=2,alpha=0.6) +
    geom_smooth() +
#    geom_line(aes(y=ir),color="darkred") +
    labs(y="Genetic Distance(cM)",x="Physical Position (bp)",title=chromstr)
    
    myPlots[[i]] <- gg
  }

  multi.page <- ggarrange(plotlist = myPlots,nrow=3,ncol=2)
  ggexport(multi.page,filename="GeneticPhysicalCorrelation.pdf")
}

chromplot()

warnings()

