#!/bin/bash
#SBATCH --job-name=default_run
#SBATCH -o %jlod2.out
#SBATCH -e %jlod2.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G


b=/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/RQTL/lepmap3/code/bin/
ped=pedigree.txt

for i in {22..23}
do
  vcf=$i/indiv.vcf.recode.vcf
  java -cp $b ParentCall2 data = $ped  vcfFile = $vcf halfSibs=1 > $i/p.call 2> $i/p.err
  java -cp $b Filtering2 data=$i/p.call  removeNonInformative=1 dataTolerance=0.01 > $i/p_fil.call 2> $i/p_fil.err
  java -cp $b  SeparateChromosomes2 data=$i/p_fil.call lodLimit=8 numThreads=8 > $i/map.txt 2> $i/map.err
  java -cp $b  OrderMarkers2 data=$i/p_fil.call map=$i/map.txt numThreads=8 sexAveraged=1 > $i/order.txt 2> $i/order.err
  cut -f1,2 $i/p.call | tail -n +8 > $i/marker_to_physical.txt
  cut -f1,2 $i/order.txt | tail -n +4 > $i/marker_to_genetic.txt
done



