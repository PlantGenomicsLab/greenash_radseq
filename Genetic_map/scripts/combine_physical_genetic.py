
gen_fn="marker_to_genetic_firstlg.txt"
phy_fn="marker_to_physical.txt"


with open("mapped_markers.txt",'w') as wf:
  for i in range(1,24):
    gen=[]
    phy={}

# Read files
    with open("%d/"%i+gen_fn) as rf:
      for line in rf:
        line = line.strip().split()
        if(line==[]):
          continue
        gen.append(line)
    with open("%d/"%i+phy_fn) as rf:
      ln=1
      for line in rf:
        # Chr#, bp
        line=line.strip().split()
        phy[ln]=line
        ln+=1

# Write combined file, using phy mapping to relate marker# to physical location

    print(gen[0])

    for g in gen:
      mark_num,gen_dist = g
      chr_num,phys_dist = phy[int(mark_num)]
      wr="\t".join([chr_num,phys_dist,gen_dist])+"\n"
      wf.write(wr)

        



