# **Merlin**
1. Run **pedstats** to verify that input files are being interpreted correctly.
```
./pedstats -d phenotype.dat -p phenogenotab2fix2.ped
```
*Pedstats results*
```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File : phenogenotab2fix2.ped (-pname)
                     Data File :   phenotype.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree, --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []


PEDIGREE STRUCTURE 
==================
   Individuals: 76 
      Founders: 76 founders, 0 nonfounders
        Gender: 0 females, 0 males
      Families: 8

  Family Sizes
       Average: 9.50 (3 to 20)
  Distribution: 5 (25.0%), 3 (12.5%) and 7 (12.5%)

  Generations
       Average: 1.00 (1 to 1)
  Distribution: 1 (100.0%), 0 (0.0%) and 2 (0.0%)

Checking family connectedness ... 
 Family 0 (8 individuals) consists of 8 disjoint groups
 Family 1 (9 individuals) consists of 9 disjoint groups
 Family 2 (5 individuals) consists of 5 disjoint groups
 Family 3 (3 individuals) consists of 3 disjoint groups
 Family 4 (5 individuals) consists of 5 disjoint groups
 Family 5 (20 individuals) consists of 20 disjoint groups
 Family 6 (7 individuals) consists of 7 disjoint groups
 Family 7 (19 individuals) consists of 19 disjoint groups
  
Use the --rewritePedigree option to break families into connected groups


QUANTITATIVE TRAIT STATISTICS
=============================

               [All Phenotypes]      Min      Max     Mean      Var  SibCorr
host_kill_perce       73  96.1%    0.000    0.714    0.180    0.037        -
mean_larval_wei       73  96.1%    0.012   58.390    3.415  129.360        -
          Total      146  96.1%


                [Founders Only]      Min      Max     Mean      Var  SibCorr
host_kill_perce       73  96.1%    0.000    0.714    0.180    0.037        -
mean_larval_wei       73  96.1%    0.012   58.390    3.415  129.360        -
          Total      146  96.1%

Switching to summary output mode because there are more than 50 markers.
See file pedstats.markerinfo for detailed marker information.

DATA QUALITY
============

HIGHEST AND LOWEST GENOTYPING RATES BY MARKER

         MARKER   RANK    PROP   N_GENO |        MARKER   RANK    PROP   N_GENO

-------------------------------------------------------------------------------
 Chr06_19457139      1  100.0%       76 | Chr02_16495556  34759    0.0%        0
  Chr17_4564473      2  100.0%       76 |  Chr01_444886  34758    1.3%        1
 Chr06_19457122      3  100.0%       76 |  Chr15_260535  34757    2.6%        2
 Chr13_11769230      4  100.0%       76 | Chr22_11027255  34756    7.9%        6
 Chr06_19456993      5  100.0%       76 | Chr13_9323967  34755    9.2%        7
  Chr17_4564375      6  100.0%       76 | Chr04_31182083  34754   10.5%        8
 Chr06_19456941      7  100.0%       76 | Chr04_31182077  34753   10.5%        8
 Chr06_19129686      8  100.0%       76 | Chr04_1961511  34752   10.5%        8
 Chr06_19129694      9  100.0%       76 | Chr19_10465434  34751   10.5%        8
  Chr17_4564354     10  100.0%       76 | Chr06_22329577  34750   11.8%        9

         Totals 34759   93.5%  2469090


HIGHEST AND LOWEST HETEROZYGOSITIES BY MARKER

         MARKER   RANK     HET   N_GENO |        MARKER   RANK     HET   N_GENO

-------------------------------------------------------------------------------
 Chr04_13362368      1  100.0%       76 | Chr06_13812414  34758    0.0%       69
  Chr16_6918999      2  100.0%       76 | Chr06_12440759  34757    0.0%       75
 Chr05_21212297      3  100.0%       76 | Chr10_13726836  34756    0.0%       71
 Chr14_20496530      4  100.0%       76 | Chr06_11912260  34755    0.0%       71
 Chr05_21212296      5  100.0%       76 | Chr13_11769230  34754    0.0%       76
 Chr04_13362374      6  100.0%       76 | Chr10_13726848  34753    0.0%       53
 Chr10_10853644      7  100.0%       76 | Chr10_13726931  34752    0.0%       29
  Chr09_7786274      8  100.0%       76 | Chr10_13726899  34751    0.0%       29
 Chr10_10853646      9  100.0%       76 | Chr06_14079639  34750    0.0%       71
  Chr11_3856003     10  100.0%       76 | Chr06_14974882  34749    0.0%       69

         Totals 34758   16.2%  2469090


Detailed marker summaries for all 34759 markers written to file pedstats.markerinfo

If you find this program useful in your work, please cite:

	Wigginton, JE and Abecasis, GR (2005) PEDSTATS: descriptive
	statistics, graphics and quality assessment for gene mapping data.
	Bioinformatics. 21(16):3445-3447
```

2. I used the **--rewritePedigree** option to break families into connected groups, as **recommended by the pedstats output**

```
./pedstats -d phenotype.dat -p phenogenotab2fix2.ped --rewritePedigree
```

*Pedstats --rewritePedigree ouput*
```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File : phenogenotab2fix2.ped (-pname)
                     Data File :   phenotype.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree [ON], --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []


PEDIGREE STRUCTURE 
==================
   Individuals: 76 
      Founders: 76 founders, 0 nonfounders
        Gender: 0 females, 0 males
      Families: 8

  Family Sizes
       Average: 9.50 (3 to 20)
  Distribution: 5 (25.0%), 3 (12.5%) and 7 (12.5%)

  Generations
       Average: 1.00 (1 to 1)
  Distribution: 1 (100.0%), 0 (0.0%) and 2 (0.0%)

Checking family connectedness ... 
 Family 0 (8 individuals) consists of 8 disjoint groups
 Family 1 (9 individuals) consists of 9 disjoint groups
 Family 2 (5 individuals) consists of 5 disjoint groups
 Family 3 (3 individuals) consists of 3 disjoint groups
 Family 4 (5 individuals) consists of 5 disjoint groups
 Family 5 (20 individuals) consists of 20 disjoint groups
 Family 6 (7 individuals) consists of 7 disjoint groups
 Family 7 (19 individuals) consists of 19 disjoint groups
  
Use the --rewritePedigree option to break families into connected groups
Rewriting data file as [pedstats.dat]... Done!
Rewriting pedigree file as [pedstats.ped]... Done!


QUANTITATIVE TRAIT STATISTICS
=============================

               [All Phenotypes]      Min      Max     Mean      Var  SibCorr
host_kill_perce       73  96.1%    0.000    0.714    0.180    0.037        -
mean_larval_wei       73  96.1%    0.012   58.390    3.415  129.360        -
          Total      146  96.1%


                [Founders Only]      Min      Max     Mean      Var  SibCorr
host_kill_perce       73  96.1%    0.000    0.714    0.180    0.037        -
mean_larval_wei       73  96.1%    0.012   58.390    3.415  129.360        -
          Total      146  96.1%

Switching to summary output mode because there are more than 50 markers.
See file pedstats.markerinfo for detailed marker information.

DATA QUALITY
============

HIGHEST AND LOWEST GENOTYPING RATES BY MARKER

         MARKER   RANK    PROP   N_GENO |        MARKER   RANK    PROP   N_GENO

-------------------------------------------------------------------------------
 Chr06_19457139      1  100.0%       76 | Chr02_16495556  34759    0.0%        0
  Chr17_4564473      2  100.0%       76 |  Chr01_444886  34758    1.3%        1
 Chr06_19457122      3  100.0%       76 |  Chr15_260535  34757    2.6%        2
 Chr13_11769230      4  100.0%       76 | Chr22_11027255  34756    7.9%        6
 Chr06_19456993      5  100.0%       76 | Chr13_9323967  34755    9.2%        7
  Chr17_4564375      6  100.0%       76 | Chr04_31182083  34754   10.5%        8
 Chr06_19456941      7  100.0%       76 | Chr04_31182077  34753   10.5%        8
 Chr06_19129686      8  100.0%       76 | Chr04_1961511  34752   10.5%        8
 Chr06_19129694      9  100.0%       76 | Chr19_10465434  34751   10.5%        8
  Chr17_4564354     10  100.0%       76 | Chr06_22329577  34750   11.8%        9

         Totals 34759   93.5%  2469090


HIGHEST AND LOWEST HETEROZYGOSITIES BY MARKER

         MARKER   RANK     HET   N_GENO |        MARKER   RANK     HET   N_GENO

-------------------------------------------------------------------------------
 Chr04_13362368      1  100.0%       76 | Chr06_13812414  34758    0.0%       69
  Chr16_6918999      2  100.0%       76 | Chr06_12440759  34757    0.0%       75
 Chr05_21212297      3  100.0%       76 | Chr10_13726836  34756    0.0%       71
 Chr14_20496530      4  100.0%       76 | Chr06_11912260  34755    0.0%       71
 Chr05_21212296      5  100.0%       76 | Chr13_11769230  34754    0.0%       76
 Chr04_13362374      6  100.0%       76 | Chr10_13726848  34753    0.0%       53
 Chr10_10853644      7  100.0%       76 | Chr10_13726931  34752    0.0%       29
  Chr09_7786274      8  100.0%       76 | Chr10_13726899  34751    0.0%       29
 Chr10_10853646      9  100.0%       76 | Chr06_14079639  34750    0.0%       71
  Chr11_3856003     10  100.0%       76 | Chr06_14974882  34749    0.0%       69

         Totals 34758   16.2%  2469090


Detailed marker summaries for all 34759 markers written to file pedstats.markerinfo

If you find this program useful in your work, please cite:

	Wigginton, JE and Abecasis, GR (2005) PEDSTATS: descriptive
	statistics, graphics and quality assessment for gene mapping data.
	Bioinformatics. 21(16):3445-3447

```

3. In the next step, I used the **two resulting output files** from the *--rewritePedigree*: **pedstats.dat y pedstats.ped** to perform a Score test. The **score test** (*--fastAssoc*) is rapid and ideal for screening very large numbers of markers (for example, in a first pass analysis of a genome-wide association (GWA) scan)

```
./merlin -d pedstats.dat -p pedstats.ped -m geneticmaptab.map --fastAssoc
```
**Results**
448 significantly associated SNPs with HKP and 4895 with MLW (p-value<0.05). Also subset those with a p-value<0.01 obtaining 72 and 3188 SNPs, respectively. Output files:

```
merlinoutputHKP.txt
merlinoutputfastAssMLW
merlinoutfastAss.txt
/labs/Wegrzyn/IreneCobo/Fraxinus/HKPsig0.05.txt  /labs/Wegrzyn/IreneCobo/Fraxinus/HKPsig0.01.txt   /labs/Wegrzyn/IreneCobo/Fraxinus/MLWsig0.05.txt /labs/Wegrzyn/IreneCobo/Fraxinus/MLWsig0.01.txt
```

4. Since the results looked nice, I used **the more accurate likelihood-ratio test** (*--assoc*)

```
./merlin -d pedstats.dat -p pedstats.ped -m geneticmaptab.map --assoc --pdf --tabulate
```
Also obtained a high number of significantly associated (pvalue<0.05) SNPs in both traits (5500 in total, 488 HKP (merlinassocHKP0.05.csv) and 5012 MLW (merlinassocMLW0.05.csv), /labs/Wegrzyn/IreneCobo/Fraxinus/Merlinassocsig0.05tab.txt ). PDF with figures: merlin.pdf. 

However, the linkage analysis looked suspicious (/labs/Wegrzyn/IreneCobo/Fraxinus/merlinassocout.txt), given that the program did not manage to calculate the LD for all the SNPs and the LOD, H2 and pvalues were exactly the same in all the available results (LOD=0, H2=33%, pvalue=0.5). 

*likelihood-ratio test (--assoc) output*
```
Phenotype: host_kill_percentage [ASSOC] (73 families, h2 = 50.00%)
===============================================================================
---- LINKAGE TEST RESULTS ----  ----------- ASSOCIATION TEST RESULTS ----------
 Position    H2    LOD  pvalue     Marker Allele  Effect      H2    LOD  pvalue
    0.000  33.3%  0.00    0.50           ---
    1.299  33.3%  0.00    0.50 Chr01_1872      1  -0.003   0.00%   0.00    0.98
                               Chr01_3169      1   0.007   0.02%   0.00    0.91
    2.051  33.3%  0.00    0.50 Chr01_1740      1  -0.035   0.13%   0.02    0.75
    7.971  33.3%  0.00    0.50 Chr01_3181      1   0.096   1.73%   0.21    0.33
                               Chr01_8052      1   0.033   0.92%   0.16    0.39
    8.592  33.3%  0.00    0.50           ---
    9.213  33.3%  0.00    0.50 Chr01_2109      1   0.058   0.48%   0.07    0.56
   10.463  33.3%  0.00    0.50 Chr01_2706      2  -0.140   1.42%   0.46   0.144
                               Chr01_2706      1  -0.008   0.02%   0.00    0.89
```

5. Tried running a **QTL analysis** 

```
./merlin-regress -d pedstats.dat -p pedstats.ped -m geneticmaptab.map -t modelfile.txt
``` 

*QTL analysis output showing* **No results**:

```
Pedigree-Wide Regression Analysis (Model 1)
======================================================
       Position      H2   Stdev    Info     LOD  pvalue
          0.000      na      na      na      na      na
          1.299      na      na      na      na      na
          2.051      na      na      na      na      na
          7.971      na      na      na      na      na
          8.592      na      na      na      na      na
```

I checked the input files and all look nice and run in merlin without giving any errors. It seems like the issue is not format-related but maybe related to the pedigree information provided. 

6. I performed a **variance components analysis** (*--vc*)

**Variance components analysis:**

```
./merlin -d pedstats.dat -p pedstats.ped -m geneticmaptab.map --vc
```
*Results of the variance components analysis*

```
Phenotype: host_kill_percentage [VC] (73 families, h2 = 50.00%)
==================================================================
            Position      H2    ChiSq     LOD  pvalue
               0.000   33.33%    0.00    0.00     0.5
               1.299   33.33%    0.00    0.00     0.5
               2.051   33.33%    0.00    0.00     0.5
               7.971   33.33%    0.00    0.00     0.5
               8.592   33.33%    0.00    0.00     0.5
               9.213   33.33%    0.00    0.00     0.5
              10.463   33.33%    0.00    0.00     0.5
and so on…
Phenotype: mean_larval_weight [VC] (73 families, h2 = 50.00%)
==================================================================
            Position      H2    ChiSq     LOD  pvalue
               0.000   33.33%    0.00    0.00     0.5
               1.299   33.33%    0.00    0.00     0.5
               2.051   33.33%    0.00    0.00     0.5
               7.971   33.33%    0.00    0.00     0.5
               8.592   33.33%    0.00    0.00     0.5
               9.213   33.33%    0.00    0.00     0.5
              10.463   33.33%    0.00    0.00     0.5
and so on…
```

7. Merlin allows **genotype inference** to be decoupled from association tests. To estimate missing genotypes, I used the *--infer* parameter. Details of estimated genotypes will be stored in a pair of files, merlin-infer.dat and merlin-infer.ped. Command line:

```
./merlin -d pedstats.dat -p pedstats.ped -m geneticmaptab.map --infer
```

8. Run again the **QTL analysis using the inferred genotypes** 

```
./merlin-regress -d merlin-infer.dat -p merlin-infer.ped -m geneticmaptab.map -t modelfile.txt
```
**Results**
```
Pedigree-Wide Regression Analysis (Trait: COUNT(1,Chr23_2333704))
======================================================
    Position   H2  Stdev  Info   LOD pvalue
     0.000   na   na   na   na   na
     8.510   na   na   na   na   na
     9.131   na   na   na   na   na
     11.088   na   na   na   na   na
     13.620   na   na   na   na   na
     14.904   na   na   na   na   na
     15.566   na   na   na   na   na
     16.187   na   na   na   na   na
     17.437   na   na   na   na   na
     19.969   na   na   na   na   na
```

9. I performed a **Variance components analysis and** run **pedstats separatedly for each family** (C, D, E, Ha, I, J, K). Same results:

**Table 1.** Statistics for the two traits (host kill percentage HKP and mean larval weight MLW) in all families (Total) and per family

|Family|TRAIT|MEAN|VARIANCE|HERITABILITY|
|---|---|---|---|---|
|Total|host_kill_percentage|0.180|0.037|0.5|
|	|mean_larval_weight|3.145|129.36|0.5|
|C|host_kill_percentage|0.125941414|0.026736603|0.5|
|	|mean_larval_weight|3.393578245|111.7524017|0.5|
|D|host_kill_percentage| 0.19051128|0.017602827|0.5|
|	|mean_larval_weight|8.458728296|425.8086112|0.5|
|E|host_kill_percentage|0.129581818|0.036021951|0.5|
|	|mean_larval_weight|7.580508358|116.8213892|0.5|
|Ha|host_kill_percentage|0.249669986|0.049882945|0.5|
|	|mean_larval_weight|9.318075525|279.3890065|0.5|
|I|host_kill_percentage|0.184332546|0.041672721|0.5|
|	|mean_larval_weight|3.462709309|123.6225487|0.5|
|J|host_kill_percentage| 0.11214596|0.024751591|0.5|
|	|mean_larval_weight|10.25085644|444.8554246|0.5|
|K|host_kill_percentage| 0.189990303|0.042467453|0.5|
|	|mean_larval_weight|2.931079733|80.62588781|0.5|


*Variance components analysis output*
```
Phenotype: host_kill_percentage [VC] (73 families, h2 = 50.00%)
==================================================================
            Position      H2    ChiSq     LOD  pvalue
               0.000   33.33%    0.00    0.00     0.5
               1.299   33.33%    0.00    0.00     0.5
               2.051   33.33%    0.00    0.00     0.5
               7.971   33.33%    0.00    0.00     0.5
               8.592   33.33%    0.00    0.00     0.5
               9.213   33.33%    0.00    0.00     0.5
              10.463   33.33%    0.00    0.00     0.5

and so on…

Phenotype: mean_larval_weight [VC] (73 families, h2 = 50.00%)
==================================================================
            Position      H2    ChiSq     LOD  pvalue
               0.000   33.33%    0.00    0.00     0.5
               1.299   33.33%    0.00    0.00     0.5
               2.051   33.33%    0.00    0.00     0.5
               7.971   33.33%    0.00    0.00     0.5
               8.592   33.33%    0.00    0.00     0.5
               9.213   33.33%    0.00    0.00     0.5
              10.463   33.33%    0.00    0.00     0.5

and so on…
```

- Just notice that the parents were assigned to a different family from the progeny (0), fixed It
- Tried running family K separatedly (individuals 8 and 6 are the parents)
```
./pedstats -d phenotype.dat -p famKfix.ped
```
```
7	8	0	0	1	1/1     1/1     1/1     2/1     2/2    $
7	6	0	0	2	0/0     1/1     1/1     2/2     2/2    $
7	59	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	60	8	6	0	1/1     1/1     0/0     2/1     0/0    $
7	61	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	62	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	63	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	64	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	65	8	6	0	1/1     1/1     0/0     2/1     0/0    $
7	66	8	6	0	1/1     1/1     0/0     2/1     0/0    $
7	67	8	6	0	1/1     1/1     0/0     1/1     0/0    $
7	68	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	69	8	6	0	1/1     1/1     0/0     2/1     0/0    $
7	70	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	71	8	6	0	1/1     1/1     0/0     2/1     0/0    $
7	72	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	73	8	6	0	1/1     1/1     0/0     2/1     0/0    $
7	74	8	6	0	1/1     1/1     0/0     2/2     0/0    $
7	75	8	6	0	1/1     1/1     0/0     2/1     0/0    $
7	76	8	6	0	1/1     1/1     1/1     2/2     2/1    $
7	77	8	6	0	1/1     1/1     0/0     2/2     0/0    $
```
- After fixing it, new error ():
FATAL ERROR - 
Mendelian inheritance errors detected.
- Maybe it is caused by an error during the input file formating steps. I redid it for the family K (I subseted the K family individuals in Xanadu from the original genotype file (geno.txt) and included the phenotypic values and pedigree information manually, to discard possible errors in the coding). Same error:

```
./pedstats -d phenotype.dat -p genoKalltab2.ped
```
```
Chr21_20110543 - Fam 7: Child 13 [G/A] has parents [G/G]*[G/G]
Chr21_20110551 - Fam 7: Child 13 [G/A] has parents [G/G]*[G/G]
Chr21_20351569 - Fam 7: Child 13 [T/T] has parents [C/T]*[C/C]
Chr21_20351569 - Fam 7: Child 18 [T/T] has parents [C/T]*[C/C]
Chr21_20351569 - Fam 7: Child 11 [T/T] has parents [C/T]*[C/C]
Chr21_20351569 - Fam 7: Child 3 [T/T] has parents [C/T]*[C/C]
Chr21_20351569 - Fam 7: Child 7 [T/T] has parents [C/T]*[C/C]

Mendelian inheritance errors detected

FATAL ERROR - 
Mendelian inheritance errors detected
```
- Seems like the issue is located in the original "geno.txt" file