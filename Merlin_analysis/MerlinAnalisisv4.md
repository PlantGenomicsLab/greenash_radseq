Working directory: /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/merlin-1.1.2

1. Run pedstats:

```
./pedstats -d genoMtab.dat -p genov4rep2tab2.ped
```

```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File : genov4rep2tab2.ped (-pname)
                     Data File :    genoMtab.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree, --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []

Parent named 0 for Person 17 in Family 2 is missing
Parent named 0 for Person 18 in Family 2 is missing
Parent named 0 for Person 19 in Family 2 is missing
Parent named 0 for Person 20 in Family 2 is missing
Parent named 0 for Person 21 in Family 2 is missing
Parent named 0 for Person 22 in Family 3 is missing
Parent named 0 for Person 23 in Family 3 is missing
Parent named 0 for Person 24 in Family 3 is missing
Parent named 0 for Person 97 in Family 3 is missing
Parent named 0 for Person 25 in Family 7 is missing
Parent named 0 for Person 26 in Family 7 is missing
Parent named 0 for Person 29 in Family 7 is missing
Parent named 0 for Person 30 in Family 7 is missing
Parent named 0 for Person 32 in Family 7 is missing
Parent named 0 for Person 33 in Family 7 is missing
Parent named 0 for Person 34 in Family 7 is missing
Parent named 0 for Person 35 in Family 7 is missing
Parent named 0 for Person 36 in Family 7 is missing
Parent named 0 for Person 37 in Family 7 is missing
Parent named 0 for Person 39 in Family 7 is missing
Parent named 0 for Person 40 in Family 7 is missing
Parent named 0 for Person 41 in Family 7 is missing
Parent named 0 for Person 42 in Family 7 is missing

FATAL ERROR - 
Please correct problems with pedigree structure
```
The problem is that Merlin does not allow to have individuals with only one parent lacking. It has to be or both parents present or any of them. 

I removed the other parent in those individuals, in a new ped file "genov4rep2tab2x.ped" and I run again pedstats

```
./pedstats -d genoMtab.dat -p genov4rep2tab2x.ped
```

```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File : genov4rep2tab2x.ped (-pname)
                     Data File :    genoMtab.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree, --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []


PEDIGREE STRUCTURE 
==================
   Individuals: 104 
      Founders: 46 founders, 58 nonfounders
        Gender: 8 females, 5 males
      Families: 12

  Family Sizes
       Average: 8.67 (1 to 21)
  Distribution: 1 (8.3%), 2 (8.3%) and 3 (8.3%)

  Generations
       Average: 1.42 (1 to 2)
  Distribution: 1 (58.3%), 2 (41.7%) and 0 (0.0%)

Checking family connectedness ... 
 Family 0 (3 individuals) consists of 3 disjoint groups
 Family 2 (6 individuals) consists of 6 disjoint groups
 Family 3 (5 individuals) consists of 5 disjoint groups
 Family 4 (2 individuals) consists of 2 disjoint groups
 Family 5 (4 individuals) consists of 4 disjoint groups
 Family 7 (15 individuals) consists of 15 disjoint groups
  
Use the --rewritePedigree option to break families into connected groups


QUANTITATIVE TRAIT STATISTICS
=============================

               [All Phenotypes]      Min      Max     Mean      Var  SibCorr
host_kill_perce       96  92.3%    0.000    0.710    0.163    0.036   -0.040
mean_larval_wei       96  92.3%    0.000    0.096    0.047    0.000   -0.047
          Total      192  92.3%


                [Founders Only]      Min      Max     Mean      Var  SibCorr
host_kill_perce       40  87.0%    0.000    0.500    0.128    0.025        -
mean_larval_wei       40  87.0%    0.014    0.096    0.044    0.000        -
          Total       80  87.0%

Switching to summary output mode because there are more than 50 markers.
See file pedstats.markerinfo for detailed marker information.

DATA QUALITY
============

HIGHEST AND LOWEST GENOTYPING RATES BY MARKER

         MARKER   RANK    PROP   N_GENO |        MARKER   RANK    PROP   N_GENO

-------------------------------------------------------------------------------
   Chr11_341317      1  100.0%      104 | Chr05_37594944  43561   22.1%       23
   Chr11_307484      2  100.0%      104 | Chr01_37881693  43560   23.1%       24
   Chr11_341318      3  100.0%      104 | Chr14_18079813  43559   23.1%       24
 Chr05_37999917      4  100.0%      104 | Chr14_18079817  43558   23.1%       24
 Chr01_42496532      5  100.0%      104 | Chr07_2807315  43557   24.0%       25
  Chr09_6458110      6  100.0%      104 | Chr07_2807313  43556   24.0%       25
   Chr11_341320      7  100.0%      104 | Chr02_33705545  43555   24.0%       25
 Chr18_11775452      8  100.0%      104 | Chr07_2807314  43554   24.0%       25
 Chr01_42496438      9  100.0%      104 | Chr07_2807323  43553   24.0%       25
   Chr11_341324     10  100.0%      104 | Chr09_27835077  43552   24.0%       25

         Totals 43561   79.2%  3589947


HIGHEST AND LOWEST HETEROZYGOSITIES BY MARKER

         MARKER   RANK     HET   N_GENO |        MARKER   RANK     HET   N_GENO

-------------------------------------------------------------------------------
  Chr01_9438671      1  100.0%      102 | Chr16_16517228  43561    0.0%       52
 Chr10_10853676      2  100.0%       97 | Chr16_12733618  43560    0.0%       42
  Chr11_3856087      3  100.0%      103 | Chr23_19433261  43559    0.0%       68
  Chr01_9438670      4  100.0%      102 | Chr07_3015289  43558    0.0%       54
  Chr11_3856086      5  100.0%      103 | Chr16_12733557  43557    0.0%       51
  Chr16_6919035      6   99.0%      104 | Chr01_29463165  43556    0.0%       70
  Chr16_6919037      7   99.0%      104 | Chr04_4463873  43555    0.0%       44
  Chr01_4430980      8   99.0%      104 | Chr12_30298652  43554    0.0%       35
   Chr17_569131      9   99.0%      100 | Chr12_30298496  43553    0.0%       35
  Chr12_5329340     10   99.0%      100 | Chr16_12228823  43552    0.0%       90

         Totals 43561   25.7%  3589947


Detailed marker summaries for all 43561 markers written to file pedstats.markerinfo

If you find this program useful in your work, please cite:

	Wigginton, JE and Abecasis, GR (2005) PEDSTATS: descriptive
	statistics, graphics and quality assessment for gene mapping data.
	Bioinformatics. 21(16):3445-3447

```
2. I used the **--rewritePedigree** option to break families into connected groups, **as recommended by the pedstats output**

```
./pedstats -d genoMtab.dat -p genov4rep2tab2x.ped --rewritePedigree
```
```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File : genov4rep2tab2x.ped (-pname)
                     Data File :    genoMtab.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree [ON], --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []


PEDIGREE STRUCTURE 
==================
   Individuals: 104 
      Founders: 46 founders, 58 nonfounders
        Gender: 8 females, 5 males
      Families: 12

  Family Sizes
       Average: 8.67 (1 to 21)
  Distribution: 1 (8.3%), 2 (8.3%) and 3 (8.3%)

  Generations
       Average: 1.42 (1 to 2)
  Distribution: 1 (58.3%), 2 (41.7%) and 0 (0.0%)

Checking family connectedness ... 
 Family 0 (3 individuals) consists of 3 disjoint groups
 Family 2 (6 individuals) consists of 6 disjoint groups
 Family 3 (5 individuals) consists of 5 disjoint groups
 Family 4 (2 individuals) consists of 2 disjoint groups
 Family 5 (4 individuals) consists of 4 disjoint groups
 Family 7 (15 individuals) consists of 15 disjoint groups
  
Use the --rewritePedigree option to break families into connected groups
Rewriting data file as [pedstats.dat]... Done!
Rewriting pedigree file as [pedstats.ped]... Done!


QUANTITATIVE TRAIT STATISTICS
=============================

               [All Phenotypes]      Min      Max     Mean      Var  SibCorr
host_kill_perce       96  92.3%    0.000    0.710    0.163    0.036   -0.040
mean_larval_wei       96  92.3%    0.000    0.096    0.047    0.000   -0.047
          Total      192  92.3%


                [Founders Only]      Min      Max     Mean      Var  SibCorr
host_kill_perce       40  87.0%    0.000    0.500    0.128    0.025        -
mean_larval_wei       40  87.0%    0.014    0.096    0.044    0.000        -
          Total       80  87.0%

Switching to summary output mode because there are more than 50 markers.
See file pedstats.markerinfo for detailed marker information.

DATA QUALITY
============

HIGHEST AND LOWEST GENOTYPING RATES BY MARKER

         MARKER   RANK    PROP   N_GENO |        MARKER   RANK    PROP   N_GENO

-------------------------------------------------------------------------------
   Chr11_341317      1  100.0%      104 | Chr05_37594944  43561   22.1%       23
   Chr11_307484      2  100.0%      104 | Chr01_37881693  43560   23.1%       24
   Chr11_341318      3  100.0%      104 | Chr14_18079813  43559   23.1%       24
 Chr05_37999917      4  100.0%      104 | Chr14_18079817  43558   23.1%       24
 Chr01_42496532      5  100.0%      104 | Chr07_2807315  43557   24.0%       25
  Chr09_6458110      6  100.0%      104 | Chr07_2807313  43556   24.0%       25
   Chr11_341320      7  100.0%      104 | Chr02_33705545  43555   24.0%       25
 Chr18_11775452      8  100.0%      104 | Chr07_2807314  43554   24.0%       25
 Chr01_42496438      9  100.0%      104 | Chr07_2807323  43553   24.0%       25
   Chr11_341324     10  100.0%      104 | Chr09_27835077  43552   24.0%       25

         Totals 43561   79.2%  3589947


HIGHEST AND LOWEST HETEROZYGOSITIES BY MARKER

         MARKER   RANK     HET   N_GENO |        MARKER   RANK     HET   N_GENO

-------------------------------------------------------------------------------
  Chr01_9438671      1  100.0%      102 | Chr16_16517228  43561    0.0%       52
 Chr10_10853676      2  100.0%       97 | Chr16_12733618  43560    0.0%       42
  Chr11_3856087      3  100.0%      103 | Chr23_19433261  43559    0.0%       68
  Chr01_9438670      4  100.0%      102 | Chr07_3015289  43558    0.0%       54
  Chr11_3856086      5  100.0%      103 | Chr16_12733557  43557    0.0%       51
  Chr16_6919035      6   99.0%      104 | Chr01_29463165  43556    0.0%       70
  Chr16_6919037      7   99.0%      104 | Chr04_4463873  43555    0.0%       44
  Chr01_4430980      8   99.0%      104 | Chr12_30298652  43554    0.0%       35
   Chr17_569131      9   99.0%      100 | Chr12_30298496  43553    0.0%       35
  Chr12_5329340     10   99.0%      100 | Chr16_12228823  43552    0.0%       90

         Totals 43561   25.7%  3589947


Detailed marker summaries for all 43561 markers written to file pedstats.markerinfo

If you find this program useful in your work, please cite:

	Wigginton, JE and Abecasis, GR (2005) PEDSTATS: descriptive
	statistics, graphics and quality assessment for gene mapping data.
	Bioinformatics. 21(16):3445-3447
```

As a result, I obtained two new input files: **pedstats.dat** and **pedstats.ped**

3. In the next step, I used the two resulting output files from the --rewritePedigree: **pedstats.dat** and **pedstats.ped** to perform a Score test. The score test (--fastAssoc) is rapid and ideal for screening very large numbers of markers (for example, in a first pass analysis of a genome-wide association (GWA) scan)

```
./merlin -d pedstats.dat -p pedstats.ped -m geneticmaptab.map --fastAssoc
```
Problem with the .map file: 43,561 SNPs in the .ped file but 11537 genetic positions in the map file

Filtered the 43,561 SNPs in the .ped file by the unique genetic positions in the .map file (performed by Jeremy): unique_map.txt and filtered_by_map.vcf (reformatting them into .ped, .dat and .map files needed for Merlin)

Jeremy corrected the problem. I am using the new corrected files: unique_map.txt (map file) and filtered_by_map.vcf (geno). I reformatted them to match the input format requirements for Merlin. For the vcf file, I used "Tassel" to transform it into "geno" file. I sorted its column names (marker names) using Tassel and afterwards, I transformed it into geno format. I used R to "manually" transform it into pedigree format. The map file, I sorted the marker names in alfabetic order and I "manually" reformatted it into the input file format required for Merlin in R. I obtained the .dat file from this .map file as well. 

The reformatted input files: phenogeno.ped, datfiletab.dat and mapfinaltab.map

I redid the analysis

1. Run pedstats
```
./pedstats -d datfiletab.dat -p phenogeno.ped 
```
```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File :   phenogeno.ped (-pname)
                     Data File :  datfiletab.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree, --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []

Parent named 0 for Person 25 in Family 7 is missing
Parent named 0 for Person 26 in Family 7 is missing
Parent named 0 for Person 29 in Family 7 is missing
Parent named 0 for Person 30 in Family 7 is missing
Parent named 0 for Person 32 in Family 7 is missing
Parent named 0 for Person 33 in Family 7 is missing
Parent named 0 for Person 34 in Family 7 is missing
Parent named 0 for Person 35 in Family 7 is missing
Parent named 0 for Person 36 in Family 7 is missing
Parent named 0 for Person 37 in Family 7 is missing
Parent named 0 for Person 39 in Family 7 is missing
Parent named 0 for Person 40 in Family 7 is missing
Parent named 0 for Person 41 in Family 7 is missing
Parent named 0 for Person 42 in Family 7 is missing

FATAL ERROR - 
Please correct problems with pedigree structure
```
Again the same problem, only in family 7. I put "0" in both parents for all individuals belonging to family 7 and I rerun the analysis

```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File :   phenogeno.ped (-pname)
                     Data File :  datfiletab.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree, --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []


PEDIGREE STRUCTURE 
==================
   Individuals: 104 
      Founders: 55 founders, 49 nonfounders
        Gender: 8 females, 5 males
      Families: 12

  Family Sizes
       Average: 8.67 (1 to 21)
  Distribution: 3 (16.7%), 5 (16.7%) and 9 (16.7%)

  Generations
       Average: 1.33 (1 to 2)
  Distribution: 1 (66.7%), 2 (33.3%) and 0 (0.0%)

Checking family connectedness ... 
 Family 0 (3 individuals) consists of 3 disjoint groups
 Family 1 (9 individuals) consists of 9 disjoint groups
 Family 2 (5 individuals) consists of 5 disjoint groups
 Family 3 (6 individuals) consists of 6 disjoint groups
 Family 4 (3 individuals) consists of 3 disjoint groups
 Family 5 (5 individuals) consists of 5 disjoint groups
 Family 7 (15 individuals) consists of 15 disjoint groups
  
Use the --rewritePedigree option to break families into connected groups


QUANTITATIVE TRAIT STATISTICS
=============================

               [All Phenotypes]      Min      Max     Mean      Var  SibCorr
host_kill_perce       96  92.3%    0.000    0.710    0.166    0.036   -0.049
mean_larval_wei       96  92.3%    0.012    0.096    0.048    0.000   -0.031
          Total      192  92.3%


                [Founders Only]      Min      Max     Mean      Var  SibCorr
host_kill_perce       49  89.1%    0.000    0.500    0.127    0.025        -
mean_larval_wei       49  89.1%    0.014    0.096    0.045    0.000        -
          Total       98  89.1%

Switching to summary output mode because there are more than 50 markers.
See file pedstats.markerinfo for detailed marker information.

DATA QUALITY
============

HIGHEST AND LOWEST GENOTYPING RATES BY MARKER

         MARKER   RANK    PROP   N_GENO |        MARKER   RANK    PROP   N_GENO

-------------------------------------------------------------------------------
  Chr08_4329071      1  100.0%      104 |  Chr21_279850   2049   25.0%       26
  Chr15_6074520      2  100.0%      104 | Chr09_27835035   2048   26.0%       27
 Chr19_13440673      3  100.0%      104 | Chr09_1491749   2047   26.0%       27
  Chr08_9128595      4  100.0%      104 | Chr07_2807284   2046   26.0%       27
  Chr08_4861379      5  100.0%      104 | Chr14_20023393   2045   26.9%       28
  Chr15_8041140      6  100.0%      104 | Chr05_22229245   2044   27.9%       29
  Chr22_1604927      7  100.0%      104 | Chr14_4593221   2043   27.9%       29
  Chr08_1301048      8  100.0%      104 | Chr06_9343854   2042   28.8%       30
  Chr08_2077777      9  100.0%      104 | Chr14_15335172   2041   29.8%       31
 Chr20_15241622     10  100.0%      104 | Chr06_16937433   2040   29.8%       31

         Totals  2049   79.3%   168911


HIGHEST AND LOWEST HETEROZYGOSITIES BY MARKER

         MARKER   RANK     HET   N_GENO |        MARKER   RANK     HET   N_GENO

-------------------------------------------------------------------------------
  Chr01_9438671      1  100.0%      102 | Chr10_18549574   2049    0.0%       80
  Chr16_6918999      2   97.1%      104 | Chr02_14791822   2048    0.0%       90
 Chr15_26155994      3   97.0%       99 | Chr10_9227055   2047    0.0%       55
 Chr03_31628892      4   96.9%       96 | Chr15_21152900   2046    0.0%       79
 Chr05_21575253      5   88.3%      103 | Chr16_1580284   2045    0.0%       50
 Chr05_21575295      6   87.4%      103 | Chr03_32311862   2044    0.0%       51
 Chr21_19938216      7   85.5%       55 | Chr05_30060975   2043    0.0%       47
  Chr23_7033478      8   83.7%       86 | Chr11_16770937   2042    0.0%       37
  Chr06_3761530      9   82.7%      104 | Chr20_13822401   2041    0.0%       61
 Chr03_31628868     10   78.1%       96 | Chr05_1359729   2040    0.0%       55

         Totals  2049   24.7%   168911


Detailed marker summaries for all 2049 markers written to file pedstats.markerinfo

If you find this program useful in your work, please cite:

	Wigginton, JE and Abecasis, GR (2005) PEDSTATS: descriptive
	statistics, graphics and quality assessment for gene mapping data.
	Bioinformatics. 21(16):3445-3447

```

2. I used the --rewritePedigree option to break families into connected groups, as recommended by the output

```
./pedstats -d datfiletab.dat -p phenogeno.ped --rewritePedigree
```
```
Pedigree Statistics - 0.6.10
(c) 1999-2006 Goncalo Abecasis, 2002-2006 Jan Wigginton

The following parameters are in effect:
                 Pedigree File :   phenogeno.ped (-pname)
                     Data File :  datfiletab.dat (-dname)
                      IBD File :    pedstats.ibd (-iname)
                Adobe PDF File :    pedstats.pdf (-aname)
            Missing Value Code :         -99.999 (-xname)

Additional Options
    Pedigree File : --ignoreMendelianErrors, --chromosomeX, --trim
   Hardy-Weinberg : --hardyWeinberg, --showAll, --cutoff [0.05]
        HW Sample : --checkFounders, --checkAll, --checkUnrelated
           Output : --pairs, --rewritePedigree [ON], --markerTables, --verbose
         Grouping : --bySex, --byFamily
     Age Checking : --age [], --birth []
      Generations : --minGap [13.00], --maxGap [70.00], --sibGap [30.00]
      PDF Options : --pdf, --familyPDF, --traitPDF, --affPDF, --markerPDF
           Filter : --minGenos, --minPhenos, --minCovariates, --affectedFor []


PEDIGREE STRUCTURE 
==================
   Individuals: 104 
      Founders: 55 founders, 49 nonfounders
        Gender: 8 females, 5 males
      Families: 12

  Family Sizes
       Average: 8.67 (1 to 21)
  Distribution: 3 (16.7%), 5 (16.7%) and 9 (16.7%)

  Generations
       Average: 1.33 (1 to 2)
  Distribution: 1 (66.7%), 2 (33.3%) and 0 (0.0%)

Checking family connectedness ... 
 Family 0 (3 individuals) consists of 3 disjoint groups
 Family 1 (9 individuals) consists of 9 disjoint groups
 Family 2 (5 individuals) consists of 5 disjoint groups
 Family 3 (6 individuals) consists of 6 disjoint groups
 Family 4 (3 individuals) consists of 3 disjoint groups
 Family 5 (5 individuals) consists of 5 disjoint groups
 Family 7 (15 individuals) consists of 15 disjoint groups
  
Use the --rewritePedigree option to break families into connected groups
Rewriting data file as [pedstats.dat]... Done!
Rewriting pedigree file as [pedstats.ped]... Done!


QUANTITATIVE TRAIT STATISTICS
=============================

               [All Phenotypes]      Min      Max     Mean      Var  SibCorr
host_kill_perce       96  92.3%    0.000    0.710    0.166    0.036   -0.049
mean_larval_wei       96  92.3%    0.012    0.096    0.048    0.000   -0.031
          Total      192  92.3%


                [Founders Only]      Min      Max     Mean      Var  SibCorr
host_kill_perce       49  89.1%    0.000    0.500    0.127    0.025        -
mean_larval_wei       49  89.1%    0.014    0.096    0.045    0.000        -
          Total       98  89.1%

Switching to summary output mode because there are more than 50 markers.
See file pedstats.markerinfo for detailed marker information.

DATA QUALITY
============

HIGHEST AND LOWEST GENOTYPING RATES BY MARKER

         MARKER   RANK    PROP   N_GENO |        MARKER   RANK    PROP   N_GENO

-------------------------------------------------------------------------------
  Chr08_4329071      1  100.0%      104 |  Chr21_279850   2049   25.0%       26
  Chr15_6074520      2  100.0%      104 | Chr09_27835035   2048   26.0%       27
 Chr19_13440673      3  100.0%      104 | Chr09_1491749   2047   26.0%       27
  Chr08_9128595      4  100.0%      104 | Chr07_2807284   2046   26.0%       27
  Chr08_4861379      5  100.0%      104 | Chr14_20023393   2045   26.9%       28
  Chr15_8041140      6  100.0%      104 | Chr05_22229245   2044   27.9%       29
  Chr22_1604927      7  100.0%      104 | Chr14_4593221   2043   27.9%       29
  Chr08_1301048      8  100.0%      104 | Chr06_9343854   2042   28.8%       30
  Chr08_2077777      9  100.0%      104 | Chr14_15335172   2041   29.8%       31
 Chr20_15241622     10  100.0%      104 | Chr06_16937433   2040   29.8%       31

         Totals  2049   79.3%   168911


HIGHEST AND LOWEST HETEROZYGOSITIES BY MARKER

         MARKER   RANK     HET   N_GENO |        MARKER   RANK     HET   N_GENO

-------------------------------------------------------------------------------
  Chr01_9438671      1  100.0%      102 | Chr10_18549574   2049    0.0%       80
  Chr16_6918999      2   97.1%      104 | Chr02_14791822   2048    0.0%       90
 Chr15_26155994      3   97.0%       99 | Chr10_9227055   2047    0.0%       55
 Chr03_31628892      4   96.9%       96 | Chr15_21152900   2046    0.0%       79
 Chr05_21575253      5   88.3%      103 | Chr16_1580284   2045    0.0%       50
 Chr05_21575295      6   87.4%      103 | Chr03_32311862   2044    0.0%       51
 Chr21_19938216      7   85.5%       55 | Chr05_30060975   2043    0.0%       47
  Chr23_7033478      8   83.7%       86 | Chr11_16770937   2042    0.0%       37
  Chr06_3761530      9   82.7%      104 | Chr20_13822401   2041    0.0%       61
 Chr03_31628868     10   78.1%       96 | Chr05_1359729   2040    0.0%       55

         Totals  2049   24.7%   168911


Detailed marker summaries for all 2049 markers written to file pedstats.markerinfo

If you find this program useful in your work, please cite:

	Wigginton, JE and Abecasis, GR (2005) PEDSTATS: descriptive
	statistics, graphics and quality assessment for gene mapping data.
	Bioinformatics. 21(16):3445-3447
```

3. In the next step, I used the two resulting output files from the --rewritePedigree: **pedstats.dat** and **pedstats.ped** to perform a Score test. The score test (--fastAssoc) is rapid and ideal for screening very large numbers of markers (for example, in a first pass analysis of a genome-wide association (GWA) scan)

New working directory: /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/Goodfiles

```
./merlin -d pedstats.dat -p pedstats.ped -m mapfinaltab.map --fastAssoc
```
The output is in /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/Goodfiles/fastAssocResults.txt

4. Since the results looked nice (there were significantly associated SNPs with both HKP and MLW (p-value<0.05).), I used the **more accurate likelihood-ratio test (--assoc)**

```
./merlin -d pedstats.dat -p pedstats.ped -m mapfinaltab.map --assoc --pdf --tabulate
```
The results were stored in /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/Goodfiles/AssocResults.txt

5. Since the results looked fine, I tried running a QTL analysis, using "merlin-regress". MERLIN-REGRESS implements an extension of the Haseman-Elston quantitative trait linkage analysis procedure that extracts linkage information from trait squared-sums and differences from all non-inbred relative pairs. For a detailed analytical description of this approach, please see the manuscript by [Sham et al. (2000)](http://www.sph.umich.edu/csg/abecasis/Merlin/reference.html). This regression approach provides a powerful quantitative trait linkage test even in selected samples, but requires specification of the trait mean, variance and covariances between different relative pairs. The present implementation derives covariances between different types of relative pairs from their kinship coefficients and an estimate of the trait heritability. 

The working directory: /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/merlin-1.1.2

```
./merlin-regress -d pedstats.dat -p pedstats.ped -m mapfinaltab.map -t modelfile.txt
```
Results in /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/merlin-1.1.2/QTLresults.txt

**Any of the P-values was statistically significant for any loci**. All p-values were equal to 0.5 or 0.4

5. Merlin allows genotype inference to be decoupled from association tests. To estimate missing genotypes, I used the --infer parameter. Details of estimated genotypes will be stored in a pair of files, merlin-infer.dat and merlin-infer.ped. In the inferred pedigree file (saved as merlin-infer.ped here), each locus is described in 5 columns: the most likely genotype for the missing genotype, the expectation of count for the tested allele, and the posterior probabilities for three genotypes. In the inferred data file (saved as merlin-infer.dat here), 5 rows corresponding to the 5 columns in the pedigree file are labeled as M, T, C, C, and C type respectively. The posterior probabilities can help infer a missing genotype: if one of the three posterior probabilities is large enough, then the missing genotype can be inferred as the genotype with the highest posterior probability; otherwise, if none of the posterior probabilities is large enough, then the missing genotype remains uninferred. In order to control the error rate of genotype inference, a high cutoff value, say, .99, is usually chosen for the posterior probabilities, so that the missing error rate is controlled to be less than 1%. 

Working directory: /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/Goodfiles. Command line:

```
./merlin -d pedstats.dat -p pedstats.ped -m mapfinaltab.map --infer
```

As it can be observed in the merlin-infer.dat file, only the lacking genotypes from the Chr23 were infered, even though lacking genotypes can be observed in other chromosomes, such as Chr01 (see pedstats.ped). Thus, the merlin-infer.ped and merlin-infer.dat, contained the infered lacking genotypes for the 55 loci of the chomosome 23. 

I got the first column per loci (the most likely genotype for the missing genotype) from the merlin-infer.ped file and I included it in the pedstats.ped file, substituting the entire chromosome 23 in this pedstats.ped file by the infered genotypes form the merlin-infer.ped of the chromosome 23. Since the order of the loci of this infered genotypes was different from the order of the same loci in the map file (mapfinaltab.map) and dat file (pedstats.dat), I put the loci names of the chr23 in the correct order in both files (map and dat files). The new names for the new ped, dat and map files including the infered genotypes were infered.ped, infered.dat and infered.map, respectively.

6. Run again the QTL analysis using the new files with inferred genotypes (Working directory: /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/merlin-1.1.2)

```
./merlin-regress -d inferred.dat -p inferred.ped -m inferred.map -t modelfile.txt
```
Results of the QTL on the inferred genotypes: /labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/Merlin/Fraxinus/v4final/merlin-1.1.2/QTLresultsinferchr23.txt 

**Again, no statistically significant associations. All p-values were equal to 0.5 or 0.4**

**LINKAGE ANALYSIS FOR SMALL FAMILIES IN MERLIN:**

1. First, run pedstats

```
pedstats -d asp.dat -p asp.ped
```

2. Non-parametric linkage analysis (why non-parametric: In these methods we assume that we know the genetic distances (and hence recombination fractions) between the markers. For parametric linkage analysis, we will also assume we know the underlying disease model (e.g. recessive, dominant etc). We fix a position for the disease locus and calculate the overall likelihood for the disease and marker data, assuming the disease locus position is correct. We then repeat the analysis with the disease locus positioned at different locations in relation to the known markers. In this way we construct a multipoint LOD score curve across the region: the position where the LOD score is maximum is the best estimate of the disease locus location)

```
merlin -d asp.dat -p asp.ped -m asp.map --pairs --npl
```

**TIP**:The standard non-parametric linkage analysis carried out by Merlin uses the Kong and Cox (1997) linear model to evaluate the evidence for linkage. This model is designed to identify small increases in allele sharing spread across a large number of families -- this is what one usually expects in a complex disease. If you are searching for a large increase in allele sharing in a small number of families, you can select the Kong and Cox (1997) exponential model by adding the **--exp** option to your command line, after the **--npl** or **--pairs** options. This alternative model is more computationally intensive and requires more memory, but provides a better linkage test if you expect a large increase in allele sharing among affected individuals.
