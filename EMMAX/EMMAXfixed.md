Working directory: /labs/Wegrzyn/IreneCobo/Fraxinus/v1.4
Reference article: 

EMMAX program used is a linear mixed model (LMM) to perform GWAS, correcting by a wide range of sample structures (which encompasses population stratification and hidden relatedness). To this end, it uses an internally calculated kinship matrix as a random effect, and population structure externally calculated as a fixed effect (Kang et al. 2010)[https://www.nature.com/articles/ng.548#Sec9]. 

Tutorial: https://genome.sph.umich.edu/wiki/EMMAX#IBS_matrix_or_BN_matrix.3F 
Why EMMAX is suitable for performing family-based GWAS on quantitative traits (see the conclusion): https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4102448/ . Another advantage of EMMAX is that it allows including imputed SNPs in the analysis

Other interesting information: https://stats.stackexchange.com/questions/223797/beta-values-for-mixed-models (fixed and random effect definition, roughly)

1. Perform linkage disequilibrium filtering using plink to retain only independent SNPs for DAPC analysis: 
```
plink --vcf output.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --indep-pairwise 50 5 0.2 --recode vcf --out outputLD0.2.vcf
```
2. Perform DAPC analysis in R (in addition, a PCA analysis was also performed)
```
library(adegenet)
library(vcfR)
#Usando solo un subset del panel2 para probar

#The only input file needed is a vcf or vcf.gz file
vcf <- read.vcfR("outputLD0.2.vcf")
x <- vcfR2genlight(vcf) #transform vcf to genlight
x
grp <- find.clusters(x, max.n.clust=40)#97 number of PCs, 7 number of clusters
dapc1 <- dapc(x, grp$grp) #70 PCs retained, 5 discrmininant functions. DAPC is implemented by the function "dapc", which first transforms the data using PCA, and then performs a Discriminant Analysis on the retained principal components.Two graphs will appear as a result to help decide the number of principal components and the number of discriminant functions to be retained  
dapc1
dapc1$assign
scatter(dapc1)#plot the groups in a scatterplot and the DA eigenvalues
#The scatterplot can be customized (dot size)
scatter(dapc1, cell = 0, pch = 18:23, cstar = 0, mstree = TRUE, lwd = 2, lty = 2)
#position of the DA eigenvalues plot
scatter(dapc1, posi.da="bottomleft", bg="white", pch=17:22)

#Change of colors, position of the PCA eigenvalues plot 
myCol <- c("darkblue","purple","green","orange","red","blue")
scatter(dapc1, posi.da="bottomleft", bg="white", legend = TRUE,
        pch=17:22, cstar=0, col=myCol, scree.pca=TRUE,
        posi.pca="bottomright")

scatter(dapc1, posi.pca="bottomright", posi.da="bottomleft",scree.da=FALSE, bg="white", pch=20, cell=0, cstar=0, col=myCol, solid=.4,
        cex=3,clab=0, leg=TRUE, txt.leg=paste("Cluster",1:6))

scatter(dapc1, ratio.pca=0.3, bg="white", pch=20, cell=0,
        cstar=0, col=myCol, solid=.4, cex=3, clab=0,
        mstree=TRUE, scree.da=FALSE, posi.pca="bottomright", posi.da="bottomleft",
        leg=TRUE, txt.leg=paste("Cluster",1:6))
par(xpd=TRUE)
points(dapc1$grp.coord[,1], dapc1$grp.coord[,2], pch=4,
       cex=3, lwd=8, col="black")
points(dapc1$grp.coord[,1], dapc1$grp.coord[,2], pch=4,
       cex=3, lwd=2, col=myCol)
myInset <- function(){
  temp <- dapc1$pca.eig
  temp <- 100* cumsum(temp)/sum(temp)
  plot(temp, col=rep(c("black","lightgrey"),
                     c(dapc1$n.pca,1000)), ylim=c(0,100),
       xlab="PCA axis", ylab="Cumulated variance (%)",
       cex=1, pch=20, type="h", lwd=2)
}
add.scatter(myInset(), posi="bottomright",
            inset=c(-0.03,-0.01), ratio=.28,
            bg=transp("white"))
#Membership probabilities
#Exploration
class(dapc1$posterior)
dim(dapc1$posterior)
round(head(dapc1$posterior),3)
str(dapc1$posterior)
head(dapc1$posterior)

#Change formatting for storage and used to correct by population structure in the GWAS
round<-round(dapc1$posterior,3)
head(round)
tail(round)
str(round)
assignedPop<-apply(round, 1, which.max)
head(assignedPop)
assignedPopframe<-as.data.frame(assignedPop)
head(assignedPopframe)
str(assignedPopframe)
write.csv(assignedPopframe, "assignedPopFraxinusDAPCbueno.csv")

save(x, vcf, grp, dapc1, file = "DAPC_Fraxinus.RData") 
```

![DAPCplot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/DAPCplot.png/ "DAPCplot")
![PCAplot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/PCAplot.png/ "PCAplot")
![DAPCplotfamilies](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/DAPCplotfamilies.png/ "DAPCplotfamilies")


3. Transform the .vcf file to plink format (.ped) using vcftools and the following code:

```
vcftools --vcf output.vcf --plink --out genotype
```
4. Use PLINK software to transpose your genotype files (bed or ped format) to tped/tfam format by running 

```
plink --file genotype --recode12 --output-missing-genotype 0 --transpose --out genotype
```

5. Perform BN matrix in EMMAX (follow this tutorial: http://genetics.cs.ucla.edu/emmax/install.html)
```
emmax-kin -v -h -d 10 genotype
```


6. Run EMMAX

First, using HKP phenotype. The file containing the phenotypes and the assigned populations (DAPC) has to be the same order of individuals that the .tfam file (see tutorial: http://genetics.cs.ucla.edu/emmax/install.html)
```
./emmax-intel64 -v -d 10 -t genotype -p phenoEMMAXorderHKP.txt -c assignedPoporder.txt -k output.aBN.kinf -o HKPassoc
```
I obtained this error:

```
Reading TFAM file genotype.tfam ....


Reading kinship file output.aBN.kinf...

  98 rows and 98 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderHKP.txt...

  98 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPoporder.txt...

  98 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = nan, evals[1] = nan, evals[n-1] = nan
FATAL ERROR : Minimum q eigenvalues of SKS is supposed to be close to zero, but actually 179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368.000000
FATAL ERROR : Minimum q eigenvalues of SKS is supposed to be close to zero, but actually 179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368.000000
FATAL ERROR : k = 90 > n-q = 89
Aborted
```
Maybe I have to perform the kinship matrix using the LD=0.2 

It did it and it worked

1. Use vcftools to transform the vcf file into ped format

```
vcftools --vcf outputLD0.2.vcf --plink --out genotypeLD0.2
```

2. Transpose the LD0.2 filtered genotypes into tped

```
plink --file genotypeLD0.2 --recode12 --output-missing-genotype 0 --transpose --out genotypeLD0.2
```

3. Perform kinship matrix based on the LD0.2 filtered genotypes

```
./emmax-kin-intel64 -v -d 10 genotypeLD0.2
```

I plotted the kinship matrix using a heatmap. To make it clearer, I ordered the individuals by family (following the order of the file: IDorderfamHeatmap.csv). The name of the kinship matrix with the individuals ordered by family, used to perform this heatmap, is: genotypeLD0.2orderfam.aBN.kinf

![HeatmapKinshipmatrix](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/HeatmapKinshipmatrix.png/ "HeatmapKinshipmatrix")

4. Run EMMAX

```
./emmax-intel64 -v -d 10 -t genotype -p phenoEMMAXorderHKP.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o HKPassoc
```
```
Reading TFAM file genotype.tfam ....


Reading kinship file genotypeLD0.2.aBN.kinf...

  98 rows and 98 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderHKP.txt...

  98 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPoporder.txt...

  98 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 0.000000, evals[n-1] = 9.837129
k = 89,  n-q = 89
eRvals[0] = 0.001739, eRvals[n-q-1] = 8.837129, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.070000
etas[0] = -0.315652, lambdas[0] = 0.001739
etas[1] = -0.054177, lambdas[1] = 0.072227
etas[2] = -0.426081, lambdas[2] = 0.078082
etas[3] = -0.057421, lambdas[3] = 0.093046
etas[4] = -0.021050, lambdas[4] = 0.100162
etas[5] = -0.075209, lambdas[5] = 0.102971
etas[6] = -0.124940, lambdas[6] = 0.113387
etas[7] = 0.061841, lambdas[7] = 0.113773
etas[8] = 0.103734, lambdas[8] = 0.115704
etas[9] = 0.173737, lambdas[9] = 0.117599
etas[10] = 0.326621, lambdas[10] = 0.121784
etas[11] = -0.094288, lambdas[11] = 0.122932
etas[12] = -0.033726, lambdas[12] = 0.130842
etas[13] = 0.369738, lambdas[13] = 0.131704
etas[14] = 0.168202, lambdas[14] = 0.137352
etas[15] = -0.214602, lambdas[15] = 0.138849
etas[16] = 0.128880, lambdas[16] = 0.139634
etas[17] = 0.258590, lambdas[17] = 0.142860
etas[18] = 0.090786, lambdas[18] = 0.145998
etas[19] = 0.148459, lambdas[19] = 0.150944
etas[20] = -0.262301, lambdas[20] = 0.153487
etas[21] = -0.157324, lambdas[21] = 0.157099
etas[22] = -0.199199, lambdas[22] = 0.158876
etas[23] = -0.099513, lambdas[23] = 0.161960
etas[24] = -0.003882, lambdas[24] = 0.165483
etas[25] = -0.040198, lambdas[25] = 0.168917
etas[26] = 0.201051, lambdas[26] = 0.171542
etas[27] = 0.071358, lambdas[27] = 0.173888
etas[28] = 0.064352, lambdas[28] = 0.174839
etas[29] = -0.215310, lambdas[29] = 0.179624
etas[30] = -0.091099, lambdas[30] = 0.184958
etas[31] = 0.304442, lambdas[31] = 0.188216
etas[32] = -0.064911, lambdas[32] = 0.191419
etas[33] = 0.190234, lambdas[33] = 0.192816
etas[34] = 0.103330, lambdas[34] = 0.195691
etas[35] = 0.136705, lambdas[35] = 0.197462
etas[36] = -0.011828, lambdas[36] = 0.200206
etas[37] = 0.346754, lambdas[37] = 0.206067
etas[38] = 0.176033, lambdas[38] = 0.213453
etas[39] = 0.482828, lambdas[39] = 0.218856
etas[40] = 0.016329, lambdas[40] = 0.219378
etas[41] = -0.073551, lambdas[41] = 0.223947
etas[42] = -0.152376, lambdas[42] = 0.224511
etas[43] = 0.029397, lambdas[43] = 0.229249
etas[44] = 0.146075, lambdas[44] = 0.235044
etas[45] = 0.093453, lambdas[45] = 0.236764
etas[46] = -0.123453, lambdas[46] = 0.243689
etas[47] = 0.264891, lambdas[47] = 0.246064
etas[48] = -0.247395, lambdas[48] = 0.253058
etas[49] = -0.336422, lambdas[49] = 0.264030
etas[50] = -0.057490, lambdas[50] = 0.264675
etas[51] = -0.177204, lambdas[51] = 0.267538
etas[52] = -0.316064, lambdas[52] = 0.273297
etas[53] = -0.082431, lambdas[53] = 0.274399
etas[54] = -0.108032, lambdas[54] = 0.282197
etas[55] = 0.214938, lambdas[55] = 0.285912
etas[56] = -0.039426, lambdas[56] = 0.296841
etas[57] = 0.186430, lambdas[57] = 0.302922
etas[58] = -0.343943, lambdas[58] = 0.306037
etas[59] = -0.192374, lambdas[59] = 0.311813
etas[60] = -0.163589, lambdas[60] = 0.319381
etas[61] = 0.010740, lambdas[61] = 0.330782
etas[62] = -0.556479, lambdas[62] = 0.333075
etas[63] = 0.089044, lambdas[63] = 0.337674
etas[64] = 0.072235, lambdas[64] = 0.338544
etas[65] = -0.002799, lambdas[65] = 0.347533
etas[66] = -0.141187, lambdas[66] = 0.354333
etas[67] = 0.031817, lambdas[67] = 0.361156
etas[68] = 0.058357, lambdas[68] = 0.372229
etas[69] = 0.321826, lambdas[69] = 0.384487
etas[70] = -0.151091, lambdas[70] = 0.407359
etas[71] = -0.167886, lambdas[71] = 0.415695
etas[72] = 0.092070, lambdas[72] = 0.433998
etas[73] = -0.076516, lambdas[73] = 0.443825
etas[74] = -0.363125, lambdas[74] = 0.447950
etas[75] = -0.051222, lambdas[75] = 0.461880
etas[76] = 0.169765, lambdas[76] = 0.469867
etas[77] = 0.290933, lambdas[77] = 0.485169
etas[78] = -0.165044, lambdas[78] = 0.500579
etas[79] = -0.188311, lambdas[79] = 0.529156
etas[80] = 0.100641, lambdas[80] = 0.536558
etas[81] = 0.027162, lambdas[81] = 0.586429
etas[82] = -0.108644, lambdas[82] = 0.818823
etas[83] = -0.207429, lambdas[83] = 1.014345
etas[84] = 0.202227, lambdas[84] = 1.034352
etas[85] = -0.015075, lambdas[85] = 1.754933
etas[86] = 0.009594, lambdas[86] = 3.086517
etas[87] = -0.020278, lambdas[87] = 4.635223
etas[88] = 0.138350, lambdas[88] = 8.837129
0	19121.837465	18990.796117	-55.230791
1	18990.796117	18832.803034	-55.039244
2	18832.803034	18642.841181	-54.807064
3	18642.841181	18415.201289	-54.526088
4	18415.201289	18143.498324	-54.186730
5	18143.498324	17820.749763	-53.777827
6	17820.749763	17439.541690	-53.286519
7	17439.541690	16992.311655	-52.698191
8	16992.311655	16471.776030	-51.996511
9	16471.776030	15871.521054	-51.163598
10	15871.521054	15186.757442	-50.180406
11	15186.757442	14415.205807	-49.027334
12	14415.205807	13558.034927	-47.685151
13	13558.034927	12620.723508	-46.136224
14	12620.723508	11613.672419	-44.366056
15	11613.672419	10552.378508	-42.365037
16	10552.378508	9457.012824	-40.130264
17	9457.012824	8351.334577	-37.667211
18	8351.334577	7261.005710	-34.990978
19	7261.005710	6211.514652	-32.126847
20	6211.514652	5226.024420	-29.109942
21	5226.024420	4323.490873	-25.983877
22	4323.490873	3517.339941	-22.798460
23	3517.339941	2814.868836	-19.606640
24	2814.868836	2217.387024	-16.461064
25	2217.387024	1720.980668	-13.410630
26	1720.980668	1317.697818	-10.497472
27	1317.697818	996.920471	-7.754714
28	996.920471	746.708064	-5.205209
29	746.708064	554.950025	-2.861319
30	554.950025	410.234332	-0.725651
31	410.234332	302.406667	1.207479
32	302.406667	222.847582	2.950117
33	222.847582	164.526612	4.518562
34	164.526612	121.903024	5.931332
35	121.903024	90.738196	7.207390
36	90.738196	67.871411	8.364743
37	67.871411	50.995194	9.419488
38	50.995194	38.452064	10.385288
39	38.452064	29.063457	11.273243
40	29.063457	21.993802	12.092078
41	21.993802	16.647757	12.848531
42	16.647757	12.595969	13.547849
43	12.595969	9.523527	14.194272
44	9.523527	7.195348	14.791451
45	7.195348	5.433374	15.342730
46	5.433374	4.101536	15.851310
47	4.101536	3.095576	16.320296
48	3.095576	2.335787	16.752663
49	2.335787	1.761555	17.151194
50	1.761555	1.327072	17.518404
51	1.327072	0.997917	17.856493
52	0.997917	0.748320	18.167325
53	0.748320	0.559013	18.452445
54	0.559013	0.415562	18.713120
55	0.415562	0.307109	18.950415
56	0.307109	0.225430	19.165266
57	0.225430	0.164244	19.358569
58	0.164244	0.118719	19.531252
59	0.118719	0.085116	19.684329
60	0.085116	0.060529	19.818937
61	0.060529	0.042707	19.936342
62	0.042707	0.029909	20.037923
63	0.029909	0.020804	20.125143
64	0.020804	0.014383	20.199499
65	0.014383	0.009890	20.262478
66	0.009890	0.006770	20.315517
67	0.006770	0.004616	20.359962
68	0.004616	0.003137	20.397048
69	0.003137	0.002126	20.427883
70	0.002126	0.001438	20.453445
71	0.001438	0.000971	20.474586
72	0.000971	0.000655	20.492035
73	0.000655	0.000441	20.506415
74	0.000441	0.000297	20.518250
75	0.000297	0.000199	20.527981
76	0.000199	0.000134	20.535975
77	0.000134	0.000090	20.542538
78	0.000090	0.000060	20.547924
79	0.000060	0.000041	20.552342
80	0.000041	0.000027	20.555963
81	0.000027	0.000018	20.558932
82	0.000018	0.000012	20.561366
83	0.000012	0.000008	20.563359
84	0.000008	0.000006	20.564993
85	0.000006	0.000004	20.566331
86	0.000004	0.000002	20.567427
87	0.000002	0.000002	20.568324
88	0.000002	0.000001	20.569059
89	0.000001	0.000001	20.569661
90	0.000001	0.000001	20.570154
91	0.000001	0.000000	20.570558
92	0.000000	0.000000	20.570888
93	0.000000	0.000000	20.571159
94	0.000000	0.000000	20.571381
95	0.000000	0.000000	20.571562
96	0.000000	0.000000	20.571711
97	0.000000	0.000000	20.571832
98	0.000000	0.000000	20.571932
99	0.000000	0.000000	20.572013
REMLE (delta=22026.465795, REML=20.572080, REML0=20.572382)- elapsed CPU time is 0.000000
eigen_L - elapsed CPU time is 0.000000


Reading TPED file genotype...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

GLS association - elapsed CPU time is 0.900000

Breakdown for input file parsing : 0.320000

              matrix computation : 0.290000

             p-value computation : 0.030000
```

Results are in: /labs/Wegrzyn/IreneCobo/Fraxinus/v1.4/HKPassoc.ps 

Each line consist of
1. SNP ID
2. Beta (1 is effect allele)
3. SE(beta)
4. p-value.

5. Run EMMAX again with the other phenotype MLW

```
./emmax-intel64 -v -d 10 -t genotype -p phenoEMMAXorderMLW.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o MLWassoc
```
```

Reading TFAM file genotype.tfam ....


Reading kinship file genotypeLD0.2.aBN.kinf...

  98 rows and 98 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderMLW.txt...

  98 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPoporder.txt...

  98 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 0.000000, evals[n-1] = 9.837129
k = 89,  n-q = 89
eRvals[0] = 0.001739, eRvals[n-q-1] = 8.837129, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.000000
etas[0] = -0.006225, lambdas[0] = 0.001739
etas[1] = 0.005027, lambdas[1] = 0.072227
etas[2] = -0.005289, lambdas[2] = 0.078082
etas[3] = 0.001020, lambdas[3] = 0.093046
etas[4] = 0.017039, lambdas[4] = 0.100162
etas[5] = 0.020203, lambdas[5] = 0.102971
etas[6] = 0.013320, lambdas[6] = 0.113387
etas[7] = -0.013015, lambdas[7] = 0.113773
etas[8] = -0.019691, lambdas[8] = 0.115704
etas[9] = -0.020489, lambdas[9] = 0.117599
etas[10] = 0.017634, lambdas[10] = 0.121784
etas[11] = 0.040536, lambdas[11] = 0.122932
etas[12] = 0.003273, lambdas[12] = 0.130842
etas[13] = -0.009004, lambdas[13] = 0.131704
etas[14] = -0.006958, lambdas[14] = 0.137352
etas[15] = 0.024633, lambdas[15] = 0.138849
etas[16] = -0.004962, lambdas[16] = 0.139634
etas[17] = -0.034625, lambdas[17] = 0.142860
etas[18] = -0.008261, lambdas[18] = 0.145998
etas[19] = -0.028569, lambdas[19] = 0.150944
etas[20] = 0.022889, lambdas[20] = 0.153487
etas[21] = 0.021482, lambdas[21] = 0.157099
etas[22] = -0.013524, lambdas[22] = 0.158876
etas[23] = -0.004669, lambdas[23] = 0.161960
etas[24] = 0.015199, lambdas[24] = 0.165483
etas[25] = 0.016806, lambdas[25] = 0.168917
etas[26] = -0.018143, lambdas[26] = 0.171542
etas[27] = -0.007811, lambdas[27] = 0.173888
etas[28] = -0.026114, lambdas[28] = 0.174839
etas[29] = -0.000092, lambdas[29] = 0.179624
etas[30] = -0.040563, lambdas[30] = 0.184958
etas[31] = 0.007859, lambdas[31] = 0.188216
etas[32] = 0.021577, lambdas[32] = 0.191419
etas[33] = -0.001012, lambdas[33] = 0.192816
etas[34] = -0.004919, lambdas[34] = 0.195691
etas[35] = -0.038724, lambdas[35] = 0.197462
etas[36] = -0.010967, lambdas[36] = 0.200206
etas[37] = 0.008656, lambdas[37] = 0.206067
etas[38] = -0.042956, lambdas[38] = 0.213453
etas[39] = -0.026085, lambdas[39] = 0.218856
etas[40] = -0.005610, lambdas[40] = 0.219378
etas[41] = -0.011832, lambdas[41] = 0.223947
etas[42] = -0.001221, lambdas[42] = 0.224511
etas[43] = -0.019110, lambdas[43] = 0.229249
etas[44] = -0.033467, lambdas[44] = 0.235044
etas[45] = 0.020562, lambdas[45] = 0.236764
etas[46] = 0.030245, lambdas[46] = 0.243689
etas[47] = -0.017044, lambdas[47] = 0.246064
etas[48] = 0.029263, lambdas[48] = 0.253058
etas[49] = 0.011870, lambdas[49] = 0.264030
etas[50] = -0.012271, lambdas[50] = 0.264675
etas[51] = 0.021125, lambdas[51] = 0.267538
etas[52] = 0.012991, lambdas[52] = 0.273297
etas[53] = 0.032727, lambdas[53] = 0.274399
etas[54] = 0.014680, lambdas[54] = 0.282197
etas[55] = -0.009822, lambdas[55] = 0.285912
etas[56] = 0.011537, lambdas[56] = 0.296841
etas[57] = -0.001339, lambdas[57] = 0.302922
etas[58] = 0.009343, lambdas[58] = 0.306037
etas[59] = -0.030221, lambdas[59] = 0.311813
etas[60] = 0.021573, lambdas[60] = 0.319381
etas[61] = 0.013156, lambdas[61] = 0.330782
etas[62] = 0.030816, lambdas[62] = 0.333075
etas[63] = -0.010961, lambdas[63] = 0.337674
etas[64] = -0.010617, lambdas[64] = 0.338544
etas[65] = 0.038583, lambdas[65] = 0.347533
etas[66] = -0.004539, lambdas[66] = 0.354333
etas[67] = -0.019438, lambdas[67] = 0.361156
etas[68] = -0.044128, lambdas[68] = 0.372229
etas[69] = -0.002282, lambdas[69] = 0.384487
etas[70] = 0.011420, lambdas[70] = 0.407359
etas[71] = 0.019151, lambdas[71] = 0.415695
etas[72] = 0.015149, lambdas[72] = 0.433998
etas[73] = -0.006606, lambdas[73] = 0.443825
etas[74] = 0.026544, lambdas[74] = 0.447950
etas[75] = 0.000436, lambdas[75] = 0.461880
etas[76] = -0.007269, lambdas[76] = 0.469867
etas[77] = -0.025637, lambdas[77] = 0.485169
etas[78] = -0.072375, lambdas[78] = 0.500579
etas[79] = 0.037501, lambdas[79] = 0.529156
etas[80] = 0.003030, lambdas[80] = 0.536558
etas[81] = 0.025787, lambdas[81] = 0.586429
etas[82] = 0.032409, lambdas[82] = 0.818823
etas[83] = 0.037770, lambdas[83] = 1.014345
etas[84] = -0.009187, lambdas[84] = 1.034352
etas[85] = -0.000041, lambdas[85] = 1.754933
etas[86] = 0.026560, lambdas[86] = 3.086517
etas[87] = 0.005559, lambdas[87] = 4.635223
etas[88] = 0.034380, lambdas[88] = 8.837129
0	2547.401602	2519.438521	208.119966
1	2519.438521	2485.891042	208.145431
2	2485.891042	2445.797628	208.176155
3	2445.797628	2398.101239	208.213130
4	2398.101239	2341.673005	208.257487
5	2341.673005	2275.354419	208.310499
6	2275.354419	2198.023391	208.373569
7	2198.023391	2108.688691	208.448202
8	2108.688691	2006.614756	208.535958
9	2006.614756	1891.473725	208.638370
10	1891.473725	1763.513444	208.756839
11	1763.513444	1623.719618	208.892490
12	1623.719618	1473.939105	209.046001
13	1473.939105	1316.923825	209.217418
14	1316.923825	1156.256195	209.405975
15	1156.256195	996.132486	209.609958
16	996.132486	841.010799	209.826650
17	841.010799	695.169309	210.052377
18	695.169309	562.254407	210.282703
19	562.254407	444.911837	210.512732
20	444.911837	344.577463	210.737506
21	344.577463	261.462024	210.952434
22	261.462024	194.712552	211.153676
23	194.712552	142.692220	211.338437
24	142.692220	103.303713	211.505127
25	103.303713	74.289448	211.653384
26	74.289448	53.465857	211.783981
27	53.465857	38.876153	211.898654
28	38.876153	28.867255	211.999877
29	28.867255	22.108244	212.090623
30	22.108244	17.570846	212.174130
31	17.570846	14.489955	212.253670
32	14.489955	12.317364	212.332341
33	12.317364	10.676771	212.412860
34	10.676771	9.324058	212.497370
35	9.324058	8.113970	212.587269
36	8.113970	6.972793	212.683070
37	6.972793	5.875974	212.784319
38	5.875974	4.829715	212.889604
39	4.829715	3.855934	212.996667
40	3.855934	2.980470	213.102633
41	2.980470	2.224637	213.204342
42	2.224637	1.600251	213.298740
43	1.600251	1.107929	213.383264
44	1.107929	0.738095	213.456160
45	0.738095	0.473783	213.516662
46	0.473783	0.294194	213.565009
47	0.294194	0.178078	213.602300
48	0.178078	0.106311	213.630219
49	0.106311	0.063388	213.650702
50	0.063388	0.037892	213.665615
51	0.037892	0.022186	213.676494
52	0.022186	0.011681	213.684389
53	0.011681	0.003952	213.689824
54	0.003952	-0.002074	213.692856
55	-0.002074	-0.006751	213.693210
56	-0.006751	-0.010144	213.690443
57	-0.010144	-0.012274	213.684113
58	-0.012274	-0.013229	213.673925
59	-0.013229	-0.013184	213.659823
60	-0.013184	-0.012376	213.642037
61	-0.012376	-0.011066	213.621065
62	-0.011066	-0.009496	213.597625
63	-0.009496	-0.007865	213.572566
64	-0.007865	-0.006317	213.546776
65	-0.006317	-0.004939	213.521094
66	-0.004939	-0.003772	213.496244
67	-0.003772	-0.002823	213.472793
68	-0.002823	-0.002076	213.451140
69	-0.002076	-0.001504	213.431520
70	-0.001504	-0.001076	213.414028
71	-0.001076	-0.000761	213.398649
72	-0.000761	-0.000534	213.385286
73	-0.000534	-0.000371	213.373790
74	-0.000371	-0.000257	213.363984
75	-0.000257	-0.000177	213.355678
76	-0.000177	-0.000121	213.348683
77	-0.000121	-0.000082	213.342822
78	-0.000082	-0.000056	213.337931
79	-0.000056	-0.000038	213.333863
80	-0.000038	-0.000026	213.330489
81	-0.000026	-0.000017	213.327698
82	-0.000017	-0.000012	213.325392
83	-0.000012	-0.000008	213.323491
84	-0.000008	-0.000005	213.321925
85	-0.000005	-0.000004	213.320636
86	-0.000004	-0.000002	213.319578
87	-0.000002	-0.000002	213.318708
88	-0.000002	-0.000001	213.317994
89	-0.000001	-0.000001	213.317408
90	-0.000001	-0.000000	213.316927
91	-0.000000	-0.000000	213.316534
92	-0.000000	-0.000000	213.316211
93	-0.000000	-0.000000	213.315946
94	-0.000000	-0.000000	213.315729
95	-0.000000	-0.000000	213.315551
96	-0.000000	-0.000000	213.315406
97	-0.000000	-0.000000	213.315287
98	-0.000000	-0.000000	213.315189
99	-0.000000	-0.000000	213.315109
REMLE (delta=2.523593, REML=213.693418, REML0=213.314748)- elapsed CPU time is 0.010000
eigen_L - elapsed CPU time is 0.000000


Reading TPED file genotype...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

GLS association - elapsed CPU time is 0.820000

Breakdown for input file parsing : 0.360000

              matrix computation : 0.270000

             p-value computation : 0.020000
```

Results are in: /labs/Wegrzyn/IreneCobo/Fraxinus/v1.4/

MLWassoc.ps
HKPassoc.ps

Each line consist of:
1. SNP ID
2. Beta (1 is effect allele)
3. SE(beta)
4. p-value.

Now it is necessary to correct by multiple testing. Since I used a SNP panel not corrected by LD to perform the GWAS in EMMAX, then, Bonferroni correction would be too retrictive/conservative. It is because the Bonferroni correction assumes independence among the association tests. However, there is a substantial degree of correlation between the association statistics due to a phenomenon called linkage disequilibrium, which renders the Bonferroni correction too conservative (https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0903-6#Sec12.)  Thus, FDR correction would be more appropriate: http://lybird300.github.io/2015/10/19/multiple-test-correction.html

The FDR correction was performed in R. The results of this FDR correction are in HKPpadjust.csv and MLWpadjust.csv

After filtering the significant SNPs (P<0.05) 104 and 112 significant SNPs were obtained for the phenotypes HKP and MLW, respectively. The results are stored in the files HKPpadjust0.05.csv and MLWpadjust0.05.csv. 

However, all significant SNPs in HKP and all with the exception of 8 SNPs in MLW showed a p-value and a padjust equal to 0, as well as the SE(beta). 

I will try to use this method of multiple-testing correction specific for LMM called MultiTrans: https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0903-6#Sec12

R documentation for MultiTrans: http://genetics.cs.ucla.edu/multiTrans/install.html

Not working. Instead, I imputed the lacking SNPs using LinkImpute (reference articles: https://acsess.onlinelibrary.wiley.com/doi/full/10.3835/plantgenome2015.11.0113 and https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4632058/):

I used the simple imputation, using the default parameters

```
java -jar LinkImpute.jar -q output.ped outputimputed

Starting to read in dataset...
	Read in data set of 98 samples and 29417 SNPs.
Finished reading in data set.
Starting calculating correlations...
	[==================================================]
	[==================================================]
Finished calculating correlations.
Masking 10000 genotypes
Starting optimizing parameters...
	....................................................................................
	Best k:		4
	Best l:		21
	Accuracy:	0.9396
Finished optimizing parameters.
Starting imputation...
	[==================================================]
Finished imputation.
Writing output...
Finished writing output.
```

The results showed that the best k was 4 and the best l was 21 for the data set, which gave an accuracy of 0.9396.

I also performed the imputation using a .ped file containing the family and phenotypic data:

```
java -jar LinkImpute.jar -q outputpedsingle2.ped outputimputedpedsingle

Starting to read in dataset...
	Read in data set of 98 samples and 29417 SNPs.
Finished reading in data set.
Starting calculating correlations...
	[==================================================]
	[==================================================]
Finished calculating correlations.
Masking 10000 genotypes
Starting optimizing parameters...
	............................................................................................................................................................................
	Best k:		14
	Best l:		65
	Accuracy:	0.9497
Finished optimizing parameters.
Starting imputation...
	[==================================================]
Finished imputation.
Writing output...
Finished writing output.

```

The optimization of parameters showed that the best k was 14 and the best l 65 for the data set, showing an accuracy of 0.9497

Afterwards, I calculated the identity‐by‐descent (IBD) using PLINK (Purcell et al., 2007; Purcell, 2009). When two or more accessions had IBD Pi > 0.9, one accession from the clonal group was randomly chosen, and its genotype data were retained while the genotype data from the other clones were removed. Since IBD calculation in plink is not LD-aware, I used the LD pruned dataset to perform the IBD calculation.

```
plink --file genotypeLD0.2 --genome --out IBDLD0.2
```
This code gives as a result 2 files:

- IBDLD0.2.genome
- IBDLD0.2.log

I sorted the 10th column of the IBDLD0.2.genome file (PI_HAT) in descendent order: IBDLD0.2sorted.genome

The following pairs of individuals showed a PI_HAT higher that 0.9:

- FP3005-FP3013 (PI_HAT 0.9973) (Siblings, both belong to family C, they are siblings)
- FP3110-Sample_3108 (PI_HAT 0.929) (Siblings, both belong to family K)

Then I decided to perform the GWAS in EMMAX using all the individuals and the matrix with the imputed SNPs and also by removing these two individuals (I removed FP3013 and FP3110) and compare the differences. 

1. Use PLINK software to transpose the imputed genotype files (bed or ped format) to tped/tfam format by running 

```
plink --file outputimputed --recode12 --output-missing-genotype 0 --transpose --out outputimputed
```

2. Run EMMAX using both phenotypes (HKP and MLW):

```
./emmax-intel64 -v -d 10 -t outputimputed -p phenoEMMAXorderHKP.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o HKPassocimputed
./emmax-intel64 -v -d 10 -t outputimputed -p phenoEMMAXorderMLW.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o MLWassocimputed
```
The results files are called HKPassocimputed.ps and MLWassocimputed.ps

3. I run EMMAX also in the same files but without FP3013 and FP3110 individuals:

```
./emmax-intel64 -v -d 10 -t outputimputednoFP3110noFP3013 -p phenoEMMAXorderHKPnoFP3110noFP3013.txt -k outputnoFP3013noFP3110LD0.2.aBN.kinf -c assignedPopordernoFP3110noFP3013.txt -o HKPassocimputednoFP3110noFP3013
```
```
./emmax-intel64 -v -d 10 -t outputimputednoFP3110noFP3013 -p phenoEMMAXorderMLWnoFP3110noFP3013.txt -k outputnoFP3013noFP3110LD0.2.aBN.kinf -c assignedPopordernoFP3110noFP3013.txt -o MLWassocimputednoFP3110noFP3013
```

The results files are called HKPassocimputednoFP3110noFP3013.ps and MLWassocimputednoFP3110noFP3013.ps

Pvalues for both, GWAS results from imputed SNPs including all individuals and without FP3013 and FP3110 were corrected using FDR and Bonferroni correction for multiple testing. Corrected p-values <0.05 were considered as significant. These were the results:

- HKP:
  - All imputed SNPs=  100 significant SNPs using FDR and 100 for Bonferroni
  - Without FP3110 and FP3013 = 0 singnificant SNPs using FDR and 0 using Bonferroni
- MLW
  - All imputed SNPs= 100 significant SNPs using FDR and 100 using Bonferroni (exactly the same SNPs between them and in comparison with the 100 HKP significant SNPs)
  - Without FP3110 and FP3013 = 0 singnificant SNPs using FDR and 0 using Bonferroni

The results (for all imputed SNPs) are in the files: HKPimputedFDR0.05.csv, HKPimputedBonf0.05.csv, MLWimputedFDR0.05.csv, MLWimputedBonf0.05.csv, containing the same 100 significant (P<0.05) SNPs each

I have compared these 100 significant SNPs with the 104 and 112 significant SNPs obtained for HKP and MLW phenotypes before imputation and they are different SNPs. 

The best linear unbiased predictions (BLUPs) is a method for estimating random effects, so so it has gradually become more common application to generate more precise estimates of genotypic values 

So I calculated the BLUPs for both phenotypes and I performed the GWAS in EMMAX using the dataset that offered more significantly associated SNPs (the ones previous to the imputation) 

A standard BLUPS model looks like this (Doubt: How to develop the model for this data, maybe "family" as the only random effect?): 
Y = lmer(X~(1|LINE) + (1|LOC) + (1|YEAR) + (1|LINE:LOC) + (1|LINE: YEAR))

Being LINE=line, LOC,=location, YEAR=year

Following steps:

- LD decay calculation:

To calculate the LD, I used plink (https://zzz.bwh.harvard.edu/plink/ld.shtml):

Using a 1 Mb (1000 kb) window
```
plink --file output --r2 --ld-window-kb 1000 --out outputLDwindow1Mb
```
And also a 500 bp (0.5 Kb) window:

```
plink --file output --r2 --ld-window-kb 0.5 --out outputLDwindow500pb
```

The results for both analysis are located in the following files: outputLDwindow1Mb.ld and outputLDwindow500pb.ld

Afterwards, I plotted the LD decay in R :

To do so, I calculated the distance between SNPs for both files using the following code:

```
awk '{print $5-$2,$7}' outputLDwindow500pb.ld > outputLDwindow500pbsummary.txt #distance in pb
```
```
awk '{print $5-$2,$7}' outputLDwindow1Mb.ld > outputLDwindow1Mbsummary.txt #distance in pb
```

```
awk '{print $1/1000,$2}' outputLDwindow1Mbsummary.txt > outputLDwindow1Mbsummarykb.txt #distance in kb
```

They were also sorted in ascendent order by the first column (pb or kb) using this code:

```
sort -k1,1 outputLDwindow1Mbsummarykb.txt > outputLDwindow1Mbsummarykbsort.txt
```
```
sort -k1,1 outputLDwindow500pbsummary.txt > outputLDwindow500pbsummarysort.txt
```

I plotted these summary files in R using the following code:

- Code: https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/LDdecayplot.R
- Plot: 
Inter-SNP distance 1 Mb: https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/LD_decay_Inter-SNP_distance_up_to_1_MB.png 
Inter-SNP distance 500 pb: https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/LD_decay_inter_SNP_500_bp.png

- Correlation between the two phenotypes: I calculated it using a Pearson correlation test in R 

Plot: ![PearsonCorrelationTestPlot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/Pearson_correlation_test_phenotypes.png "PearsonCorrelationPlot")
Code: ![PearsonCorerlationCode](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/PhenotypeCorrelation_R.R "PearsonCorrelationCode")

- Manhatan plot with the GWAS results

To see where the statistically significant SNPs from the GWAS calculation (in both datasets, imputed and non-imputed), and for both phenotypes (HKP and MLW), were located, I performed four Manhattan plots (HKP, MLW, HKPimputed and MLWimputed):

Plots: ![HKPManhattanPlot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/HKPManhattanPlot.png "HKPManhatanPlot"), ![MLWManhattanPlot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/MLWManhattanPlot.png "MLWManhattanPlot"), ![HKPimputedManhattanPlot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/HKPimputedManhattanPlot.png "HKPImputedManhattanPlot"), ![MLWimputedManhattanPlot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/MLWimputedManhattanPlot.png "MLWImputedManhattanPlot")
Code: ![ManhattanPlot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/ManhatanPlot.R "ManhattanPlotR")

The kinship matrix was plotted in a heatmap to see if it reflects the Family clustering: 

![HeatmapKinshipmatrix](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/HeatmapKinshipmatrix.png/ "HeatmapKinshipmatrix")

Heatmap: ![HeatmapHotColor](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/HeatmapKinshipHeatColorsLabeled.png "HeatmapHotColors")
Code: ![HeatmapHotColorsCode](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/HeatmapKinshipmatrix.R "HeatmapHotColorsCode")

I also plotted the phenotypes (HKP and MLW) in several histograms, for all families together and for each family separatedly:

HistogramHKP.png, HistogramMLW.png, HKPFamilyC(D,E,HA,HB, I, J, K, G)Histogram.png, MLWFamilyC(D,E,HA,HB, I, J, K, G)Histogram.png


**IMPUTED LinkImpute**

- This section is the continuation of the data quality filtering performed using LinkImputeR (https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/tree/master/Filtering). After this filtering, 930660 SNPs were obtained. The SNPs were stored in a vcf file called "imputed.vcf"

Working directory: /labs/Wegrzyn/IreneCobo/Fraxinus/LinkImputeRJeremy

1. Perform linkage disequilibrium filtering using plink to retain only independent SNPs for DAPC analysis: 
```
plink --vcf imputed.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --indep-pairwise 50 5 0.2 --recode vcf --out imputedLD0.2.vcf
```
2. Perform DAPC analysis in R
```
#Code from tutorial DAPC adegenet (https://adegenet.r-forge.r-project.org/files/tutorial-dapc.pdf)
#How include populations in the genlight object, and plot them, from this tutorial: https://grunwaldlab.github.io/Population_Genetics_in_R/gbs_analysis.html

library(adegenet)
library(vcfR)

#Include families to which each individual belongs, and we order them as the vcf 
#Incluimos las familias a las que pertenece cada individuo y las ordenamos igual que el vcf

#The only input file needed is a vcf or vcf.gz file
vcf <- read.vcfR("imputedLD0.2.vcf.vcf")
dim(vcf)#number of variants 930660 
head(vcf[,1:10])
x <- vcfR2genlight(vcf) #transform vcf to genlight
x
#incluimos las poblaciones a las que pertenece cada individuo en el genlight object (x)
# Include populationsn to which each individual belongs in the genlight object (x)
pop(x)<-IDfamord$V2
grp <- find.clusters(x, max.n.clust=40)#There are about 100 PCs, I retained 150 to keep them all. 4 clusters. 
grp
x@pop
dapc1 <- dapc(x, grp$grp) #80 PCs retained, 3 discrmininant functions. DAPC is implemented by the function "dapc", which first transforms the data using PCA, and then performs a Discriminant Analysis on the retained principal components.Two graphs will appear as a result to help decide the number of principal components and the number of discriminant functions to be retained  
dapc1
dapc2 <- dapc(x, x@pop,n.pca=70, n.da=6)
dapc3 <- dapc(x,n.pca=70, n.da=6)
scatter(dapc1)#plot the groups in a scatterplot and the DA eigenvalues
#The scatterplot can be customized (dot size)
scatter(dapc1, cell = 0, pch = 18:23, cstar = 0, mstree = TRUE, lwd = 2, lty = 2)
#position of the DA eigenvalues plot
scatter(dapc1, posi.da="bottomleft", bg="white", pch=17:22)

#Change of colors, position of the PCA eigenvalues plot 
myCol <- c("darkblue","purple","green","red")
scatter(dapc1, posi.da="bottomleft", bg="white", legend = TRUE,
        pch=17:22, cstar=0, col=myCol, scree.pca=TRUE,
        posi.pca="bottomright")
scatter(dapc1, posi.da="bottomleft", bg="white", legend = TRUE,
        pch=x@pop, cstar=0, col=myCol, scree.pca=TRUE,
        posi.pca="bottomright")
scatter(dapc1, col = myCol, cex = 2, legend = TRUE, clabel = F, posi.leg = "bottomleft", scree.pca = TRUE,
        posi.pca = "bottomright", pch=x@pop, cleg = 0.75)
scatter(dapc2, cex = 2, legend = TRUE, clabel = F, posi.leg = "bottomleft", scree.pca = TRUE,
        posi.pca = "bottomright", cleg = 0.75)
scatter(dapc3, cex = 2, legend = TRUE, clabel = F, posi.leg = "topright", scree.pca = TRUE,
        posi.pca = "bottomright", cleg = 0.75)
myCol <- c("darkblue","purple","green","orange")
scatter(dapc1, posi.da="bottomright", bg="white",
        pch=17:22, cstar=0, col=myCol, scree.pca=TRUE,
        posi.pca="bottomleft")
#Membership probabilities
#Exploration
class(dapc1$posterior)
dim(dapc1$posterior)
round(head(dapc1$posterior),3)
str(dapc1$posterior)
head(dapc1$posterior)
head(dapc1$assign)
#Change formatting for storage and used to correct by population structure in the GWAS
round<-round(dapc1$posterior,3)
head(round)
tail(round)
str(round)
assignedPop<-apply(round, 1, which.max)
head(assignedPop)
assignedPopframe<-as.data.frame(assignedPop)
head(assignedPopframe)
str(assignedPopframe)
write.csv(assignedPopframe, "assignedPopFraxinusImputedDAPC.csv")
```
![DAPCImputed](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/DAPCImputed.png/ "DAPCImputed")

3. Transform the .vcf file to plink format (.ped) using vcftools and the following code:

```
vcftools --vcf imputed.vcf --plink --out imputed
```
4. Use PLINK software to transpose your genotype files (bed or ped format) to tped/tfam format by running 

```
plink --file imputed --recode12 --output-missing-genotype 0 --transpose --out imputed
```

5. Perform BN matrix in EMMAX (follow this tutorial: http://genetics.cs.ucla.edu/emmax/install.html)
```
./emmax-kin-intel64 -v -d 10 imputedLD0.2
```

6. Run EMMAX for the two phenotypes (HKP and MLW)

HKP phenotype

```
./emmax-intel64 -v -d 10 -t imputed -p phenoEMMAXorderHKP.txt -k imputedLD0.2.aBN.kinf -c assignedPopFraxinusImputedDAPC.txt -o HKPassocImputed
```
-t: transposed genotype imputed matrix
-k: kinship matrix performed in the imputed genotypes filtered by Linkage disequilibrium
-c: Population structure as covariate
-o: output

```
Reading TFAM file imputed.tfam ....


Reading kinship file imputedLD0.2.aBN.kinf...

  100 rows and 100 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderHKP.txt...

  100 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPopFraxinusImputedDAPC.txt...

  100 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 0.000000, evals[n-1] = 4.985196
k = 91,  n-q = 91
eRvals[0] = 0.130078, eRvals[n-q-1] = 3.985196, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.130000
etas[0] = -0.327316, lambdas[0] = 0.130078
etas[1] = 0.000677, lambdas[1] = 0.135780
etas[2] = 0.093985, lambdas[2] = 0.136573
etas[3] = -0.059687, lambdas[3] = 0.137881
etas[4] = -0.078074, lambdas[4] = 0.140680
etas[5] = 0.100785, lambdas[5] = 0.141315
etas[6] = -0.182823, lambdas[6] = 0.143108
etas[7] = -0.144061, lambdas[7] = 0.145229
etas[8] = -0.063563, lambdas[8] = 0.147537
etas[9] = 0.231500, lambdas[9] = 0.148861
etas[10] = 0.240785, lambdas[10] = 0.149744
etas[11] = 0.296964, lambdas[11] = 0.150969
etas[12] = 0.132093, lambdas[12] = 0.151619
etas[13] = 0.160224, lambdas[13] = 0.152900
etas[14] = 0.260964, lambdas[14] = 0.154264
etas[15] = -0.167213, lambdas[15] = 0.155412
etas[16] = 0.055927, lambdas[16] = 0.156639
etas[17] = 0.178530, lambdas[17] = 0.157679
etas[18] = -0.306998, lambdas[18] = 0.160953
etas[19] = 0.034772, lambdas[19] = 0.162092
etas[20] = -0.103905, lambdas[20] = 0.162960
etas[21] = 0.252186, lambdas[21] = 0.165364
etas[22] = 0.099962, lambdas[22] = 0.166686
etas[23] = 0.328697, lambdas[23] = 0.168981
etas[24] = 0.048820, lambdas[24] = 0.169402
etas[25] = 0.382645, lambdas[25] = 0.170663
etas[26] = -0.270154, lambdas[26] = 0.171582
etas[27] = 0.325844, lambdas[27] = 0.173644
etas[28] = -0.441102, lambdas[28] = 0.175394
etas[29] = -0.093250, lambdas[29] = 0.178569
etas[30] = -0.154460, lambdas[30] = 0.181141
etas[31] = -0.037891, lambdas[31] = 0.182844
etas[32] = -0.096426, lambdas[32] = 0.185579
etas[33] = -0.056193, lambdas[33] = 0.187017
etas[34] = -0.279541, lambdas[34] = 0.190860
etas[35] = 0.157557, lambdas[35] = 0.193298
etas[36] = -0.017131, lambdas[36] = 0.193535
etas[37] = -0.013695, lambdas[37] = 0.200104
etas[38] = 0.113164, lambdas[38] = 0.203163
etas[39] = 0.121625, lambdas[39] = 0.204487
etas[40] = -0.120713, lambdas[40] = 0.211116
etas[41] = -0.230111, lambdas[41] = 0.213197
etas[42] = 0.090684, lambdas[42] = 0.219688
etas[43] = 0.265772, lambdas[43] = 0.221869
etas[44] = -0.217367, lambdas[44] = 0.223454
etas[45] = 0.120374, lambdas[45] = 0.227155
etas[46] = 0.006074, lambdas[46] = 0.228339
etas[47] = -0.105638, lambdas[47] = 0.231061
etas[48] = -0.138658, lambdas[48] = 0.237735
etas[49] = 0.177499, lambdas[49] = 0.238958
etas[50] = -0.089640, lambdas[50] = 0.241818
etas[51] = -0.268355, lambdas[51] = 0.247345
etas[52] = -0.203005, lambdas[52] = 0.249351
etas[53] = 0.102118, lambdas[53] = 0.252158
etas[54] = 0.003942, lambdas[54] = 0.253567
etas[55] = 0.150175, lambdas[55] = 0.256513
etas[56] = 0.287915, lambdas[56] = 0.257778
etas[57] = -0.131969, lambdas[57] = 0.259132
etas[58] = -0.123766, lambdas[58] = 0.264140
etas[59] = -0.128784, lambdas[59] = 0.274087
etas[60] = 0.316285, lambdas[60] = 0.274603
etas[61] = 0.219316, lambdas[61] = 0.279161
etas[62] = 0.206315, lambdas[62] = 0.284471
etas[63] = 0.241061, lambdas[63] = 0.286792
etas[64] = 0.111997, lambdas[64] = 0.297662
etas[65] = -0.106571, lambdas[65] = 0.299033
etas[66] = -0.204725, lambdas[66] = 0.304936
etas[67] = 0.189363, lambdas[67] = 0.314002
etas[68] = -0.093941, lambdas[68] = 0.325118
etas[69] = -0.060549, lambdas[69] = 0.354461
etas[70] = 0.179064, lambdas[70] = 0.414638
etas[71] = -0.178313, lambdas[71] = 0.438702
etas[72] = -0.066562, lambdas[72] = 0.463051
etas[73] = 0.333106, lambdas[73] = 0.477633
etas[74] = -0.162937, lambdas[74] = 0.491703
etas[75] = -0.106558, lambdas[75] = 0.505573
etas[76] = 0.037697, lambdas[76] = 0.553688
etas[77] = 0.148853, lambdas[77] = 0.636021
etas[78] = -0.031072, lambdas[78] = 0.681860
etas[79] = -0.303986, lambdas[79] = 0.784785
etas[80] = -0.512597, lambdas[80] = 0.853428
etas[81] = -0.069878, lambdas[81] = 0.888070
etas[82] = -0.268748, lambdas[82] = 0.928393
etas[83] = 0.157817, lambdas[83] = 1.613943
etas[84] = 0.167279, lambdas[84] = 1.782806
etas[85] = 0.071216, lambdas[85] = 1.893792
etas[86] = -0.208478, lambdas[86] = 1.995695
etas[87] = 0.209939, lambdas[87] = 2.033285
etas[88] = -0.212230, lambdas[88] = 2.183938
etas[89] = -0.030400, lambdas[89] = 3.090552
etas[90] = 0.129873, lambdas[90] = 3.985196
0	45.825930	45.821149	11.721626
1	45.821149	45.815310	11.722086
2	45.815310	45.808179	11.722649
3	45.808179	45.799473	11.723336
4	45.799473	45.788842	11.724175
5	45.788842	45.775863	11.725199
6	45.775863	45.760019	11.726450
7	45.760019	45.740679	11.727977
8	45.740679	45.717074	11.729842
9	45.717074	45.688270	11.732119
10	45.688270	45.653127	11.734898
11	45.653127	45.610264	11.738290
12	45.610264	45.557997	11.742430
13	45.557997	45.494289	11.747480
14	45.494289	45.416670	11.753642
15	45.416670	45.322154	11.761155
16	45.322154	45.207140	11.770315
17	45.207140	45.067296	11.781477
18	45.067296	44.897429	11.795072
19	44.897429	44.691344	11.811620
20	44.691344	44.441682	11.831747
21	44.441682	44.139768	11.856205
22	44.139768	43.775446	11.885893
23	43.775446	43.336961	11.921882
24	43.336961	42.810867	11.965436
25	42.810867	42.182037	12.018045
26	42.182037	41.433807	12.081439
27	41.433807	40.548312	12.157614
28	40.548312	39.507100	12.248834
29	39.507100	38.292094	12.357629
30	38.292094	36.886960	12.486761
31	36.886960	35.278909	12.639164
32	35.278909	33.460892	12.817834
33	33.460892	31.434015	13.025679
34	31.434015	29.209895	13.265308
35	29.209895	26.812509	13.538766
36	26.812509	24.279027	13.847238
37	24.279027	21.659120		14.190732
38	21.659120	19.012451	14.567806
39	19.012451	16.404374	14.975360
40	16.404374	13.900354	15.408572
41	13.900354	11.559948	15.861003
42	11.559948	9.431438	16.324890
43	9.431438	7.548035	16.791605
44	7.548035	5.926244	17.252242
45	5.926244	4.566464	17.698239
46	4.566464	3.455423	18.121961
47	3.455423	2.569782	18.517163
48	2.569782	1.880131	18.879289
49	1.880131	1.354718	19.205565
50	1.354718	0.962454	19.494930
51	0.962454	0.674987	19.747818
52	0.674987	0.467857	19.965858
53	0.467857	0.320887	20.151547
54	0.320887	0.218040	20.307933
55	0.218040	0.146958	20.438334
56	0.146958	0.098371	20.546123
57	0.098371	0.065478	20.634554
58	0.065478	0.043392	20.706649
59	0.043392	0.028663	20.765132
60	0.028663	0.018894	20.812391
61	0.018894	0.012440	20.850479
62	0.012440	0.008188	20.881125
63	0.008188	0.005391	20.905764
64	0.005391	0.003553	20.925575
65	0.003553	0.002344	20.941514
66	0.002344	0.001549	20.954350
67	0.001549	0.001025	20.964702
68	0.001025	0.000679	20.973062
69	0.000679	0.000451	20.979824
70	0.000451	0.000300	20.985302
71	0.000300	0.000199	20.989746
72	0.000199	0.000133	20.993355
73	0.000133	0.000089	20.996291
74	0.000089	0.000059	20.998680
75	0.000059	0.000039	21.000627
76	0.000039	0.000026	21.002215
77	0.000026	0.000018	21.003511
78	0.000018	0.000012	21.004568
79	0.000012	0.000008	21.005432
80	0.000008	0.000005	21.006138
81	0.000005	0.000004	21.006716
82	0.000004	0.000002	21.007188
83	0.000002	0.000002	21.007574
84	0.000002	0.000001	21.007889
85	0.000001	0.000001	21.008148
86	0.000001	0.000000	21.008359
87	0.000000	0.000000	21.008532
88	0.000000	0.000000	21.008674
89	0.000000	0.000000	21.008789
90	0.000000	0.000000	21.008884
91	0.000000	0.000000	21.008962
92	0.000000	0.000000	21.009025
93	0.000000	0.000000	21.009077
94	0.000000	0.000000	21.009120
95	0.000000	0.000000	21.009155
96	0.000000	0.000000	21.009183
97	0.000000	0.000000	21.009207
98	0.000000	0.000000	21.009226
99	0.000000	0.000000	21.009242
REMLE (delta=22026.465795, REML=21.009254, REML0=21.009312)- elapsed CPU time is 0.010000
eigen_L - elapsed CPU time is 0.000000


Reading TPED file imputed...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

Reading 30000-th SNP and testing association....

Reading 40000-th SNP and testing association....

Reading 50000-th SNP and testing association....

Reading 60000-th SNP and testing association....

Reading 70000-th SNP and testing association....

Reading 80000-th SNP and testing association....

Reading 90000-th SNP and testing association....

Reading 100000-th SNP and testing association....

Reading 110000-th SNP and testing association....

Reading 120000-th SNP and testing association....

Reading 130000-th SNP and testing association....

Reading 140000-th SNP and testing association....

Reading 150000-th SNP and testing association....

Reading 160000-th SNP and testing association....

Reading 170000-th SNP and testing association....

Reading 180000-th SNP and testing association....

Reading 190000-th SNP and testing association....

Reading 200000-th SNP and testing association....

Reading 210000-th SNP and testing association....

Reading 220000-th SNP and testing association....

Reading 230000-th SNP and testing association....

Reading 240000-th SNP and testing association....

Reading 250000-th SNP and testing association....

Reading 260000-th SNP and testing association....

Reading 270000-th SNP and testing association....

Reading 280000-th SNP and testing association....

Reading 290000-th SNP and testing association....

Reading 300000-th SNP and testing association....

Reading 310000-th SNP and testing association....

Reading 320000-th SNP and testing association....

Reading 330000-th SNP and testing association....

Reading 340000-th SNP and testing association....

Reading 350000-th SNP and testing association....

Reading 360000-th SNP and testing association....

Reading 370000-th SNP and testing association....

Reading 380000-th SNP and testing association....

Reading 390000-th SNP and testing association....

Reading 400000-th SNP and testing association....

Reading 410000-th SNP and testing association....

Reading 420000-th SNP and testing association....

Reading 430000-th SNP and testing association....

Reading 440000-th SNP and testing association....

Reading 450000-th SNP and testing association....

Reading 460000-th SNP and testing association....

Reading 470000-th SNP and testing association....

Reading 480000-th SNP and testing association....

Reading 490000-th SNP and testing association....

Reading 500000-th SNP and testing association....

Reading 510000-th SNP and testing association....

Reading 520000-th SNP and testing association....

Reading 530000-th SNP and testing association....

Reading 540000-th SNP and testing association....

Reading 550000-th SNP and testing association....

Reading 560000-th SNP and testing association....

Reading 570000-th SNP and testing association....

Reading 580000-th SNP and testing association....

Reading 590000-th SNP and testing association....

Reading 600000-th SNP and testing association....

Reading 610000-th SNP and testing association....

Reading 620000-th SNP and testing association....

Reading 630000-th SNP and testing association....

Reading 640000-th SNP and testing association....

Reading 650000-th SNP and testing association....

Reading 660000-th SNP and testing association....

Reading 670000-th SNP and testing association....

Reading 680000-th SNP and testing association....

Reading 690000-th SNP and testing association....

Reading 700000-th SNP and testing association....

Reading 710000-th SNP and testing association....

Reading 720000-th SNP and testing association....

Reading 730000-th SNP and testing association....

Reading 740000-th SNP and testing association....

Reading 750000-th SNP and testing association....

Reading 760000-th SNP and testing association....

Reading 770000-th SNP and testing association....

Reading 780000-th SNP and testing association....

Reading 790000-th SNP and testing association....

Reading 800000-th SNP and testing association....

Reading 810000-th SNP and testing association....

Reading 820000-th SNP and testing association....

Reading 830000-th SNP and testing association....

Reading 840000-th SNP and testing association....

Reading 850000-th SNP and testing association....

Reading 860000-th SNP and testing association....

Reading 870000-th SNP and testing association....

Reading 880000-th SNP and testing association....

Reading 890000-th SNP and testing association....

Reading 900000-th SNP and testing association....

Reading 910000-th SNP and testing association....

Reading 920000-th SNP and testing association....

Reading 930000-th SNP and testing association....

GLS association - elapsed CPU time is 45.130000

Breakdown for input file parsing : 6.560000

              matrix computation : 34.040000

             p-value computation : 0.890000
```

- Same analysis using the MLW phenotype

```
./emmax-intel64 -v -d 10 -t imputed -p phenoEMMAXorderMLW.txt -k imputedLD0.2.aBN.kinf -c assignedPopFraxinusImputedDAPC.txt -o MLWassocImputed
```
```

Reading TFAM file imputed.tfam ....


Reading kinship file imputedLD0.2.aBN.kinf...

  100 rows and 100 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderMLW.txt...

  100 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPopFraxinusImputedDAPC.txt...

  100 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 0.000000, evals[n-1] = 4.985196
k = 91,  n-q = 91
eRvals[0] = 0.130078, eRvals[n-q-1] = 3.985196, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.140000
etas[0] = -0.001497, lambdas[0] = 0.130078
etas[1] = -0.002257, lambdas[1] = 0.135780
etas[2] = -0.008042, lambdas[2] = 0.136573
etas[3] = -0.014684, lambdas[3] = 0.137881
etas[4] = -0.021931, lambdas[4] = 0.140680
etas[5] = -0.018553, lambdas[5] = 0.141315
etas[6] = 0.002694, lambdas[6] = 0.143108
etas[7] = -0.008934, lambdas[7] = 0.145229
etas[8] = -0.021823, lambdas[8] = 0.147537
etas[9] = -0.017878, lambdas[9] = 0.148861
etas[10] = -0.030266, lambdas[10] = 0.149744
etas[11] = -0.024172, lambdas[11] = 0.150969
etas[12] = -0.007781, lambdas[12] = 0.151619
etas[13] = -0.002359, lambdas[13] = 0.152900
etas[14] = -0.033647, lambdas[14] = 0.154264
etas[15] = -0.005750, lambdas[15] = 0.155412
etas[16] = 0.017832, lambdas[16] = 0.156639
etas[17] = -0.026185, lambdas[17] = 0.157679
etas[18] = 0.030933, lambdas[18] = 0.160953
etas[19] = -0.014581, lambdas[19] = 0.162092
etas[20] = -0.001270, lambdas[20] = 0.162960
etas[21] = -0.052776, lambdas[21] = 0.165364
etas[22] = -0.000702, lambdas[22] = 0.166686
etas[23] = 0.001907, lambdas[23] = 0.168981
etas[24] = -0.006652, lambdas[24] = 0.169402
etas[25] = -0.025427, lambdas[25] = 0.170663
etas[26] = -0.014147, lambdas[26] = 0.171582
etas[27] = -0.015323, lambdas[27] = 0.173644
etas[28] = 0.000743, lambdas[28] = 0.175394
etas[29] = 0.032058, lambdas[29] = 0.178569
etas[30] = -0.034159, lambdas[30] = 0.181141
etas[31] = 0.008324, lambdas[31] = 0.182844
etas[32] = 0.002875, lambdas[32] = 0.185579
etas[33] = -0.012651, lambdas[33] = 0.187017
etas[34] = 0.017058, lambdas[34] = 0.190860
etas[35] = 0.032760, lambdas[35] = 0.193298
etas[36] = 0.029641, lambdas[36] = 0.193535
etas[37] = 0.021396, lambdas[37] = 0.200104
etas[38] = -0.051527, lambdas[38] = 0.203163
etas[39] = 0.013918, lambdas[39] = 0.204487
etas[40] = 0.002665, lambdas[40] = 0.211116
etas[41] = -0.003105, lambdas[41] = 0.213197
etas[42] = 0.010599, lambdas[42] = 0.219688
etas[43] = -0.015987, lambdas[43] = 0.221869
etas[44] = 0.038893, lambdas[44] = 0.223454
etas[45] = -0.022577, lambdas[45] = 0.227155
etas[46] = 0.009543, lambdas[46] = 0.228339
etas[47] = 0.002454, lambdas[47] = 0.231061
etas[48] = -0.046988, lambdas[48] = 0.237735
etas[49] = -0.010899, lambdas[49] = 0.238958
etas[50] = 0.004508, lambdas[50] = 0.241818
etas[51] = 0.009159, lambdas[51] = 0.247345
etas[52] = 0.011618, lambdas[52] = 0.249351
etas[53] = 0.024697, lambdas[53] = 0.252158
etas[54] = 0.002941, lambdas[54] = 0.253567
etas[55] = -0.020408, lambdas[55] = 0.256513
etas[56] = -0.020875, lambdas[56] = 0.257778
etas[57] = -0.002174, lambdas[57] = 0.259132
etas[58] = -0.005437, lambdas[58] = 0.264140
etas[59] = -0.027873, lambdas[59] = 0.274087
etas[60] = -0.024993, lambdas[60] = 0.274603
etas[61] = 0.030153, lambdas[61] = 0.279161
etas[62] = -0.043895, lambdas[62] = 0.284471
etas[63] = -0.011255, lambdas[63] = 0.286792
etas[64] = 0.009708, lambdas[64] = 0.297662
etas[65] = 0.007767, lambdas[65] = 0.299033
etas[66] = 0.007712, lambdas[66] = 0.304936
etas[67] = -0.026136, lambdas[67] = 0.314002
etas[68] = 0.006999, lambdas[68] = 0.325118
etas[69] = 0.028724, lambdas[69] = 0.354461
etas[70] = 0.001213, lambdas[70] = 0.414638
etas[71] = -0.034269, lambdas[71] = 0.438702
etas[72] = -0.026712, lambdas[72] = 0.463051
etas[73] = 0.030322, lambdas[73] = 0.477633
etas[74] = -0.004950, lambdas[74] = 0.491703
etas[75] = -0.026271, lambdas[75] = 0.505573
etas[76] = -0.002935, lambdas[76] = 0.553688
etas[77] = -0.030393, lambdas[77] = 0.636021
etas[78] = -0.010040, lambdas[78] = 0.681860
etas[79] = 0.058958, lambdas[79] = 0.784785
etas[80] = 0.017717, lambdas[80] = 0.853428
etas[81] = -0.015876, lambdas[81] = 0.888070
etas[82] = 0.007294, lambdas[82] = 0.928393
etas[83] = -0.016861, lambdas[83] = 1.613943
etas[84] = -0.003814, lambdas[84] = 1.782806
etas[85] = -0.026610, lambdas[85] = 1.893792
etas[86] = 0.001761, lambdas[86] = 1.995695
etas[87] = -0.006054, lambdas[87] = 2.033285
etas[88] = 0.024468, lambdas[88] = 2.183938
etas[89] = -0.025703, lambdas[89] = 3.090552
etas[90] = -0.045948, lambdas[90] = 3.985196
0	31.986969	31.983773	212.037183
1	31.983773	31.979870	212.037504
2	31.979870	31.975103	212.037897
3	31.975103	31.969283	212.038376
4	31.969283	31.962176	212.038962
5	31.962176	31.953500	212.039677
6	31.953500	31.942908	212.040550
7	31.942908	31.929978	212.041616
8	31.929978	31.914197	212.042918
9	31.914197	31.894938	212.044507
10	31.894938	31.871442	212.046447
11	31.871442	31.842780	212.048816
12	31.842780	31.807830	212.051705
13	31.807830	31.765224	212.055232
14	31.765224	31.713310	212.059534
15	31.713310	31.650086	212.064780
16	31.650086	31.573139	212.071176
17	31.573139	31.479561	212.078971
18	31.479561	31.365869	212.088467
19	31.365869	31.227896	212.100026
20	31.227896	31.060691	212.114089
21	31.060691	30.858408	212.131181
22	30.858408	30.614191	212.151933
23	30.614191	30.320082	212.177097
24	30.320082	29.966953	212.207563
25	29.966953	29.544499	212.244380
26	29.544499	29.041305	212.288768
27	29.041305	28.445059	212.342140
28	28.445059	27.742930	212.406104
29	27.742930	26.922183	212.482464
30	26.922183	25.971072	212.573199
31	25.971072	24.880035	212.680425
32	24.880035	23.643172	212.806326
33	23.643172	22.259905	212.953047
34	22.259905	20.736648	213.122554
35	20.736648	19.088185	213.316444
36	19.088185	17.338412	213.535742
37	17.338412	15.520078	213.780661
38	15.520078	13.673285	214.050400
39	13.673285	11.842727	214.342970
40	11.842727	10.073954	214.655123
41	10.073954	8.409224	214.982382
42	8.409224	6.883680	215.319211
43	6.883680	5.522532	215.659302
44	5.522532	4.339695	215.995948
45	4.339695	3.337991	216.322458
46	3.337991	2.510671	216.632555
47	2.510671	1.843785	216.920709
48	1.843785	1.318857	217.182376
49	1.318857	0.915366	217.414134
50	0.915366	0.612729	217.613739
51	0.612729	0.391660	217.780111
52	0.391660	0.234934	217.913287
53	0.234934	0.127708	218.014353
54	0.127708	0.057545	218.085343
55	0.057545	0.014283	218.129114
56	0.014283	-0.010167	218.149169
57	-0.010167	-0.022056	218.149442
58	-0.022056	-0.026044	218.134055
59	-0.026044	-0.025459	218.107060
60	-0.025459	-0.022558	218.072218
61	-0.022558	-0.018777	218.032820
62	-0.018777	-0.014954	217.991587
63	-0.014954	-0.011520	217.950629
64	-0.011520	-0.008648	217.911468
65	-0.008648	-0.006358	217.875107
66	-0.006358	-0.004598	217.842120
67	-0.004598	-0.003280	217.812745
68	-0.003280	-0.002314	217.786978
69	-0.002314	-0.001617	217.764654
70	-0.001617	-0.001122	217.745507
71	-0.001122	-0.000774	217.729222
72	-0.000774	-0.000531	217.715464
73	-0.000531	-0.000363	217.703906
74	-0.000363	-0.000247	217.694242
75	-0.000247	-0.000168	217.686192
76	-0.000168	-0.000114	217.679506
77	-0.000114	-0.000077	217.673969
78	-0.000077	-0.000052	217.669391
79	-0.000052	-0.000035	217.665614
80	-0.000035	-0.000024	217.662502
81	-0.000024	-0.000016	217.659941
82	-0.000016	-0.000011	217.657835
83	-0.000011	-0.000007	217.656104
84	-0.000007	-0.000005	217.654683
85	-0.000005	-0.000003	217.653517
86	-0.000003	-0.000002	217.652561
87	-0.000002	-0.000001	217.651776
88	-0.000001	-0.000001	217.651133
89	-0.000001	-0.000001	217.650606
90	-0.000001	-0.000000	217.650174
91	-0.000000	-0.000000	217.649820
92	-0.000000	-0.000000	217.649530
93	-0.000000	-0.000000	217.649293
94	-0.000000	-0.000000	217.649099
95	-0.000000	-0.000000	217.648939
96	-0.000000	-0.000000	217.648809
97	-0.000000	-0.000000	217.648702
98	-0.000000	-0.000000	217.648615
99	-0.000000	-0.000000	217.648543
REMLE (delta=3.673599, REML=218.151522, REML0=217.648219)- elapsed CPU time is 0.000000
eigen_L - elapsed CPU time is 0.000000


Reading TPED file imputed...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

Reading 30000-th SNP and testing association....

Reading 40000-th SNP and testing association....

Reading 50000-th SNP and testing association....

Reading 60000-th SNP and testing association....

Reading 70000-th SNP and testing association....

Reading 80000-th SNP and testing association....

Reading 90000-th SNP and testing association....

Reading 100000-th SNP and testing association....

Reading 110000-th SNP and testing association....

Reading 120000-th SNP and testing association....

Reading 130000-th SNP and testing association....

Reading 140000-th SNP and testing association....

Reading 150000-th SNP and testing association....

Reading 160000-th SNP and testing association....

Reading 170000-th SNP and testing association....

Reading 180000-th SNP and testing association....

Reading 190000-th SNP and testing association....

Reading 200000-th SNP and testing association....

Reading 210000-th SNP and testing association....

Reading 220000-th SNP and testing association....

Reading 230000-th SNP and testing association....

Reading 240000-th SNP and testing association....

Reading 250000-th SNP and testing association....

Reading 260000-th SNP and testing association....

Reading 270000-th SNP and testing association....

Reading 280000-th SNP and testing association....

Reading 290000-th SNP and testing association....

Reading 300000-th SNP and testing association....

Reading 310000-th SNP and testing association....

Reading 320000-th SNP and testing association....

Reading 330000-th SNP and testing association....

Reading 340000-th SNP and testing association....

Reading 350000-th SNP and testing association....

Reading 360000-th SNP and testing association....

Reading 370000-th SNP and testing association....

Reading 380000-th SNP and testing association....

Reading 390000-th SNP and testing association....

Reading 400000-th SNP and testing association....

Reading 410000-th SNP and testing association....

Reading 420000-th SNP and testing association....

Reading 430000-th SNP and testing association....

Reading 440000-th SNP and testing association....

Reading 450000-th SNP and testing association....

Reading 460000-th SNP and testing association....

Reading 470000-th SNP and testing association....

Reading 480000-th SNP and testing association....

Reading 490000-th SNP and testing association....

Reading 500000-th SNP and testing association....

Reading 510000-th SNP and testing association....

Reading 520000-th SNP and testing association....

Reading 530000-th SNP and testing association....

Reading 540000-th SNP and testing association....

Reading 550000-th SNP and testing association....

Reading 560000-th SNP and testing association....

Reading 570000-th SNP and testing association....

Reading 580000-th SNP and testing association....

Reading 590000-th SNP and testing association....

Reading 600000-th SNP and testing association....

Reading 610000-th SNP and testing association....

Reading 620000-th SNP and testing association....

Reading 630000-th SNP and testing association....

Reading 640000-th SNP and testing association....

Reading 650000-th SNP and testing association....

Reading 660000-th SNP and testing association....

Reading 670000-th SNP and testing association....

Reading 680000-th SNP and testing association....

Reading 690000-th SNP and testing association....

Reading 700000-th SNP and testing association....

Reading 710000-th SNP and testing association....

Reading 720000-th SNP and testing association....

Reading 730000-th SNP and testing association....

Reading 740000-th SNP and testing association....

Reading 750000-th SNP and testing association....

Reading 760000-th SNP and testing association....

Reading 770000-th SNP and testing association....

Reading 780000-th SNP and testing association....

Reading 790000-th SNP and testing association....

Reading 800000-th SNP and testing association....

Reading 810000-th SNP and testing association....

Reading 820000-th SNP and testing association....

Reading 830000-th SNP and testing association....

Reading 840000-th SNP and testing association....

Reading 850000-th SNP and testing association....

Reading 860000-th SNP and testing association....

Reading 870000-th SNP and testing association....

Reading 880000-th SNP and testing association....

Reading 890000-th SNP and testing association....

Reading 900000-th SNP and testing association....

Reading 910000-th SNP and testing association....

Reading 920000-th SNP and testing association....

Reading 930000-th SNP and testing association....

GLS association - elapsed CPU time is 44.020000

Breakdown for input file parsing : 7.220000

              matrix computation : 31.920000

             p-value computation : 0.890000
```

The results are included in HKPassocImputed.ps and MLWassocImputed.ps

Each line consists on:
1. SNP ID
2. Beta (1 is effect allele)
3. SE(beta)
4. p-value.

The HKP association results file (HKPassocImputed.ps) contained 35,188 significant SNPs (P<0.05) and the MLW file, 35,584 significant SNPs. However, it is necessary to correct the p-values by multiple testing, since the imputed file (imputed.vcf) had 930,660 SNPs.

I performed FDR and Bonferroni corrections for the p-values of both files. After both FDR and Boferroni testing, I obtained 0 significant SNPs for both phenotypes and both corrections. Both corrections were performed in R using the following code:

```
padjustMLW<-p.adjust(MLW$V4, method = "BH", n = 930660)
padjustMLWBonf<-p.adjust(MLW$V4, method = "bonferroni", n = 930660)
```

I try performing q-value instead to perform multiple testing correction. It was performed in R using the following code:

```
qvalueHKP <- qvalue(p = pvalueHKP)
qvalueMLW <- qvalue(p = pvalueMLW)
```
And produced the following results:

HKP:
```
Cumulative number of significant calls:

          <1e-04 <0.001 <0.01 <0.025 <0.05  <0.1     <1
p-value      275    984 11247  23649 35188 63097 930658
q-value        0      0     0      0     0     0  23833
local FDR      0      0     0      0     0     0      7
```

MLW:
```
Cumulative number of significant calls:

          <1e-04 <0.001 <0.01 <0.025 <0.05  <0.1     <1
p-value       53    485  5174  14830 35584 73552 930660
q-value        0      0     0      0     0     0 930660
local FDR      0      0     0      0     0     0      5
```

MultiTrans: https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0903-6#Sec31
This article mention that for Linear Mixed Models (LMM) such as EMMAX,  Multiple comparison adjustments (Bonferroni, false discovery rate, and positive false discovery rate) are overly conservative (https://www.frontiersin.org/articles/10.3389/fpls.2019.01794/full)

**MultiTrans**

Working directory: /labs/Wegrzyn/IreneCobo/Fraxinus/LinkImputeRJeremy
Tutorial: http://genetics.cs.ucla.edu/multiTrans/install.html

Prepare the impute files:

1. Genotype file 

- Transform the .ped file into a 0,1,2 matrix (0 and 2 homocygous, 1 heterozygous)

```
plink --file imputed --recode AD --out imputedcompgeno 
```
(I tried using "A" because "AD" was Aditive+Dominant, and I want it just additive, that is, one column per marker)
```
plink --file imputed --recode A --out imputedcompgenoA
```
- Remove the first column of a tab or space delimited file (I repeated it six times, to remove the first six columns)
```
awk '{$1=""}1' imputedcompgeno5.txt | awk '{$1=$1}1' > imputedcompgeno6.txt 
```

- Remove the first line of a file
```
sed '1d' imputedcompgeno6.txt > imputedcompgenonohead.txt
```
- Run the first part of MultiTrans (working directory: /labs/Wegrzyn/IreneCobo/Fraxinus/LinkImputeRJeremy/MultiTrans)
```
module load R/3.5.1
R CMD BATCH --args -Xpath="imputedcompgenonoheadA.txt" -Kpath="imputedLD0.2.aBN.kinf" -VCpath="VC.txt" -outputPath="outputMultiTrans1" generateR.R generateR.log
```
It produces a generateR.log file as output. There, it can be found the following error:

```
Error: cannot allocate vector of size 6453.2 Gb
Execution halted
```


**EMMAX using just kinship correction**

It is possible that we are overcorrecting by including kinship matrix and population structure (they must contain similar information). Overcorrection by kinship/population structure can lead to the loose of important information. Thus, we tried running EMMAX only using the kinship matrix:

**MLW phenotype**

```
./emmax-intel64 -v -d 10 -t imputed -p phenoEMMAXorderMLW.txt -k imputedLD0.2.aBN.kinf -o MLWassocImputedonlykinship
```
```

Reading TFAM file imputed.tfam ....


Reading kinship file imputedLD0.2.aBN.kinf...

  100 rows and 100 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderMLW.txt...

  100 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

No covariates were found... using intercept only 

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 1.130064, evals[n-1] = 5.050847
k = 92,  n-q = 92
eRvals[0] = 0.130064, eRvals[n-q-1] = 4.050847, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.000000
etas[0] = 0.001437, lambdas[0] = 0.130064
etas[1] = 0.002216, lambdas[1] = 0.135780
etas[2] = 0.007978, lambdas[2] = 0.136549
etas[3] = -0.014646, lambdas[3] = 0.137864
etas[4] = 0.021242, lambdas[4] = 0.140646
etas[5] = 0.019400, lambdas[5] = 0.141287
etas[6] = -0.002472, lambdas[6] = 0.143087
etas[7] = 0.008803, lambdas[7] = 0.145226
etas[8] = 0.021798, lambdas[8] = 0.147537
etas[9] = 0.016981, lambdas[9] = 0.148606
etas[10] = 0.030773, lambdas[10] = 0.149742
etas[11] = 0.024146, lambdas[11] = 0.150950
etas[12] = -0.008403, lambdas[12] = 0.151591
etas[13] = 0.002266, lambdas[13] = 0.152883
etas[14] = 0.033559, lambdas[14] = 0.154259
etas[15] = 0.005550, lambdas[15] = 0.155410
etas[16] = 0.018295, lambdas[16] = 0.156616
etas[17] = 0.025887, lambdas[17] = 0.157667
etas[18] = -0.030826, lambdas[18] = 0.160953
etas[19] = 0.014956, lambdas[19] = 0.162082
etas[20] = 0.000503, lambdas[20] = 0.162869
etas[21] = 0.052860, lambdas[21] = 0.165344
etas[22] = 0.001466, lambdas[22] = 0.166662
etas[23] = -0.002613, lambdas[23] = 0.168879
etas[24] = 0.005927, lambdas[24] = 0.169356
etas[25] = 0.025691, lambdas[25] = 0.170654
etas[26] = -0.014152, lambdas[26] = 0.171579
etas[27] = 0.014521, lambdas[27] = 0.173473
etas[28] = -0.001192, lambdas[28] = 0.175386
etas[29] = -0.032098, lambdas[29] = 0.178569
etas[30] = 0.034123, lambdas[30] = 0.181105
etas[31] = -0.008435, lambdas[31] = 0.182843
etas[32] = 0.000361, lambdas[32] = 0.185185
etas[33] = -0.013575, lambdas[33] = 0.186841
etas[34] = -0.017169, lambdas[34] = 0.190859
etas[35] = -0.016391, lambdas[35] = 0.193224
etas[36] = -0.040086, lambdas[36] = 0.193304
etas[37] = -0.021617, lambdas[37] = 0.200103
etas[38] = 0.052897, lambdas[38] = 0.203151
etas[39] = -0.009683, lambdas[39] = 0.203768
etas[40] = -0.003490, lambdas[40] = 0.210986
etas[41] = 0.003279, lambdas[41] = 0.212743
etas[42] = -0.010634, lambdas[42] = 0.219638
etas[43] = 0.013230, lambdas[43] = 0.221702
etas[44] = 0.039923, lambdas[44] = 0.223405
etas[45] = 0.022257, lambdas[45] = 0.227140
etas[46] = -0.010022, lambdas[46] = 0.228326
etas[47] = -0.003170, lambdas[47] = 0.230138
etas[48] = 0.047069, lambdas[48] = 0.237727
etas[49] = 0.010026, lambdas[49] = 0.238915
etas[50] = -0.004237, lambdas[50] = 0.241807
etas[51] = -0.008784, lambdas[51] = 0.247318
etas[52] = -0.011791, lambdas[52] = 0.249318
etas[53] = -0.024244, lambdas[53] = 0.252094
etas[54] = -0.002733, lambdas[54] = 0.253566
etas[55] = 0.017801, lambdas[55] = 0.256404
etas[56] = 0.022537, lambdas[56] = 0.257750
etas[57] = 0.002469, lambdas[57] = 0.259128
etas[58] = -0.003136, lambdas[58] = 0.264050
etas[59] = 0.042582, lambdas[59] = 0.272366
etas[60] = 0.001001, lambdas[60] = 0.274340
etas[61] = -0.023414, lambdas[61] = 0.278667
etas[62] = 0.042367, lambdas[62] = 0.284443
etas[63] = 0.010175, lambdas[63] = 0.286782
etas[64] = 0.001885, lambdas[64] = 0.296744
etas[65] = -0.011582, lambdas[65] = 0.298850
etas[66] = -0.005064, lambdas[66] = 0.304729
etas[67] = 0.028068, lambdas[67] = 0.313816
etas[68] = -0.001438, lambdas[68] = 0.323363
etas[69] = -0.033368, lambdas[69] = 0.347442
etas[70] = 0.004017, lambdas[70] = 0.406137
etas[71] = -0.005698, lambdas[71] = 0.429797
etas[72] = 0.042916, lambdas[72] = 0.440419
etas[73] = -0.026465, lambdas[73] = 0.477190
etas[74] = 0.005366, lambdas[74] = 0.491689
etas[75] = 0.020197, lambdas[75] = 0.503717
etas[76] = 0.002844, lambdas[76] = 0.553688
etas[77] = 0.007326, lambdas[77] = 0.626749
etas[78] = 0.034912, lambdas[78] = 0.645837
etas[79] = -0.057362, lambdas[79] = 0.784306
etas[80] = -0.023092, lambdas[80] = 0.841503
etas[81] = -0.008485, lambdas[81] = 0.864653
etas[82] = -0.023358, lambdas[82] = 0.910871
etas[83] = 0.011518, lambdas[83] = 0.946455
etas[84] = 0.004627, lambdas[84] = 1.737257
etas[85] = -0.004071, lambdas[85] = 1.791805
etas[86] = -0.026553, lambdas[86] = 1.893818
etas[87] = 0.005834, lambdas[87] = 2.032948
etas[88] = -0.011873, lambdas[88] = 2.095747
etas[89] = -0.018840, lambdas[89] = 2.218256
etas[90] = -0.012491, lambdas[90] = 3.610883
etas[91] = -0.052110, lambdas[91] = 4.050847
0	33.708549	33.705224	214.262056
1	33.705224	33.701164	214.262395
2	33.701164	33.696207	214.262809
3	33.696207	33.690153	214.263314
4	33.690153	33.682762	214.263931
5	33.682762	33.673737	214.264685
6	33.673737	33.662720	214.265605
7	33.662720	33.649271	214.266729
8	33.649271	33.632857	214.268100
9	33.632857	33.612826	214.269775
10	33.612826	33.588386	214.271820
11	33.588386	33.558574	214.274316
12	33.558574	33.522219	214.277361
13	33.522219	33.477902	214.281077
14	33.477902	33.423902	214.285611
15	33.423902	33.358136	214.291140
16	33.358136	33.278093	214.297882
17	33.278093	33.180748	214.306098
18	33.180748	33.062473	214.316106
19	33.062473	32.918934	214.328291
20	32.918934	32.744975	214.343114
21	32.744975	32.534508	214.361132
22	32.534508	32.280389	214.383011
23	32.280389	31.974327	214.409543
24	31.974327	31.606806	214.441670
25	31.606806	31.167072	214.480497
26	31.167072	30.643209	214.527319
27	30.643209	30.022345	214.583629
28	30.022345	29.291046	214.651131
29	29.291046	28.435944	214.731738
30	28.435944	27.444666	214.827556
31	27.444666	26.307059	214.940838
32	26.307059	25.016730	215.073921
33	25.016730	23.572772	215.229112
34	23.572772	21.981505	215.408540
35	21.981505	20.257932	215.613968
36	20.257932	18.426545	215.846565
37	18.426545	16.521105	216.106673
38	16.521105	14.583138	216.393572
39	14.583138	12.659123	216.705307
40	12.659123	10.796649	217.038598
41	10.796649	9.040121	217.388867
42	9.040121	7.426750	217.750401
43	7.426750	5.983559	218.116646
44	5.983559	4.725864	218.480598
45	4.725864	3.657374	218.835230
46	3.657374	2.771681	219.173913
47	2.771681	2.054686	219.490770
48	2.054686	1.487373	219.780924
49	1.487373	1.048448	220.040642
50	1.048448	0.716464	220.267376
51	0.716464	0.471298	220.459752
52	0.471298	0.294980	220.617505
53	0.294980	0.172029	220.741417
54	0.172029	0.089463	220.833233
55	0.089463	0.036634	220.895559
56	0.036634	0.005002	220.931717
57	0.005002	-0.012113	220.945561
58	-0.012113	-0.019774	220.941238
59	-0.019774	-0.021676	220.922942
60	-0.021676	-0.020393	220.894663
61	-0.020393	-0.017620	220.859988
62	-0.017620	-0.014395	220.821958
63	-0.014395	-0.011297	220.783003
64	-0.011297	-0.008599	220.744941
65	-0.008599	-0.006391	220.709029
66	-0.006391	-0.004661	220.676046
67	-0.004661	-0.003347	220.646393
68	-0.003347	-0.002374	220.620185
69	-0.002374	-0.001667	220.597341
70	-0.001667	-0.001160	220.577653
71	-0.001160	-0.000802	220.560841
72	-0.000802	-0.000552	220.546594
73	-0.000552	-0.000378	220.534595
74	-0.000378	-0.000258	220.524540
75	-0.000258	-0.000175	220.516150
76	-0.000175	-0.000119	220.509173
77	-0.000119	-0.000080	220.503387
78	-0.000080	-0.000054	220.498600
79	-0.000054	-0.000037	220.494647
80	-0.000037	-0.000025	220.491388
81	-0.000025	-0.000017	220.488704
82	-0.000017	-0.000011	220.486497
83	-0.000011	-0.000008	220.484682
84	-0.000008	-0.000005	220.483192
85	-0.000005	-0.000003	220.481968
86	-0.000003	-0.000002	220.480965
87	-0.000002	-0.000002	220.480142
88	-0.000002	-0.000001	220.479467
89	-0.000001	-0.000001	220.478913
90	-0.000001	-0.000000	220.478460
91	-0.000000	-0.000000	220.478089
92	-0.000000	-0.000000	220.477784
93	-0.000000	-0.000000	220.477535
94	-0.000000	-0.000000	220.477331
95	-0.000000	-0.000000	220.477163
96	-0.000000	-0.000000	220.477027
97	-0.000000	-0.000000	220.476914
98	-0.000000	-0.000000	220.476822
99	-0.000000	-0.000000	220.476747
REMLE (delta=4.245665, REML=220.946022, REML0=220.476407)- elapsed CPU time is 0.000000
eigen_L - elapsed CPU time is 0.000000


Reading TPED file imputed...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

Reading 30000-th SNP and testing association....

Reading 40000-th SNP and testing association....

Reading 50000-th SNP and testing association....

Reading 60000-th SNP and testing association....

Reading 70000-th SNP and testing association....

Reading 80000-th SNP and testing association....

Reading 90000-th SNP and testing association....

Reading 100000-th SNP and testing association....

Reading 110000-th SNP and testing association....

Reading 120000-th SNP and testing association....

Reading 130000-th SNP and testing association....

Reading 140000-th SNP and testing association....

Reading 150000-th SNP and testing association....

Reading 160000-th SNP and testing association....

Reading 170000-th SNP and testing association....

Reading 180000-th SNP and testing association....

Reading 190000-th SNP and testing association....

Reading 200000-th SNP and testing association....

Reading 210000-th SNP and testing association....

Reading 220000-th SNP and testing association....

Reading 230000-th SNP and testing association....

Reading 240000-th SNP and testing association....

Reading 250000-th SNP and testing association....

Reading 260000-th SNP and testing association....

Reading 270000-th SNP and testing association....

Reading 280000-th SNP and testing association....

Reading 290000-th SNP and testing association....

Reading 300000-th SNP and testing association....

Reading 310000-th SNP and testing association....

Reading 320000-th SNP and testing association....

Reading 330000-th SNP and testing association....

Reading 340000-th SNP and testing association....

Reading 350000-th SNP and testing association....

Reading 360000-th SNP and testing association....

Reading 370000-th SNP and testing association....

Reading 380000-th SNP and testing association....

Reading 390000-th SNP and testing association....

Reading 400000-th SNP and testing association....

Reading 410000-th SNP and testing association....

Reading 420000-th SNP and testing association....

Reading 430000-th SNP and testing association....

Reading 440000-th SNP and testing association....

Reading 450000-th SNP and testing association....

Reading 460000-th SNP and testing association....

Reading 470000-th SNP and testing association....

Reading 480000-th SNP and testing association....

Reading 490000-th SNP and testing association....

Reading 500000-th SNP and testing association....

Reading 510000-th SNP and testing association....

Reading 520000-th SNP and testing association....

Reading 530000-th SNP and testing association....

Reading 540000-th SNP and testing association....

Reading 550000-th SNP and testing association....

Reading 560000-th SNP and testing association....

Reading 570000-th SNP and testing association....

Reading 580000-th SNP and testing association....

Reading 590000-th SNP and testing association....

Reading 600000-th SNP and testing association....

Reading 610000-th SNP and testing association....

Reading 620000-th SNP and testing association....

Reading 630000-th SNP and testing association....

Reading 640000-th SNP and testing association....

Reading 650000-th SNP and testing association....

Reading 660000-th SNP and testing association....

Reading 670000-th SNP and testing association....

Reading 680000-th SNP and testing association....

Reading 690000-th SNP and testing association....

Reading 700000-th SNP and testing association....

Reading 710000-th SNP and testing association....

Reading 720000-th SNP and testing association....

Reading 730000-th SNP and testing association....

Reading 740000-th SNP and testing association....

Reading 750000-th SNP and testing association....

Reading 760000-th SNP and testing association....

Reading 770000-th SNP and testing association....

Reading 780000-th SNP and testing association....

Reading 790000-th SNP and testing association....

Reading 800000-th SNP and testing association....

Reading 810000-th SNP and testing association....

Reading 820000-th SNP and testing association....

Reading 830000-th SNP and testing association....

Reading 840000-th SNP and testing association....

Reading 850000-th SNP and testing association....

Reading 860000-th SNP and testing association....

Reading 870000-th SNP and testing association....

Reading 880000-th SNP and testing association....

Reading 890000-th SNP and testing association....

Reading 900000-th SNP and testing association....

Reading 910000-th SNP and testing association....

Reading 920000-th SNP and testing association....

Reading 930000-th SNP and testing association....

GLS association - elapsed CPU time is 24.680000

Breakdown for input file parsing : 10.030000

              matrix computation : 7.540000

             p-value computation : 1.380000
```

**HKP phenotype**

```
./emmax-intel64 -v -d 10 -t imputed -p phenoEMMAXorderHKP.txt -k imputedLD0.2.aBN.kinf -o HKPassocImputedonlykinship
```
```

Reading TFAM file imputed.tfam ....


Reading kinship file imputedLD0.2.aBN.kinf...

  100 rows and 100 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderHKP.txt...

  100 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

No covariates were found... using intercept only 

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 1.130064, evals[n-1] = 5.050847
k = 92,  n-q = 92
eRvals[0] = 0.130064, eRvals[n-q-1] = 4.050847, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.000000
etas[0] = 0.328152, lambdas[0] = 0.130064
etas[1] = -0.000205, lambdas[1] = 0.135780
etas[2] = -0.094195, lambdas[2] = 0.136549
etas[3] = -0.057451, lambdas[3] = 0.137864
etas[4] = 0.083293, lambdas[4] = 0.140646
etas[5] = -0.097545, lambdas[5] = 0.141287
etas[6] = 0.177899, lambdas[6] = 0.143087
etas[7] = 0.146611, lambdas[7] = 0.145226
etas[8] = 0.063903, lambdas[8] = 0.147537
etas[9] = -0.224472, lambdas[9] = 0.148606
etas[10] = -0.247263, lambdas[10] = 0.149742
etas[11] = -0.297086, lambdas[11] = 0.150950
etas[12] = 0.137364, lambdas[12] = 0.151591
etas[13] = -0.159667, lambdas[13] = 0.152883
etas[14] = -0.261652, lambdas[14] = 0.154259
etas[15] = 0.167401, lambdas[15] = 0.155410
etas[16] = 0.054995, lambdas[16] = 0.156616
etas[17] = -0.177912, lambdas[17] = 0.157667
etas[18] = 0.305897, lambdas[18] = 0.160953
etas[19] = -0.042291, lambdas[19] = 0.162082
etas[20] = 0.113424, lambdas[20] = 0.162869
etas[21] = -0.253844, lambdas[21] = 0.165344
etas[22] = -0.096821, lambdas[22] = 0.166662
etas[23] = -0.325149, lambdas[23] = 0.168879
etas[24] = -0.092589, lambdas[24] = 0.169356
etas[25] = -0.379016, lambdas[25] = 0.170654
etas[26] = -0.271531, lambdas[26] = 0.171579
etas[27] = -0.302594, lambdas[27] = 0.173473
etas[28] = 0.449947, lambdas[28] = 0.175386
etas[29] = 0.093602, lambdas[29] = 0.178569
etas[30] = 0.146862, lambdas[30] = 0.181105
etas[31] = 0.036213, lambdas[31] = 0.182843
etas[32] = 0.117924, lambdas[32] = 0.185185
etas[33] = -0.044423, lambdas[33] = 0.186841
etas[34] = 0.279872, lambdas[34] = 0.190859
etas[35] = 0.063882, lambdas[35] = 0.193224
etas[36] = -0.144615, lambdas[36] = 0.193304
etas[37] = 0.013520, lambdas[37] = 0.200103
etas[38] = -0.102901, lambdas[38] = 0.203151
etas[39] = -0.117887, lambdas[39] = 0.203768
etas[40] = 0.084998, lambdas[40] = 0.210986
etas[41] = 0.249726, lambdas[41] = 0.212743
etas[42] = -0.079368, lambdas[42] = 0.219638
etas[43] = -0.261490, lambdas[43] = 0.221702
etas[44] = -0.228494, lambdas[44] = 0.223405
etas[45] = -0.117460, lambdas[45] = 0.227140
etas[46] = -0.000067, lambdas[46] = 0.228326
etas[47] = 0.104565, lambdas[47] = 0.230138
etas[48] = 0.137732, lambdas[48] = 0.237727
etas[49] = -0.176072, lambdas[49] = 0.238915
etas[50] = 0.086962, lambdas[50] = 0.241807
etas[51] = 0.261156, lambdas[51] = 0.247318
etas[52] = 0.211918, lambdas[52] = 0.249318
etas[53] = -0.104926, lambdas[53] = 0.252094
etas[54] = -0.003690, lambdas[54] = 0.253566
etas[55] = -0.134353, lambdas[55] = 0.256404
etas[56] = -0.295675, lambdas[56] = 0.257750
etas[57] = 0.131897, lambdas[57] = 0.259128
etas[58] = -0.128712, lambdas[58] = 0.264050
etas[59] = -0.080290, lambdas[59] = 0.272366
etas[60] = -0.314914, lambdas[60] = 0.274340
etas[61] = -0.241110, lambdas[61] = 0.278667
etas[62] = -0.209486, lambdas[62] = 0.284443
etas[63] = -0.239669, lambdas[63] = 0.286782
etas[64] = 0.122341, lambdas[64] = 0.296744
etas[65] = 0.071016, lambdas[65] = 0.298850
etas[66] = 0.213674, lambdas[66] = 0.304729
etas[67] = -0.184150, lambdas[67] = 0.313816
etas[68] = 0.104527, lambdas[68] = 0.323363
etas[69] = 0.051853, lambdas[69] = 0.347442
etas[70] = -0.094459, lambdas[70] = 0.406137
etas[71] = -0.120653, lambdas[71] = 0.429797
etas[72] = 0.236987, lambdas[72] = 0.440419
etas[73] = -0.325902, lambdas[73] = 0.477190
etas[74] = 0.164292, lambdas[74] = 0.491689
etas[75] = 0.092413, lambdas[75] = 0.503717
etas[76] = -0.037849, lambdas[76] = 0.553688
etas[77] = -0.109857, lambdas[77] = 0.626749
etas[78] = -0.094254, lambdas[78] = 0.645837
etas[79] = 0.283489, lambdas[79] = 0.784306
etas[80] = 0.510617, lambdas[80] = 0.841503
etas[81] = 0.196589, lambdas[81] = 0.864653
etas[82] = 0.103471, lambdas[82] = 0.910871
etas[83] = 0.214169, lambdas[83] = 0.946455
etas[84] = -0.009111, lambdas[84] = 1.737257
etas[85] = 0.160284, lambdas[85] = 1.791805
etas[86] = 0.069796, lambdas[86] = 1.893818
etas[87] = -0.232913, lambdas[87] = 2.032948
etas[88] = -0.124774, lambdas[88] = 2.095747
etas[89] = 0.295056, lambdas[89] = 2.218256
etas[90] = 0.018382, lambdas[90] = 3.610883
etas[91] = 0.154430, lambdas[91] = 4.050847
0	47.709979	47.705048	11.744926
1	47.705048	47.699026	11.745405
2	47.699026	47.691672	11.745991
3	47.691672	47.682693	11.746706
4	47.682693	47.671730	11.747580
5	47.671730	47.658344	11.748646
6	47.658344	47.642004	11.749949
7	47.642004	47.622057	11.751539
8	47.622057	47.597713	11.753480
9	47.597713	47.568007	11.755851
10	47.568007	47.531764	11.758744
11	47.531764	47.487556	11.762276
12	47.487556	47.433652	11.766586
13	47.433652	47.367946	11.771844
14	47.367946	47.287893	11.778259
15	47.287893	47.190411	11.786082
16	47.190411	47.071787	11.795620
17	47.071787	46.927550	11.807242
18	46.927550	46.752344	11.821398
19	46.752344	46.539775	11.838629
20	46.539775	46.282251	11.859588
21	46.282251	45.970814	11.885058
22	45.970814	45.594985	11.915977
23	45.594985	45.142623	11.953460
24	45.142623	44.599839	11.998827
25	44.599839	43.951004	12.053631
26	43.951004	43.178886	12.119679
27	43.178886	42.265001	12.199055
28	42.265001	41.190238	12.294127
29	41.190238	39.935838	12.407543
30	39.935838	38.484808	12.542197
31	38.484808	36.823767	12.701171
32	36.823767	34.945201	12.887623
33	34.945201	32.849962	13.104629
34	32.849962	30.549693	13.354969
35	30.549693	28.068761	13.640857
36	28.068761	25.445132	13.963627
37	25.445132	22.729703	14.323415
38	22.729703	19.983759	14.718859
39	19.983759	17.274588	15.146891
40	17.274588	14.669764	15.602651
41	14.669764	12.230968	16.079593
42	12.230968	10.008451	16.569767
43	10.008451	8.037093	17.064287
44	8.037093	6.334673	17.553916
45	6.334673	4.902423	18.029704
46	4.902423	3.727480	18.483585
47	3.727480	2.786536	18.908854
48	2.786536	2.049907	19.300479
49	2.049907	1.485341	19.655221
50	1.485341	1.061082	19.971582
51	1.061082	0.748010	20.249604
52	0.748010	0.520836	20.490603
53	0.520836	0.358537	20.696850
54	0.358537	0.244245	20.871269
55	0.244245	0.164828	21.017166
56	0.164828	0.110318	21.137997
57	0.110318	0.073317	21.237186
58	0.073317	0.048447	21.317987
59	0.048447	0.031872	21.383387
60	0.031872	0.020903	21.436052
61	0.020903	0.013683	21.478303
62	0.013683	0.008949	21.512113
63	0.008949	0.005854	21.539132
64	0.005854	0.003833	21.560717
65	0.003833	0.002513	21.577969
66	0.002513	0.001650	21.591774
67	0.001650	0.001086	21.602837
68	0.001086	0.000716	21.611721
69	0.000716	0.000473	21.618869
70	0.000473	0.000313	21.624632
71	0.000313	0.000208	21.629287
72	0.000208	0.000138	21.633055
73	0.000138	0.000092	21.636109
74	0.000092	0.000061	21.638588
75	0.000061	0.000041	21.640604
76	0.000041	0.000027	21.642244
77	0.000027	0.000018	21.643581
78	0.000018	0.000012	21.644670
79	0.000012	0.000008	21.645559
80	0.000008	0.000005	21.646285
81	0.000005	0.000004	21.646878
82	0.000004	0.000002	21.647362
83	0.000002	0.000002	21.647758
84	0.000002	0.000001	21.648082
85	0.000001	0.000001	21.648347
86	0.000001	0.000000	21.648563
87	0.000000	0.000000	21.648740
88	0.000000	0.000000	21.648885
89	0.000000	0.000000	21.649004
90	0.000000	0.000000	21.649101
91	0.000000	0.000000	21.649181
92	0.000000	0.000000	21.649246
93	0.000000	0.000000	21.649299
94	0.000000	0.000000	21.649343
95	0.000000	0.000000	21.649378
96	0.000000	0.000000	21.649407
97	0.000000	0.000000	21.649431
98	0.000000	0.000000	21.649451
99	0.000000	0.000000	21.649467
REMLE (delta=22026.465795, REML=21.649480, REML0=21.649539)- elapsed CPU time is 0.000000
eigen_L - elapsed CPU time is 0.010000


Reading TPED file imputed...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

Reading 30000-th SNP and testing association....

Reading 40000-th SNP and testing association....

Reading 50000-th SNP and testing association....

Reading 60000-th SNP and testing association....

Reading 70000-th SNP and testing association....

Reading 80000-th SNP and testing association....

Reading 90000-th SNP and testing association....

Reading 100000-th SNP and testing association....

Reading 110000-th SNP and testing association....

Reading 120000-th SNP and testing association....

Reading 130000-th SNP and testing association....

Reading 140000-th SNP and testing association....

Reading 150000-th SNP and testing association....

Reading 160000-th SNP and testing association....

Reading 170000-th SNP and testing association....

Reading 180000-th SNP and testing association....

Reading 190000-th SNP and testing association....

Reading 200000-th SNP and testing association....

Reading 210000-th SNP and testing association....

Reading 220000-th SNP and testing association....

Reading 230000-th SNP and testing association....

Reading 240000-th SNP and testing association....

Reading 250000-th SNP and testing association....

Reading 260000-th SNP and testing association....

Reading 270000-th SNP and testing association....

Reading 280000-th SNP and testing association....

Reading 290000-th SNP and testing association....

Reading 300000-th SNP and testing association....

Reading 310000-th SNP and testing association....

Reading 320000-th SNP and testing association....

Reading 330000-th SNP and testing association....

Reading 340000-th SNP and testing association....

Reading 350000-th SNP and testing association....

Reading 360000-th SNP and testing association....

Reading 370000-th SNP and testing association....

Reading 380000-th SNP and testing association....

Reading 390000-th SNP and testing association....

Reading 400000-th SNP and testing association....

Reading 410000-th SNP and testing association....

Reading 420000-th SNP and testing association....

Reading 430000-th SNP and testing association....

Reading 440000-th SNP and testing association....

Reading 450000-th SNP and testing association....

Reading 460000-th SNP and testing association....

Reading 470000-th SNP and testing association....

Reading 480000-th SNP and testing association....

Reading 490000-th SNP and testing association....

Reading 500000-th SNP and testing association....

Reading 510000-th SNP and testing association....

Reading 520000-th SNP and testing association....

Reading 530000-th SNP and testing association....

Reading 540000-th SNP and testing association....

Reading 550000-th SNP and testing association....

Reading 560000-th SNP and testing association....

Reading 570000-th SNP and testing association....

Reading 580000-th SNP and testing association....

Reading 590000-th SNP and testing association....

Reading 600000-th SNP and testing association....

Reading 610000-th SNP and testing association....

Reading 620000-th SNP and testing association....

Reading 630000-th SNP and testing association....

Reading 640000-th SNP and testing association....

Reading 650000-th SNP and testing association....

Reading 660000-th SNP and testing association....

Reading 670000-th SNP and testing association....

Reading 680000-th SNP and testing association....

Reading 690000-th SNP and testing association....

Reading 700000-th SNP and testing association....

Reading 710000-th SNP and testing association....

Reading 720000-th SNP and testing association....

Reading 730000-th SNP and testing association....

Reading 740000-th SNP and testing association....

Reading 750000-th SNP and testing association....

Reading 760000-th SNP and testing association....

Reading 770000-th SNP and testing association....

Reading 780000-th SNP and testing association....

Reading 790000-th SNP and testing association....

Reading 800000-th SNP and testing association....

Reading 810000-th SNP and testing association....

Reading 820000-th SNP and testing association....

Reading 830000-th SNP and testing association....

Reading 840000-th SNP and testing association....

Reading 850000-th SNP and testing association....

Reading 860000-th SNP and testing association....

Reading 870000-th SNP and testing association....

Reading 880000-th SNP and testing association....

Reading 890000-th SNP and testing association....

Reading 900000-th SNP and testing association....

Reading 910000-th SNP and testing association....

Reading 920000-th SNP and testing association....

Reading 930000-th SNP and testing association....

GLS association - elapsed CPU time is 23.200000

Breakdown for input file parsing : 9.630000

              matrix computation : 7.250000

             p-value computation : 1.240000
```

The results are included in HKPassocImputedonlykinship.ps and MLWassocImputedonlykinship.ps

Each line consists on:
1. SNP ID
2. Beta (1 is effect allele)
3. SE(beta)
4. p-value.

35941 SNPs for HKP phenotype and 33741 SNPs for MLW phenotype were statistically significant (P<0.05)

After multiple testing correction, 0 significant SNPs (P<0.05) were obtained for all the methods and for both phenotypes: FDR, Bonferroni and qvalue

HKP

```
Call:
qvalue(p = pvalueHKP)

pi0:	1	

Cumulative number of significant calls:

          <1e-04 <0.001 <0.01 <0.025 <0.05  <0.1     <1
p-value      279   1002 11407  23796 35941 63929 840046
q-value        0      0     0      0     0     0  24049
local FDR      0      0     0      0     0     0    673

```

MLW
```
Call:
qvalue(p = pvalueMLW)

pi0:	1	

Cumulative number of significant calls:

          <1e-04 <0.001 <0.01 <0.025 <0.05  <0.1     <1
p-value       37    381  4545  13426 33741 72366 840046
q-value        0      0     0      0     0     0 276299
local FDR      0      0     0      0     0     0      4
```

HKP lambda modified (0.1)

```
Call:
qvalue(p = pvalueHKP, pfdr = TRUE, lambda = 0.1)

pi0:	1	

Cumulative number of significant calls:

          <1e-04 <0.001 <0.01 <0.025 <0.05  <0.1     <1
p-value      279   1002 11407  23796 35941 63929 840046
q-value        0      0     0      0     0     0  24049
local FDR      0      0     0      0     0     0    673
```
MLW lamda 0.1

```
Call:
qvalue(p = pvalueMLW, pfdr = TRUE, lambda = 0.1)

pi0:	1	

Cumulative number of significant calls:

          <1e-04 <0.001 <0.01 <0.025 <0.05  <0.1     <1
p-value       37    381  4545  13426 33741 72366 840046
q-value        0      0     0      0     0     0 276299
local FDR      0      0     0      0     0     0      4
```


**Multiple testing correction: FDR, Bonferroni, qvalue**

```
#Perform FDR correction in the p-values resulting from the GWAS performed in EMMAX:

HKP<-read.table("HKPassocImputedonlykinship.ps")
head(HKP)
HKPpvalue<-HKP[,4]
length(HKPpvalue)#930660

MLW<-read.table("MLWassocImputedonlykinship.ps")
head(MLW)
MLWpvalue<-MLW[,4]
length(MLWpvalue)#930660

padjustHKP<-p.adjust(HKP$V4, method = "BH", n = 930660)
head(padjustHKP)
padjustHKP<-as.data.frame(padjustHKP)
dim(padjustHKP)

padjustHKPbonf<-p.adjust(HKP$V4, method = "bonferroni", n = 930660)
head(padjustHKPbonf)
padjustHKPbonf<-as.data.frame(padjustHKPbonf)
dim(padjustHKPbonf)

padjustMLW<-p.adjust(MLW$V4, method = "BH", n = 930660)
padjustMLWBonf<-p.adjust(MLW$V4, method = "bonferroni", n = 930660)
head(padjustMLW)
padjustMLW<-as.data.frame(padjustMLW)
padjustMLWBonf<-as.data.frame(padjustMLWBonf)
dim(padjustMLW)
dim(padjustMLWBonf)

HKPpadjust<-cbind(HKP,padjustHKP,padjustHKPbonf)
head(HKPpadjust)
MLWpadjust<-cbind(MLW,padjustMLW,padjustMLWBonf)
head(HKPpadjust)

HKPpadjust0.05<-HKPpadjust[HKPpadjust$padjustHKP< 0.05,]
MLWpadjust0.05<-MLWpadjust[MLWpadjust$padjustMLW< 0.05,]
HKPpadjustbonf0.05<-HKPpadjust[HKPpadjust$padjustHKPbonf< 0.05,]
MLWpadjustbonf0.05<-MLWpadjust[MLWpadjust$padjustMLWBonf< 0.05,]

dim(HKPpadjust0.05) #0
dim(MLWpadjust0.05) #0
dim(HKPpadjustbonf0.05) #0
dim(MLWpadjustbonf0.05) #0
head(HKP)
HKP0.05<-HKP[HKP$V4< 0.05,]
MLW0.05<-MLW[MLW$V4< 0.05,]

HKPq<-HKP[HKP$V4< 5.37e-08,]
MLWq<-MLW[MLW$V4< 5.37e-08,]

dim(HKP0.05)#35941
dim(MLW0.05)#33741
dim(HKPq)#0
dim(MLWq)#0

# I try using other multiple testing correction: q-value (https://www.researchgate.net/publication/324953882_Effects_of_Q-Value_Correction_on_Results_from_Genome-Wide_Association_Studies_Using_Varied_Input_Data_Quantities)

#install.packages("devtools")
library(devtools)
install_github("jdstorey/qvalue")

library(qvalue)
pvalueHKP<-HKP$V4
pvalueMLW<-MLW$V4
length(pvalueHKP)
length(pvalueMLW)
head(pvalueHKP)
qvalueHKP <- qvalue(p = pvalueHKP)
qvalueMLW <- qvalue(p = pvalueMLW)
qvalueHKPlambda0.1<-qvalue(p = pvalueHKP, lambda = 0.1, pfdr=TRUE)
qvalueMLWlambda0.1<-qvalue(p = pvalueMLW, lambda = 0.1, pfdr=TRUE)
head(qvalueHKP)

summary(qvalueHKP)
summary(qvalueMLW)
summary(qvalueHKPlambda0.1)
summary(qvalueMLWlambda0.1)
```
