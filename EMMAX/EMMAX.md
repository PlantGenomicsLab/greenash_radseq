Working directory: /labs/Wegrzyn/IreneCobo/Fraxinus/v1.4

EMMAX program used is a linear mixed model (LMM) to perform GWAS, correcting by a wide range of sample structures (which encompasses population stratification and hidden relatedness). To this end, it uses an internally calculated kinship matrix as a random effect, and population structure externally calculated as a fixed effect (Kang et al. 2010)[https://www.nature.com/articles/ng.548#Sec9]. 

Tutorial: https://genome.sph.umich.edu/wiki/EMMAX#IBS_matrix_or_BN_matrix.3F 
Why EMMAX is suitable for performing family-based GWAS on quantitative traits (see the conclusion): https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4102448/ . Another advantage of EMMAX is that it allows including imputed SNPs in the analysis

Other interesting information: https://stats.stackexchange.com/questions/223797/beta-values-for-mixed-models (fixed and random effect definition, roughly)

1. Perform linkage disequilibrium filtering using plink to retain only independent SNPs for DAPC analysis: 
```
plink --vcf output.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --indep-pairwise 50 5 0.2 --recode vcf --out outputLD0.2.vcf
```
2. Perform DAPC analysis in R (in addition, a PCA analysis was also performed)
```
library(adegenet)
library(vcfR)
#Usando solo un subset del panel2 para probar

#The only input file needed is a vcf or vcf.gz file
vcf <- read.vcfR("outputLD0.2.vcf")
x <- vcfR2genlight(vcf) #transform vcf to genlight
x
grp <- find.clusters(x, max.n.clust=40)#97 number of PCs, 7 number of clusters
dapc1 <- dapc(x, grp$grp) #70 PCs retained, 5 discrmininant functions. DAPC is implemented by the function "dapc", which first transforms the data using PCA, and then performs a Discriminant Analysis on the retained principal components.Two graphs will appear as a result to help decide the number of principal components and the number of discriminant functions to be retained  
dapc1
dapc1$assign
scatter(dapc1)#plot the groups in a scatterplot and the DA eigenvalues
#The scatterplot can be customized (dot size)
scatter(dapc1, cell = 0, pch = 18:23, cstar = 0, mstree = TRUE, lwd = 2, lty = 2)
#position of the DA eigenvalues plot
scatter(dapc1, posi.da="bottomleft", bg="white", pch=17:22)

#Change of colors, position of the PCA eigenvalues plot 
myCol <- c("darkblue","purple","green","orange","red","blue")
scatter(dapc1, posi.da="bottomleft", bg="white", legend = TRUE,
        pch=17:22, cstar=0, col=myCol, scree.pca=TRUE,
        posi.pca="bottomright")

scatter(dapc1, posi.pca="bottomright", posi.da="bottomleft",scree.da=FALSE, bg="white", pch=20, cell=0, cstar=0, col=myCol, solid=.4,
        cex=3,clab=0, leg=TRUE, txt.leg=paste("Cluster",1:6))

scatter(dapc1, ratio.pca=0.3, bg="white", pch=20, cell=0,
        cstar=0, col=myCol, solid=.4, cex=3, clab=0,
        mstree=TRUE, scree.da=FALSE, posi.pca="bottomright", posi.da="bottomleft",
        leg=TRUE, txt.leg=paste("Cluster",1:6))
par(xpd=TRUE)
points(dapc1$grp.coord[,1], dapc1$grp.coord[,2], pch=4,
       cex=3, lwd=8, col="black")
points(dapc1$grp.coord[,1], dapc1$grp.coord[,2], pch=4,
       cex=3, lwd=2, col=myCol)
myInset <- function(){
  temp <- dapc1$pca.eig
  temp <- 100* cumsum(temp)/sum(temp)
  plot(temp, col=rep(c("black","lightgrey"),
                     c(dapc1$n.pca,1000)), ylim=c(0,100),
       xlab="PCA axis", ylab="Cumulated variance (%)",
       cex=1, pch=20, type="h", lwd=2)
}
add.scatter(myInset(), posi="bottomright",
            inset=c(-0.03,-0.01), ratio=.28,
            bg=transp("white"))
#Membership probabilities
#Exploration
class(dapc1$posterior)
dim(dapc1$posterior)
round(head(dapc1$posterior),3)
str(dapc1$posterior)
head(dapc1$posterior)

#Change formatting for storage and used to correct by population structure in the GWAS
round<-round(dapc1$posterior,3)
head(round)
tail(round)
str(round)
assignedPop<-apply(round, 1, which.max)
head(assignedPop)
assignedPopframe<-as.data.frame(assignedPop)
head(assignedPopframe)
str(assignedPopframe)
write.csv(assignedPopframe, "assignedPopFraxinusDAPCbueno.csv")

save(x, vcf, grp, dapc1, file = "DAPC_Fraxinus.RData") 
```

![DAPCplot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/DAPCplot.png/ "DAPCplot")
![PCAplot](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/PCAplot.png/ "PCAplot")
![DAPCplotfamilies](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/DAPCplotfamilies.png/ "DAPCplotfamilies")


3. Transform the .vcf file to plink format (.ped) using vcftools and the following code:

```
vcftools --vcf output.vcf --plink --out genotype
```
4. Use PLINK software to transpose your genotype files (bed or ped format) to tped/tfam format by running 

```
plink --file genotype --recode12 --output-missing-genotype 0 --transpose --out genotype
```

5. Perform BN matrix in EMMAX (follow this tutorial: http://genetics.cs.ucla.edu/emmax/install.html)
```
emmax-kin -v -h -d 10 genotype
```


6. Run EMMAX

First, using HKP phenotype. The file containing the phenotypes and the assigned populations (DAPC) has to be the same order of individuals that the .tfam file (see tutorial: http://genetics.cs.ucla.edu/emmax/install.html)
```
./emmax-intel64 -v -d 10 -t genotype -p phenoEMMAXorderHKP.txt -c assignedPoporder.txt -k output.aBN.kinf -o HKPassoc
```
I obtained this error:

```
Reading TFAM file genotype.tfam ....


Reading kinship file output.aBN.kinf...

  98 rows and 98 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderHKP.txt...

  98 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPoporder.txt...

  98 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = nan, evals[1] = nan, evals[n-1] = nan
FATAL ERROR : Minimum q eigenvalues of SKS is supposed to be close to zero, but actually 179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368.000000
FATAL ERROR : Minimum q eigenvalues of SKS is supposed to be close to zero, but actually 179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368.000000
FATAL ERROR : k = 90 > n-q = 89
Aborted
```
Maybe I have to perform the kinship matrix using the LD=0.2 

It did it and it worked

1. Use vcftools to transform the vcf file into ped format

```
vcftools --vcf outputLD0.2.vcf --plink --out genotypeLD0.2
```

2. Transpose the LD0.2 filtered genotypes into tped

```
plink --file genotypeLD0.2 --recode12 --output-missing-genotype 0 --transpose --out genotypeLD0.2
```

3. Perform kinship matrix based on the LD0.2 filtered genotypes

```
./emmax-kin-intel64 -v -d 10 genotypeLD0.2
```
I plotted the kinship matrix using a heatmap. To make it clearer, I ordered the individuals by family (following the order of the file: IDorderfamHeatmap.csv). The name of the kinship matrix with the individuals ordered by family, used to perform this heatmap, is: genotypeLD0.2orderfam.aBN.kinf

![HeatmapKinshipmatrix](https://gitlab.com/PlantGenomicsLab/greenash_radseq/-/blob/master/EMMAX/HeatmapKinshipmatrix.png/ "HeatmapKinshipmatrix")


4. Run EMMAX

```
./emmax-intel64 -v -d 10 -t genotype -p phenoEMMAXorderHKP.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o HKPassoc
```
```
Reading TFAM file genotype.tfam ....


Reading kinship file genotypeLD0.2.aBN.kinf...

  98 rows and 98 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderHKP.txt...

  98 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPoporder.txt...

  98 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 0.000000, evals[n-1] = 9.837129
k = 89,  n-q = 89
eRvals[0] = 0.001739, eRvals[n-q-1] = 8.837129, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.070000
etas[0] = -0.315652, lambdas[0] = 0.001739
etas[1] = -0.054177, lambdas[1] = 0.072227
etas[2] = -0.426081, lambdas[2] = 0.078082
etas[3] = -0.057421, lambdas[3] = 0.093046
etas[4] = -0.021050, lambdas[4] = 0.100162
etas[5] = -0.075209, lambdas[5] = 0.102971
etas[6] = -0.124940, lambdas[6] = 0.113387
etas[7] = 0.061841, lambdas[7] = 0.113773
etas[8] = 0.103734, lambdas[8] = 0.115704
etas[9] = 0.173737, lambdas[9] = 0.117599
etas[10] = 0.326621, lambdas[10] = 0.121784
etas[11] = -0.094288, lambdas[11] = 0.122932
etas[12] = -0.033726, lambdas[12] = 0.130842
etas[13] = 0.369738, lambdas[13] = 0.131704
etas[14] = 0.168202, lambdas[14] = 0.137352
etas[15] = -0.214602, lambdas[15] = 0.138849
etas[16] = 0.128880, lambdas[16] = 0.139634
etas[17] = 0.258590, lambdas[17] = 0.142860
etas[18] = 0.090786, lambdas[18] = 0.145998
etas[19] = 0.148459, lambdas[19] = 0.150944
etas[20] = -0.262301, lambdas[20] = 0.153487
etas[21] = -0.157324, lambdas[21] = 0.157099
etas[22] = -0.199199, lambdas[22] = 0.158876
etas[23] = -0.099513, lambdas[23] = 0.161960
etas[24] = -0.003882, lambdas[24] = 0.165483
etas[25] = -0.040198, lambdas[25] = 0.168917
etas[26] = 0.201051, lambdas[26] = 0.171542
etas[27] = 0.071358, lambdas[27] = 0.173888
etas[28] = 0.064352, lambdas[28] = 0.174839
etas[29] = -0.215310, lambdas[29] = 0.179624
etas[30] = -0.091099, lambdas[30] = 0.184958
etas[31] = 0.304442, lambdas[31] = 0.188216
etas[32] = -0.064911, lambdas[32] = 0.191419
etas[33] = 0.190234, lambdas[33] = 0.192816
etas[34] = 0.103330, lambdas[34] = 0.195691
etas[35] = 0.136705, lambdas[35] = 0.197462
etas[36] = -0.011828, lambdas[36] = 0.200206
etas[37] = 0.346754, lambdas[37] = 0.206067
etas[38] = 0.176033, lambdas[38] = 0.213453
etas[39] = 0.482828, lambdas[39] = 0.218856
etas[40] = 0.016329, lambdas[40] = 0.219378
etas[41] = -0.073551, lambdas[41] = 0.223947
etas[42] = -0.152376, lambdas[42] = 0.224511
etas[43] = 0.029397, lambdas[43] = 0.229249
etas[44] = 0.146075, lambdas[44] = 0.235044
etas[45] = 0.093453, lambdas[45] = 0.236764
etas[46] = -0.123453, lambdas[46] = 0.243689
etas[47] = 0.264891, lambdas[47] = 0.246064
etas[48] = -0.247395, lambdas[48] = 0.253058
etas[49] = -0.336422, lambdas[49] = 0.264030
etas[50] = -0.057490, lambdas[50] = 0.264675
etas[51] = -0.177204, lambdas[51] = 0.267538
etas[52] = -0.316064, lambdas[52] = 0.273297
etas[53] = -0.082431, lambdas[53] = 0.274399
etas[54] = -0.108032, lambdas[54] = 0.282197
etas[55] = 0.214938, lambdas[55] = 0.285912
etas[56] = -0.039426, lambdas[56] = 0.296841
etas[57] = 0.186430, lambdas[57] = 0.302922
etas[58] = -0.343943, lambdas[58] = 0.306037
etas[59] = -0.192374, lambdas[59] = 0.311813
etas[60] = -0.163589, lambdas[60] = 0.319381
etas[61] = 0.010740, lambdas[61] = 0.330782
etas[62] = -0.556479, lambdas[62] = 0.333075
etas[63] = 0.089044, lambdas[63] = 0.337674
etas[64] = 0.072235, lambdas[64] = 0.338544
etas[65] = -0.002799, lambdas[65] = 0.347533
etas[66] = -0.141187, lambdas[66] = 0.354333
etas[67] = 0.031817, lambdas[67] = 0.361156
etas[68] = 0.058357, lambdas[68] = 0.372229
etas[69] = 0.321826, lambdas[69] = 0.384487
etas[70] = -0.151091, lambdas[70] = 0.407359
etas[71] = -0.167886, lambdas[71] = 0.415695
etas[72] = 0.092070, lambdas[72] = 0.433998
etas[73] = -0.076516, lambdas[73] = 0.443825
etas[74] = -0.363125, lambdas[74] = 0.447950
etas[75] = -0.051222, lambdas[75] = 0.461880
etas[76] = 0.169765, lambdas[76] = 0.469867
etas[77] = 0.290933, lambdas[77] = 0.485169
etas[78] = -0.165044, lambdas[78] = 0.500579
etas[79] = -0.188311, lambdas[79] = 0.529156
etas[80] = 0.100641, lambdas[80] = 0.536558
etas[81] = 0.027162, lambdas[81] = 0.586429
etas[82] = -0.108644, lambdas[82] = 0.818823
etas[83] = -0.207429, lambdas[83] = 1.014345
etas[84] = 0.202227, lambdas[84] = 1.034352
etas[85] = -0.015075, lambdas[85] = 1.754933
etas[86] = 0.009594, lambdas[86] = 3.086517
etas[87] = -0.020278, lambdas[87] = 4.635223
etas[88] = 0.138350, lambdas[88] = 8.837129
0	19121.837465	18990.796117	-55.230791
1	18990.796117	18832.803034	-55.039244
2	18832.803034	18642.841181	-54.807064
3	18642.841181	18415.201289	-54.526088
4	18415.201289	18143.498324	-54.186730
5	18143.498324	17820.749763	-53.777827
6	17820.749763	17439.541690	-53.286519
7	17439.541690	16992.311655	-52.698191
8	16992.311655	16471.776030	-51.996511
9	16471.776030	15871.521054	-51.163598
10	15871.521054	15186.757442	-50.180406
11	15186.757442	14415.205807	-49.027334
12	14415.205807	13558.034927	-47.685151
13	13558.034927	12620.723508	-46.136224
14	12620.723508	11613.672419	-44.366056
15	11613.672419	10552.378508	-42.365037
16	10552.378508	9457.012824	-40.130264
17	9457.012824	8351.334577	-37.667211
18	8351.334577	7261.005710	-34.990978
19	7261.005710	6211.514652	-32.126847
20	6211.514652	5226.024420	-29.109942
21	5226.024420	4323.490873	-25.983877
22	4323.490873	3517.339941	-22.798460
23	3517.339941	2814.868836	-19.606640
24	2814.868836	2217.387024	-16.461064
25	2217.387024	1720.980668	-13.410630
26	1720.980668	1317.697818	-10.497472
27	1317.697818	996.920471	-7.754714
28	996.920471	746.708064	-5.205209
29	746.708064	554.950025	-2.861319
30	554.950025	410.234332	-0.725651
31	410.234332	302.406667	1.207479
32	302.406667	222.847582	2.950117
33	222.847582	164.526612	4.518562
34	164.526612	121.903024	5.931332
35	121.903024	90.738196	7.207390
36	90.738196	67.871411	8.364743
37	67.871411	50.995194	9.419488
38	50.995194	38.452064	10.385288
39	38.452064	29.063457	11.273243
40	29.063457	21.993802	12.092078
41	21.993802	16.647757	12.848531
42	16.647757	12.595969	13.547849
43	12.595969	9.523527	14.194272
44	9.523527	7.195348	14.791451
45	7.195348	5.433374	15.342730
46	5.433374	4.101536	15.851310
47	4.101536	3.095576	16.320296
48	3.095576	2.335787	16.752663
49	2.335787	1.761555	17.151194
50	1.761555	1.327072	17.518404
51	1.327072	0.997917	17.856493
52	0.997917	0.748320	18.167325
53	0.748320	0.559013	18.452445
54	0.559013	0.415562	18.713120
55	0.415562	0.307109	18.950415
56	0.307109	0.225430	19.165266
57	0.225430	0.164244	19.358569
58	0.164244	0.118719	19.531252
59	0.118719	0.085116	19.684329
60	0.085116	0.060529	19.818937
61	0.060529	0.042707	19.936342
62	0.042707	0.029909	20.037923
63	0.029909	0.020804	20.125143
64	0.020804	0.014383	20.199499
65	0.014383	0.009890	20.262478
66	0.009890	0.006770	20.315517
67	0.006770	0.004616	20.359962
68	0.004616	0.003137	20.397048
69	0.003137	0.002126	20.427883
70	0.002126	0.001438	20.453445
71	0.001438	0.000971	20.474586
72	0.000971	0.000655	20.492035
73	0.000655	0.000441	20.506415
74	0.000441	0.000297	20.518250
75	0.000297	0.000199	20.527981
76	0.000199	0.000134	20.535975
77	0.000134	0.000090	20.542538
78	0.000090	0.000060	20.547924
79	0.000060	0.000041	20.552342
80	0.000041	0.000027	20.555963
81	0.000027	0.000018	20.558932
82	0.000018	0.000012	20.561366
83	0.000012	0.000008	20.563359
84	0.000008	0.000006	20.564993
85	0.000006	0.000004	20.566331
86	0.000004	0.000002	20.567427
87	0.000002	0.000002	20.568324
88	0.000002	0.000001	20.569059
89	0.000001	0.000001	20.569661
90	0.000001	0.000001	20.570154
91	0.000001	0.000000	20.570558
92	0.000000	0.000000	20.570888
93	0.000000	0.000000	20.571159
94	0.000000	0.000000	20.571381
95	0.000000	0.000000	20.571562
96	0.000000	0.000000	20.571711
97	0.000000	0.000000	20.571832
98	0.000000	0.000000	20.571932
99	0.000000	0.000000	20.572013
REMLE (delta=22026.465795, REML=20.572080, REML0=20.572382)- elapsed CPU time is 0.000000
eigen_L - elapsed CPU time is 0.000000


Reading TPED file genotype...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

GLS association - elapsed CPU time is 0.900000

Breakdown for input file parsing : 0.320000

              matrix computation : 0.290000

             p-value computation : 0.030000
```

Results are in: /labs/Wegrzyn/IreneCobo/Fraxinus/v1.4/HKPassoc.ps 

Each line consist of
1. SNP ID
2. Beta (1 is effect allele)
3. SE(beta)
4. p-value.

5. Run EMMAX again with the other phenotype MLW

```
./emmax-intel64 -v -d 10 -t genotype -p phenoEMMAXorderMLW.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o MLWassoc
```
```

Reading TFAM file genotype.tfam ....


Reading kinship file genotypeLD0.2.aBN.kinf...

  98 rows and 98 columns were observed with 0 missing values.


Reading the phenotype file phenoEMMAXorderMLW.txt...

  98 rows and 1 columns were observed with 7 missing values

nmiss = 7 , mphenoflag = 1

Reading covariate file assignedPoporder.txt...

  98 rows and 2 columns were observed with 0 missing values. Make sure that the intercept is included in the covariates

File reading - elapsed CPU time is 0.000000

evals[0] = 0.000000, evals[1] = 0.000000, evals[n-1] = 9.837129
k = 89,  n-q = 89
eRvals[0] = 0.001739, eRvals[n-q-1] = 8.837129, eRvals[n-q] = 0.000000
eigen_R - elapsed CPU time is 0.000000
etas[0] = -0.006225, lambdas[0] = 0.001739
etas[1] = 0.005027, lambdas[1] = 0.072227
etas[2] = -0.005289, lambdas[2] = 0.078082
etas[3] = 0.001020, lambdas[3] = 0.093046
etas[4] = 0.017039, lambdas[4] = 0.100162
etas[5] = 0.020203, lambdas[5] = 0.102971
etas[6] = 0.013320, lambdas[6] = 0.113387
etas[7] = -0.013015, lambdas[7] = 0.113773
etas[8] = -0.019691, lambdas[8] = 0.115704
etas[9] = -0.020489, lambdas[9] = 0.117599
etas[10] = 0.017634, lambdas[10] = 0.121784
etas[11] = 0.040536, lambdas[11] = 0.122932
etas[12] = 0.003273, lambdas[12] = 0.130842
etas[13] = -0.009004, lambdas[13] = 0.131704
etas[14] = -0.006958, lambdas[14] = 0.137352
etas[15] = 0.024633, lambdas[15] = 0.138849
etas[16] = -0.004962, lambdas[16] = 0.139634
etas[17] = -0.034625, lambdas[17] = 0.142860
etas[18] = -0.008261, lambdas[18] = 0.145998
etas[19] = -0.028569, lambdas[19] = 0.150944
etas[20] = 0.022889, lambdas[20] = 0.153487
etas[21] = 0.021482, lambdas[21] = 0.157099
etas[22] = -0.013524, lambdas[22] = 0.158876
etas[23] = -0.004669, lambdas[23] = 0.161960
etas[24] = 0.015199, lambdas[24] = 0.165483
etas[25] = 0.016806, lambdas[25] = 0.168917
etas[26] = -0.018143, lambdas[26] = 0.171542
etas[27] = -0.007811, lambdas[27] = 0.173888
etas[28] = -0.026114, lambdas[28] = 0.174839
etas[29] = -0.000092, lambdas[29] = 0.179624
etas[30] = -0.040563, lambdas[30] = 0.184958
etas[31] = 0.007859, lambdas[31] = 0.188216
etas[32] = 0.021577, lambdas[32] = 0.191419
etas[33] = -0.001012, lambdas[33] = 0.192816
etas[34] = -0.004919, lambdas[34] = 0.195691
etas[35] = -0.038724, lambdas[35] = 0.197462
etas[36] = -0.010967, lambdas[36] = 0.200206
etas[37] = 0.008656, lambdas[37] = 0.206067
etas[38] = -0.042956, lambdas[38] = 0.213453
etas[39] = -0.026085, lambdas[39] = 0.218856
etas[40] = -0.005610, lambdas[40] = 0.219378
etas[41] = -0.011832, lambdas[41] = 0.223947
etas[42] = -0.001221, lambdas[42] = 0.224511
etas[43] = -0.019110, lambdas[43] = 0.229249
etas[44] = -0.033467, lambdas[44] = 0.235044
etas[45] = 0.020562, lambdas[45] = 0.236764
etas[46] = 0.030245, lambdas[46] = 0.243689
etas[47] = -0.017044, lambdas[47] = 0.246064
etas[48] = 0.029263, lambdas[48] = 0.253058
etas[49] = 0.011870, lambdas[49] = 0.264030
etas[50] = -0.012271, lambdas[50] = 0.264675
etas[51] = 0.021125, lambdas[51] = 0.267538
etas[52] = 0.012991, lambdas[52] = 0.273297
etas[53] = 0.032727, lambdas[53] = 0.274399
etas[54] = 0.014680, lambdas[54] = 0.282197
etas[55] = -0.009822, lambdas[55] = 0.285912
etas[56] = 0.011537, lambdas[56] = 0.296841
etas[57] = -0.001339, lambdas[57] = 0.302922
etas[58] = 0.009343, lambdas[58] = 0.306037
etas[59] = -0.030221, lambdas[59] = 0.311813
etas[60] = 0.021573, lambdas[60] = 0.319381
etas[61] = 0.013156, lambdas[61] = 0.330782
etas[62] = 0.030816, lambdas[62] = 0.333075
etas[63] = -0.010961, lambdas[63] = 0.337674
etas[64] = -0.010617, lambdas[64] = 0.338544
etas[65] = 0.038583, lambdas[65] = 0.347533
etas[66] = -0.004539, lambdas[66] = 0.354333
etas[67] = -0.019438, lambdas[67] = 0.361156
etas[68] = -0.044128, lambdas[68] = 0.372229
etas[69] = -0.002282, lambdas[69] = 0.384487
etas[70] = 0.011420, lambdas[70] = 0.407359
etas[71] = 0.019151, lambdas[71] = 0.415695
etas[72] = 0.015149, lambdas[72] = 0.433998
etas[73] = -0.006606, lambdas[73] = 0.443825
etas[74] = 0.026544, lambdas[74] = 0.447950
etas[75] = 0.000436, lambdas[75] = 0.461880
etas[76] = -0.007269, lambdas[76] = 0.469867
etas[77] = -0.025637, lambdas[77] = 0.485169
etas[78] = -0.072375, lambdas[78] = 0.500579
etas[79] = 0.037501, lambdas[79] = 0.529156
etas[80] = 0.003030, lambdas[80] = 0.536558
etas[81] = 0.025787, lambdas[81] = 0.586429
etas[82] = 0.032409, lambdas[82] = 0.818823
etas[83] = 0.037770, lambdas[83] = 1.014345
etas[84] = -0.009187, lambdas[84] = 1.034352
etas[85] = -0.000041, lambdas[85] = 1.754933
etas[86] = 0.026560, lambdas[86] = 3.086517
etas[87] = 0.005559, lambdas[87] = 4.635223
etas[88] = 0.034380, lambdas[88] = 8.837129
0	2547.401602	2519.438521	208.119966
1	2519.438521	2485.891042	208.145431
2	2485.891042	2445.797628	208.176155
3	2445.797628	2398.101239	208.213130
4	2398.101239	2341.673005	208.257487
5	2341.673005	2275.354419	208.310499
6	2275.354419	2198.023391	208.373569
7	2198.023391	2108.688691	208.448202
8	2108.688691	2006.614756	208.535958
9	2006.614756	1891.473725	208.638370
10	1891.473725	1763.513444	208.756839
11	1763.513444	1623.719618	208.892490
12	1623.719618	1473.939105	209.046001
13	1473.939105	1316.923825	209.217418
14	1316.923825	1156.256195	209.405975
15	1156.256195	996.132486	209.609958
16	996.132486	841.010799	209.826650
17	841.010799	695.169309	210.052377
18	695.169309	562.254407	210.282703
19	562.254407	444.911837	210.512732
20	444.911837	344.577463	210.737506
21	344.577463	261.462024	210.952434
22	261.462024	194.712552	211.153676
23	194.712552	142.692220	211.338437
24	142.692220	103.303713	211.505127
25	103.303713	74.289448	211.653384
26	74.289448	53.465857	211.783981
27	53.465857	38.876153	211.898654
28	38.876153	28.867255	211.999877
29	28.867255	22.108244	212.090623
30	22.108244	17.570846	212.174130
31	17.570846	14.489955	212.253670
32	14.489955	12.317364	212.332341
33	12.317364	10.676771	212.412860
34	10.676771	9.324058	212.497370
35	9.324058	8.113970	212.587269
36	8.113970	6.972793	212.683070
37	6.972793	5.875974	212.784319
38	5.875974	4.829715	212.889604
39	4.829715	3.855934	212.996667
40	3.855934	2.980470	213.102633
41	2.980470	2.224637	213.204342
42	2.224637	1.600251	213.298740
43	1.600251	1.107929	213.383264
44	1.107929	0.738095	213.456160
45	0.738095	0.473783	213.516662
46	0.473783	0.294194	213.565009
47	0.294194	0.178078	213.602300
48	0.178078	0.106311	213.630219
49	0.106311	0.063388	213.650702
50	0.063388	0.037892	213.665615
51	0.037892	0.022186	213.676494
52	0.022186	0.011681	213.684389
53	0.011681	0.003952	213.689824
54	0.003952	-0.002074	213.692856
55	-0.002074	-0.006751	213.693210
56	-0.006751	-0.010144	213.690443
57	-0.010144	-0.012274	213.684113
58	-0.012274	-0.013229	213.673925
59	-0.013229	-0.013184	213.659823
60	-0.013184	-0.012376	213.642037
61	-0.012376	-0.011066	213.621065
62	-0.011066	-0.009496	213.597625
63	-0.009496	-0.007865	213.572566
64	-0.007865	-0.006317	213.546776
65	-0.006317	-0.004939	213.521094
66	-0.004939	-0.003772	213.496244
67	-0.003772	-0.002823	213.472793
68	-0.002823	-0.002076	213.451140
69	-0.002076	-0.001504	213.431520
70	-0.001504	-0.001076	213.414028
71	-0.001076	-0.000761	213.398649
72	-0.000761	-0.000534	213.385286
73	-0.000534	-0.000371	213.373790
74	-0.000371	-0.000257	213.363984
75	-0.000257	-0.000177	213.355678
76	-0.000177	-0.000121	213.348683
77	-0.000121	-0.000082	213.342822
78	-0.000082	-0.000056	213.337931
79	-0.000056	-0.000038	213.333863
80	-0.000038	-0.000026	213.330489
81	-0.000026	-0.000017	213.327698
82	-0.000017	-0.000012	213.325392
83	-0.000012	-0.000008	213.323491
84	-0.000008	-0.000005	213.321925
85	-0.000005	-0.000004	213.320636
86	-0.000004	-0.000002	213.319578
87	-0.000002	-0.000002	213.318708
88	-0.000002	-0.000001	213.317994
89	-0.000001	-0.000001	213.317408
90	-0.000001	-0.000000	213.316927
91	-0.000000	-0.000000	213.316534
92	-0.000000	-0.000000	213.316211
93	-0.000000	-0.000000	213.315946
94	-0.000000	-0.000000	213.315729
95	-0.000000	-0.000000	213.315551
96	-0.000000	-0.000000	213.315406
97	-0.000000	-0.000000	213.315287
98	-0.000000	-0.000000	213.315189
99	-0.000000	-0.000000	213.315109
REMLE (delta=2.523593, REML=213.693418, REML0=213.314748)- elapsed CPU time is 0.010000
eigen_L - elapsed CPU time is 0.000000


Reading TPED file genotype...

Reading 0-th SNP and testing association....

Reading 10000-th SNP and testing association....

Reading 20000-th SNP and testing association....

GLS association - elapsed CPU time is 0.820000

Breakdown for input file parsing : 0.360000

              matrix computation : 0.270000

             p-value computation : 0.020000
```

Results are in: /labs/Wegrzyn/IreneCobo/Fraxinus/v1.4/

MLWassoc.ps
HKPassoc.ps

Each line consist of:
1. SNP ID
2. Beta (1 is effect allele)
3. SE(beta)
4. p-value.

Now it is necessary to correct by multiple testing. Since I used a SNP panel not corrected by LD to perform the GWAS in EMMAX, then, Bonferroni correction would be too retrictive/conservative. It is because the Bonferroni correction assumes independence among the association tests. However, there is a substantial degree of correlation between the association statistics due to a phenomenon called linkage disequilibrium, which renders the Bonferroni correction too conservative (https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0903-6#Sec12.)  Thus, FDR correction would be more appropriate: http://lybird300.github.io/2015/10/19/multiple-test-correction.html

The FDR correction was performed in R. The results of this FDR correction are in HKPpadjust.csv and MLWpadjust.csv

After filtering the significant SNPs (P<0.05) 104 and 112 significant SNPs were obtained for the phenotypes HKP and MLW, respectively. The results are stored in the files HKPpadjust0.05.csv and MLWpadjust0.05.csv. 

However, all significant SNPs in HKP and all with the exception of 8 SNPs in MLW showed a p-value and a padjust equal to 0, as well as the SE(beta). 

I will try to use this method of multiple-testing correction specific for LMM called MultiTrans: https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0903-6#Sec12

R documentation for MultiTrans: http://genetics.cs.ucla.edu/multiTrans/install.html

Not working. Instead, I imputed the lacking SNPs using LinkImpute (reference articles: https://acsess.onlinelibrary.wiley.com/doi/full/10.3835/plantgenome2015.11.0113 and https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4632058/):

I used the simple imputation, using the default parameters

```
java -jar LinkImpute.jar -q output.ped outputimputed

Starting to read in dataset...
	Read in data set of 98 samples and 29417 SNPs.
Finished reading in data set.
Starting calculating correlations...
	[==================================================]
	[==================================================]
Finished calculating correlations.
Masking 10000 genotypes
Starting optimizing parameters...
	....................................................................................
	Best k:		4
	Best l:		21
	Accuracy:	0.9396
Finished optimizing parameters.
Starting imputation...
	[==================================================]
Finished imputation.
Writing output...
Finished writing output.
```

The results showed that the best k was 4 and the best l was 21 for the data set, which gave an accuracy of 0.9396.

I also performed the imputation using a .ped file containing the family and phenotypic data:

```
java -jar LinkImpute.jar -q outputpedsingle2.ped outputimputedpedsingle

Starting to read in dataset...
	Read in data set of 98 samples and 29417 SNPs.
Finished reading in data set.
Starting calculating correlations...
	[==================================================]
	[==================================================]
Finished calculating correlations.
Masking 10000 genotypes
Starting optimizing parameters...
	............................................................................................................................................................................
	Best k:		14
	Best l:		65
	Accuracy:	0.9497
Finished optimizing parameters.
Starting imputation...
	[==================================================]
Finished imputation.
Writing output...
Finished writing output.

```

The optimization of parameters showed that the best k was 14 and the best l 65 for the data set, showing an accuracy of 0.9497

Afterwards, I calculated the identity‐by‐descent (IBD) using PLINK (Purcell et al., 2007; Purcell, 2009). When two or more accessions had IBD Pi > 0.9, one accession from the clonal group was randomly chosen, and its genotype data were retained while the genotype data from the other clones were removed. Since IBD calculation in plink is not LD-aware, I used the LD pruned dataset to perform the IBD calculation.

```
plink --file genotypeLD0.2 --genome --out IBDLD0.2
```
This code gives as a result 2 files:

- IBDLD0.2.genome
- IBDLD0.2.log

I sorted the 10th column of the IBDLD0.2.genome file (PI_HAT) in descendent order: IBDLD0.2sorted.genome

The following pairs of individuals showed a PI_HAT higher that 0.9:

- FP3005-FP3013 (PI_HAT 0.9973) (Siblings, both belong to family C, they are siblings)
- FP3110-Sample_3108 (PI_HAT 0.929) (Siblings, both belong to family K)

Then I decided to perform the GWAS in EMMAX using all the individuals and the matrix with the imputed SNPs and also by removing these two individuals (I removed FP3013 and FP3110) and compare the differences. 

1. Use PLINK software to transpose the imputed genotype files (bed or ped format) to tped/tfam format by running 

```
plink --file outputimputed --recode12 --output-missing-genotype 0 --transpose --out outputimputed
```

2. Run EMMAX using both phenotypes (HKP and MLW):

```
./emmax-intel64 -v -d 10 -t outputimputed -p phenoEMMAXorderHKP.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o HKPassocimputed
./emmax-intel64 -v -d 10 -t outputimputed -p phenoEMMAXorderMLW.txt -k genotypeLD0.2.aBN.kinf -c assignedPoporder.txt -o MLWassocimputed
```
The results files are called HKPassocimputed.ps and MLWassocimputed.ps

3. I run EMMAX also in the same files but without FP3013 and FP3110 individuals:

```
./emmax-intel64 -v -d 10 -t outputimputednoFP3110noFP3013 -p phenoEMMAXorderHKPnoFP3110noFP3013.txt -k outputnoFP3013noFP3110LD0.2.aBN.kinf -c assignedPopordernoFP3110noFP3013.txt -o HKPassocimputednoFP3110noFP3013
```
```
./emmax-intel64 -v -d 10 -t outputimputednoFP3110noFP3013 -p phenoEMMAXorderMLWnoFP3110noFP3013.txt -k outputnoFP3013noFP3110LD0.2.aBN.kinf -c assignedPopordernoFP3110noFP3013.txt -o MLWassocimputednoFP3110noFP3013
```

The results files are called HKPassocimputednoFP3110noFP3013.ps and MLWassocimputednoFP3110noFP3013.ps

Pvalues for both, GWAS results from imputed SNPs including all individuals and without FP3013 and FP3110 were corrected using FDR and Bonferroni correction for multiple testing. Corrected p-values <0.05 were considered as significant. These were the results:

- HKP:
  - All imputed SNPs=  100 significant SNPs using FDR and 100 for Bonferroni
  - Without FP3110 and FP3013 = 0 singnificant SNPs using FDR and 0 using Bonferroni
- MLW
  - All imputed SNPs= 100 significant SNPs using FDR and 100 using Bonferroni (exactly the same SNPs between them and in comparison with the 100 HKP significant SNPs)
  - Without FP3110 and FP3013 = 0 singnificant SNPs using FDR and 0 using Bonferroni

The results (for all imputed SNPs) are in the files: HKPimputedFDR0.05.csv, HKPimputedBonf0.05.csv, MLWimputedFDR0.05.csv, MLWimputedBonf0.05.csv, containing the same 100 significant (P<0.05) SNPs each

I have compared these 100 significant SNPs with the 104 and 112 significant SNPs obtained for HKP and MLW phenotypes before imputation and they are different SNPs. 

The best linear unbiased predictions (BLUPs) is a method for estimating random effects, so so it has gradually become more common application to generate more precise estimates of genotypic values 

So I calculated the BLUPs for both phenotypes and I performed the GWAS in EMMAX using the dataset that offered more significantly associated SNPs (the ones previous to the imputation) 

A standard BLUPS model looks like this (Doubt: How to develop the model for this data, maybe "family" as the only random effect?): 
Y = lmer(X~(1|LINE) + (1|LOC) + (1|YEAR) + (1|LINE:LOC) + (1|LINE: YEAR))

Being LINE=line, LOC,=location, YEAR=year

Following steps:

- LD decay calculation
- Correlation between the two phenotypes
- Manhatan plot with the GWAS results

