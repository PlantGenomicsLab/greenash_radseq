# Variant Call and Filtering Results

## Freebayes results
Input:
* Script: `/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/variant_call/fourth_run_chromosomes/fb_parallel.sh`
* Aligned bam: `/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/results/aligned_ddRAD_and_WGS`
* Reference genome: `/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/genome_v1.4/Fraxinus_pennsylvanica_genome.v1.4.scaffoldsOver10k.fasta`

Output:
* vcf: `/labs/Wegrzyn/EAB_alignments/eab_post_PAG/fraxinus/scripts/variant_call/fourth_run_chromosomes/output.vcf.gz`
* 3,565,012 Sites
* 102 Individuals

## LinkImputeR Accuracy mode
LinkImputeR was used to test for different filter parameters to maximize the number of SNP's considered while maintaining accuracy/correlation values.
Accuracy file is located [here](scripts_and_output/accuracy.ini)
Corresponding results for accuracy run are [here](scripts_and_output/sum.dat)

Case 3 was chosen from these results, and used for imputation after filtering steps

## Filtering + Imputing
Filtering + Imputing scripts and output located on cluster here `/labs/Wegrzyn/EAB_github/Filtering_Local`

1. Remove problematic individuals FP3112 FP3150 for poor sequencing
Output:
* 3,565,012 Sites
* 100 Individuals

2. vcftools --mac 2 --minQ 30
Output:
* 1,325,113 Sites
* 100 Individuals

3. Custom Mendelian [Filter](scripts_and_output/mendelian_error.py)  
Working Dir: `/labs/Wegrzyn/EAB_github/Filtering_Local/filtering`

Output:
* `/labs/Wegrzyn/EAB_github/Filtering_Local/filtering/output.vcf`
* mendelianlog.txt explains each SNP and why it was converted to missing
4180743 snp's converted to missing
Breakdown by family:
 {'I': 1095995, 'C': 451840, 'G': 0, 'J': 386288, 'L': 0, 'D': 200770, 'H-A': 330207, 'F': 0, 'K': 1144035, 'E': 219087, 'H-B': 352521}

4. Impute
Working Dir: `/labs/Wegrzyn/EAB_github/Filtering_Local/imputeR`  
* [script](scripts_and_output/submit.sh)  
* Summarizing imputed VCF file is still needed, can be done by comparing original and imputed vcf, possibly through vcftools  

Input:  
* `/labs/Wegrzyn/EAB_github/Filtering_Local/imputeR/accuracy.ini`

Output:  
* `/labs/Wegrzyn/EAB_github/Filtering_Local/imputeR/imputed.vcf` 
* Imput output: `/labs/Wegrzyn/EAB_github/Filtering_Local/imputeR/imputeR2920082.err`
