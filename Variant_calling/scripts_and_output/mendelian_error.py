import sys

class mendelian:
  def __init__(self,vcf_fn,ped_fn,family_fn,ignore_set):
    self.logfile = open("mendelianlog.txt",'w')
    self.ignore_set = ignore_set.split(",")
    self.vcf_fn = vcf_fn
    self.family = self.read_families(family_fn)
    self.family_stats = dict([(i,0) for i in set(self.family.values())])
    print(self.family_stats)
    h = self.grab_header()
    self.indivs = h[9:] + ["."]
    self.col = dict(zip(h,[i for i in range(len(h))]))
    self.ped = self.read_pedigree(ped_fn)
    self.children = set(self.ped.keys()).intersection(self.indivs)
    print(self.children)
    self.miss = '.'
    self.missphased = '.:.:.:.:.:.:.:.'
    self.converted=0
    self.ln=0
    self.run_filter("output.vcf")
    self.logfile.close()

  def grab_header(self):
    with open(self.vcf_fn) as rf:
      for line in rf:
        if(line.startswith("#CHROM")):
          return line.strip().split()
      else:
        print("Cant find individuals line in vcf file")
        return

  def read_families(self,family_fn):
    d = {}
    with open(family_fn) as rf:
      for line in rf:
        line = line.strip().split()
        d[line[0]] = line[1]
    return d

  def run_filter(self,wf):
    with open(wf,'w') as wf:
      with open(self.vcf_fn) as rf:
        for line in rf:
          self.ln+=1
          if(line.startswith("#")):
            wf.write(line)
          else: # run filter
            line = line.strip().split()
            for i in self.children:
              if(self.family[i] in self.ignore_set):
                continue
              geno = line[self.col[i]]
              if("/" in geno):
                miss = self.missphased
              else:
                miss = self.miss
              if(self.check_genotype(line,i)==False):
                self.converted+=1
                self.family_stats[self.family[i]]+=1
                line[self.col[i]]=miss
            wf.write("\t".join(line)+"\n")
    print("%d snp's converted to missing" % self.converted)
    print("Breakdown by family:\n %s" % str(self.family_stats))

  def read_pedigree(self,ped_fn):
    ped = {}
    with open(ped_fn) as rf:
      for line in rf:
        line = line.strip().split(",")
        ped[line[0]] = [line[1],line[2]]
    return ped

  def check_genotype(self,line,indiv):
    parents = self.ped[indiv]

    geno = self.get_geno(line,indiv)
    if(geno[0]=="." and geno[1]=="."):
      return True
    fat = self.get_geno(line,parents[0])
    mot = self.get_geno(line,parents[1])
    combinations = [(i,j) for i in fat for j in mot]+[(j,i) for i in fat for j in mot]
    s = set()
    geno = (geno[0],geno[1])
    for i in combinations:
      s.add(i)

    # account for missing in parents/child
    if("." in mot and "." in fat): #anything is possible
      return True
    if("." in mot or "." in fat):
      if((geno[0],".") in combinations or (geno[1],".") in combinations):
        return True
    if(geno in combinations):
      return True
    else:
      self.log("%s: Child %s %c/%c has parents %s %c/%c and %s %c/%c" % (line[0]+"_"+line[1],indiv,geno[0],geno[1],parents[0],fat[0],fat[1],parents[1],mot[0],mot[1]))
      return False

  def get_geno(self,line,indiv):
    if(indiv=="."):
      return [".","."]
    geno = line[self.col[indiv]][:3]
    if(geno[1] != '/' and geno[1] != '|'):
      return [".","."]
    if(len(geno)!=3):
      print("Error getting genotype of indiv %s: %s" % (indiv,geno))
      print(line)
      sys.exit()
    return [geno[0],geno[2]]

  def log(self,line):
    self.logfile.write(line+"\n")

    








if(__name__=="__main__"):
  if(len(sys.argv)>1):
    ignore_set = sys.argv[1]
  else:
    ignore_set = ""
  vcf_fn="2.recode.vcf"
  pedigree = "ped.txt"
  family = "c_to_f.txt"
  m = mendelian(vcf_fn,pedigree,family,ignore_set)
