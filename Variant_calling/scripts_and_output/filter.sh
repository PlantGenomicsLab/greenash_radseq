#!/bin/bash
#SBATCH --job-name=default_run
#SBATCH -o %x%j.out
#SBATCH -e %x%j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G

module load vcftools/0.1.15

vcf=../1.recode.vcf

# Quality

#vcftools --vcf $vcf --recode --recode-INFO-all --minQ 30 --mac 2 --out 2

# Allelic Primitives
#module load vcflib
#vcfallelicprimitives --keep-info --keep-geno 2.recode.vcf > 3.purged.vcf

# Remove indels
#vcftools --vcf 2.recode.vcf --remove-indels --recode --recode-INFO-all --out 3

# Mendelian filter
python mendelian_error.py
