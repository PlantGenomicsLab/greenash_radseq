#!/bin/bash
#SBATCH --job-name=imputeR
#SBATCH -o %x%j.out
#SBATCH -e %x%j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G

java -Xmx100G -jar ../LinkImputeR.jar -s accuracy.ini

java -Xmx100G -jar ../LinkImputeR.jar impute.xml 'Case 3' imputed.vcf
