# Mendelian Error Filter

* Takes as input a VCF file and pedigree information contained within two csv files to filter SNPs based on mendelian errors
* Reports converted SNP numbers by family on completion
* Logs all converted SNPs to filteredSNPs.log during runtime
* example analysis `python mendelianError.py --vcf orig.vcf --ped childToParents.csv --fam childToFamilyID.csv`
