import argparse
import logging

parser = argparse.ArgumentParser(
    description="Analyze an input VCF file, and filter each site for mendelian error based on provided pedigree structure"
)
parser.add_argument(
    "--vcf",
    metavar="v",
    type=str,
    help="The full path of the VCF input file, with fileformat=VCFv4.2",
    required=True,
)
parser.add_argument(
    "--ped",
    metavar="p",
    type=str,
    help="The full path of the pedigree input csv file which maps child ID to parent IDs",
    required=True,
)
parser.add_argument(
    "--fam",
    metavar="f",
    type=str,
    help="The full path of a file that maps child ID to family ID",
    required=True,
)
parser.add_argument(
    "--out", metavar="o", type=str, help="Output file name", default="output.vcf"
)


class mendelianError:
    def __init__(self, vcfFilename, pedFilename, familyFilename, outFilename):
        logging.basicConfig(filename="filteredSNPs.log", level=logging.INFO)

        self.vcfFilename = vcfFilename
        self.childToParents = self.readFile(pedFilename)
        self.childToFamily = self.readFile(familyFilename)
        self.out = outFilename

        ## Maps individualidual strings to columns of the VCF file where their SNP data is stored
        vcfHeader = self.grabHeader()
        self.individualToColumn = dict(
            zip(vcfHeader, [columnIndex for columnIndex in range(len(vcfHeader))])
        )

        ## All children present in the current VCF file
        self.studyIndividuals = self.grabHeader()[9:] + ["."]
        self.children = set(self.childToParents.keys()).intersection(
            self.studyIndividuals
        )

        # Variables modified during filter
        ## Used to store number of mendelian errors detected per family ID
        self.familyStats = dict([(id, 0) for id in set(self.childToFamily.values())])

        ## Used to store total number of mendelian errors detected
        self._converted = 0

        self._lineNumber = 0

        # String that specifies a missing SNP value
        self._miss = "."
        # If the SNP is phased, the corresponding missing value
        self._missPhased = ".:.:.:.:.:.:.:."

    def grabHeader(self) -> list:
        # Reads input VCF and returns the header which contains the column labels
        with open(self.vcfFilename) as readFile:
            for line in readFile:
                if line.startswith("#CHROM"):
                    return line.strip().split()
            else:
                raise ValueError(
                    "Unable to parse VCF Header for study individual, ensure that input file follows correct VCF format"
                )

    def readFile(self, fileName) -> dict:
        # Reads input csv file and outputs dictionary that maps the first csv per line to the rest
        d = {}
        with open(fileName) as readFile:
            for line in readFile:
                line = line.strip().split(",")
                if len(line[1:]) == 1:
                    d[line[0]] = line[1]
                else:
                    d[line[0]] = line[1:]
        return d

    def runFilter(self) -> None:
        # Run the mendelian error filter, converting SNPs that fail the check to missing
        # Output statistics are reported at the end of the filter
        # Lines that are filtered are logged to an output file
        with open(self.out, "w") as writeFile:
            with open(self.vcfFilename) as readFile:
                for line in readFile:
                    self._lineNumber += 1
                    if line.startswith("#"):
                        writeFile.write(line)
                    else:
                        line = line.strip().split()
                        # Perform mendelian check for each SNP in each line
                        for childID in self.children:
                            genotype = line[self.individualToColumn[childID]]
                            if "/" in genotype:  # Indicates SNP is phased
                                miss = self._missPhased
                            else:
                                miss = self._miss

                            if (
                                self.checkGenotype(line, childID) == False
                            ):  # The SNP fails the check and is converted to missing value
                                self._converted += 1
                                self.familyStats[self.childToFamily[childID]] += 1
                                line[self.individualToColumn[childID]] = miss
                        writeFile.write("\t".join(line) + "\n")
        print("%d total SNPs converted to missing" % self._converted)
        print("Breakdown by familyID:\n %s" % str(self.familyStats))

    def checkGenotype(self, line, individual):
        # Check a given child genotype to see if it passes the filter
        parents = self.childToParents[individual]

        genotype = self.getGenotype(line, individual)
        if genotype[0] == "." and genotype[1] == ".":
            return True
        father = self.getGenotype(line, parents[0])
        mother = self.getGenotype(line, parents[1])

        # All genetically possible combinations of father and motherher genotypes
        combinations = [(i, j) for i in father for j in mother] + [
            (j, i) for i in father for j in mother
        ]
        s = set()
        genotype = (genotype[0], genotype[1])
        for i in combinations:
            s.add(i)

        # account for missing values in parents/child
        if (
            "." in mother and "." in father
        ):  # if both are missing the child passes the check
            return True
        if "." in mother or "." in father:
            if (genotype[0], ".") in combinations or (genotype[1], ".") in combinations:
                return True

        if genotype in combinations:
            return True
        else:
            logging.info(
                "%s: Child %s %c/%c has parents %s %c/%c and %s %c/%c"
                % (
                    line[0] + "_" + line[1],
                    individual,
                    genotype[0],
                    genotype[1],
                    parents[0],
                    father[0],
                    father[1],
                    parents[1],
                    mother[0],
                    mother[1],
                )
            )
            return False

    def getGenotype(self, line, individual):
        # Get genotype value from the vcf file for a given line and individualID
        if individual == ".":
            return [".", "."]
        genotype = line[self.individualToColumn[individual]][:3]
        if genotype[1] != "/" and genotype[1] != "|":
            return [".", "."]
        if len(genotype) != 3:
            raise ValueError(
                "Error getting genotype of individual %s: %s\n line=%s"
                % (individual, genotype, line)
            )
        return [genotype[0], genotype[2]]


if __name__ == "__main__":
    args = parser.parse_args()
    m = mendelianError(args.vcf, args.ped, args.fam, args.out)
    m.runFilter()
