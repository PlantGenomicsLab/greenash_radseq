# Preprocessing
All steps taken to turn Green Ash leaf material into computer readable and processed DNA code. 

## Background
* Sampling population cultivated and maintained by Jennifer L. Koch Laboratory
* See table [Original Family Structure](../Family_analysis/README.md)
* Physical collection conducted at the USDA Northern Research Station in Delaware, OH
* [Relevant publication](https://www.fs.fed.us/pnw/pubs/pnw_gtr963.pdf#page=124). "Saving Green Ash" by J. Romvero-Severson and Jennifer L. Koch

## Sampling
1. 3-5 leaflets collected from terminal half of leaf. Leaflets kept attached if possible.
2. Sample placed into small Ziploc bag
3. Sample ID recorded (delaware ID)
4. All samples placed into larger Ziplog bag, into cooler for storage until extraction

* 151 total leaf samples
* of these, 17 were parental

## Extraction
* Performed at the Romvero-Severson Lab at the University of Notre Dame
1. Samples ground using a freezer mill
2. Extraction conducted using the [NORGEN protocol](NORGEN_protocol.pdf) for use with plant/fungi DNA
3. Samples shipped for library prep

## Sequencing
* Library preparation performed at the University of Connecticut in the Center for Genome Innovation Lab
* Sequencing performed at QB3 at Berkeley on a NovaSeq S1
1. Samples run on the Agilent Tapestation genomic DNA assay at a 1:10 dilution
2. DNA purification performed to improve borderline samples for ddRAD
3. 12 fail individuals selected for WGS sequencing (9 parents) based on QC results
4. Passed samples sent for sequencing

* [gDNA QC results](Sequencing/README.md) in the "Master Spreadsheet"
* 60 failed, 91 sequenced ddRAD, 12 sequenced WGS
* [Pass/Fail broken down by family](Sequencing/README.md)

## Reads Processing & Alignment
* [Full Breakdown](Alignment/README.md)
