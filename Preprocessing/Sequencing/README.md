# Sequencing Links
* We used Pstl (P1) and Msel (P2) enzymes for the ddRAD sequencing - we also used UMIs which were super important for the success of the project and had an estimated coverage of 30X
* [Sequencing Results Master Spreadsheet](https://docs.google.com/spreadsheets/d/1sYFu7GqWY3Be4cqvVA7Cz4OZXlobX6CveLWakQ5UtjQ/edit?usp=sharing)


# Sequencing Tables

### Sample P/F by Family

| Sampled_Fam                 | Num_Sampled | Num_Failed | Num_Sequenced_ddRAD | Selected_For_WGS |
|-----------------------------|-------------|------------|---------------------|------------------|
| B                           | 4           | 4          | 0                   | 0                |
| C                           | 11          | 1          | 10                  | 0                |
| D                           | 10          | 5          | 5                   | 0                |
| E                           | 8           | 4          | 4                   | 0                |
| F                           | 2           | 0          | 2                   | 0                |
| G                           | 5           | 1          | 4                   | 0                |
| H                           | 22          | 3          | 19                  | 0                |
| I                           | 26          | 8          | 18                  | 1                |
| J                           | 8           | 2          | 6                   | 0                |
| K                           | 35          | 15         | 20                  | 1                |
| L                           | 1           | 0          | 1                   | 0                |
| Law                         | 1           | 1          | 0                   | 0                |
| Sum                         | 1           | 1          | 0                   | 1                |
| Parents                     | 17          | 15         | 2                   | 9                |
| Total                       | 151         | 60         | 91                  | 12               |
| Total Individuals Sequenced |             |            |                     | 103              |
