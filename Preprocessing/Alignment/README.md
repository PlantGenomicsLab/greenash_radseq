# Preprocessing/Alignment Pipeline

## Preprocessing

*Summary:* We assign which sequences come from which samples, perform basic quality control both on our ddRAD and WGS data, summarize fastqc results, and trim poor quality reads. Additionally, adapters are removed.

1. [Demultiplex](./scripts/preprocess/01_demultiplex.sh)
* Dependencies: Stacks/2.41
We run the Stacks tools `process_radtags` which checks that our barcode and RAD cutsites are intact and demultiplexes the data. Additionally we choose to discard reads that contain a phred quality score <10.

2. Fastqc
* Two scripts: one each for ddRAD, WGS
* Dependencies: fastqc
* [ddRAD](./scripts/preprocess/02_fastqc_ddRAD.sh)
* [WGS](./scripts/preprocess/03_fastqq_WGS.sh)
We run the tool fastqc with the goal of detecting problems that may be present in our data that originate from either the sequencer or the starting library material. These problems are summarized in general sequence statistics categories, examples being `Per base sequence quality` `Per sequence quality score` and `sequence duplication levels`. Certain tests such as `sequence duplication levels` we expect to fail when running fastqc with ddRAD data.

An additional tool called MultiQC was run to organize the output of fastqc with some nice visuals.
* [MultiQC](./scripts/preprocess/04_multiqc.sh)
* [Output](./figures/multiqc_report.html)

3. [Trim WGS](./scripts/preprocess/05_qualitytrim_WGS.sh)
We run the tool Trimmomatic in order to efficiently trim and crop our WGS FASTQ data and remove adapters. Run in paired-end mode in order to maintain correspondence of read pairs including improved adapter finding.
* Reads lower than 45bp were dropped, ends trimmed by 3bp and 3' end trimmed further depending on quality according to parameter SLIDINGWINDOW:4:20

## Alignment

1. [BWA_ddRAD](./scripts/align/01_bwa_ddRAD.sh)
* Dependencies: BWA, Samtools, Umitools
We run the tool `umitools extract` to remove UMIs and append to the read name, barcodes remain. Additionally `UMItools dedup` is run to group PCR duplicates and deduplicate reads to yield one read per group. `BWA mem` is run which performs local alignment by seeding alignments with maximal exact matches and extending seeds with the Smith-Waterman algorithm. Finally we utilize `samtools` to index our BAM files
* All ddRAD align 100%
* [Mapping_rates](https://docs.google.com/spreadsheets/d/1Cj00l_8IvZ-bb9vMu_44JjCYw_D69ge7hJprmNSPr8g/edit#gid=266066820)

2. [BWA_WGS](./scripts/align/02_bwa_WGS.sh)
Mark duplicates in read-id grouped paired-end SAM files. Marked duplicates will be ignored during variant calling.
* WGS map highly from 82-98%
* [visual](./figures/WGS_mapping.pdf)


3. [alignQC_WGS](./scripts/align/03_alignQC_WGS.sh)
Samtools stats run on all bam files. Output BAM stats are collected and placed into a single folder.

